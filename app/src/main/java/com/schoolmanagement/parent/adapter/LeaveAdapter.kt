package com.schoolmanagement.parent.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.pojo.LeaveDataPojo
import com.schoolmanagement.parent.util.ParentConstant
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arpit.jain on 2/21/2018.
 */
class LeaveAdapter(val context: Activity, private val leave_list: ArrayList<LeaveDataPojo>?) : RecyclerView.Adapter<LeaveAdapter.ViewHolder>() {

    var ctx: HomeActivity = context as HomeActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeaveAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_leaves, parent, false)
        return LeaveAdapter.ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: LeaveAdapter.ViewHolder, position: Int) {
        holder.bindItems(leave_list!![position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return leave_list!!.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(leaves: LeaveDataPojo, ctx: HomeActivity) {
            val name: TextView = itemView.findViewById(R.id.name)
            val grade: TextView = itemView.findViewById(R.id.grade)
            val created_date: TextView = itemView.findViewById(R.id.created_date)
            val leave_date: TextView = itemView.findViewById(R.id.leave_date)
            val leave_description: TextView = itemView.findViewById(R.id.leave_description)
            val profilePic: ImageView = itemView.findViewById(R.id.profilePic)

            if (ctx.getStudentData()!!.profile_pic != null && ctx.getStudentData()!!.profile_pic != "") {
                ctx.setProfileImageInLayout(ctx, ParentConstant.IMAGE_STUDENT_PROFILE + ctx.getStudentData()!!.profile_pic, profilePic)
            } else {
                profilePic.setImageResource(R.mipmap.usericon)
            }

            val format = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            val format2 = SimpleDateFormat("dd MMMM", Locale.US)
            val fromDate = format.parse(leaves.from_date)
            val from_date = format2.format(fromDate)
            val toDate = format.parse(leaves.to_date)
            val to_date = format2.format(toDate)

            name.text = ctx.getStudentName()
            grade.text = leaves.classes
            leave_date.text = ctx.resources.getString(R.string.leave, from_date, to_date)
            leave_description.text = leaves.reason

            if (leaves.created_at != "") {
                val newDate = leaves.created_at.replace(".000Z", "").split("T")
                created_date.text = ctx.changeDateTimeFormat(newDate[0] + " " + newDate[1])
            }
        }
    }
}