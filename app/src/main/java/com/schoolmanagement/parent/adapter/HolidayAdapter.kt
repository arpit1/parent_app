package com.schoolmanagement.parent.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.pojo.HolidayDataPojo
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arpit.jain on 2/20/2018.
 */
class HolidayAdapter(val context: Activity, val holiday_list: ArrayList<HolidayDataPojo>?) : RecyclerView.Adapter<HolidayAdapter.ViewHolder>() {

    var ctx: HomeActivity = context as HomeActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolidayAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_holiday, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: HolidayAdapter.ViewHolder, position: Int) {
        holder.bindItems(holiday_list!![position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return holiday_list!!.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(holidays: HolidayDataPojo) {
            val holidayDate: TextView = itemView.findViewById(R.id.holidayDate)
            val holidayDay: TextView = itemView.findViewById(R.id.holidayDay)
            val holidayFestival: TextView = itemView.findViewById(R.id.holidayFestival)

            val format = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            val date = format.parse(holidays.start_date)
            val dayOfTheWeek = DateFormat.format("EEEE", date) as String
            holidayDay.text = holidays.to_date
            holidayDate.text = holidays.start_date
            holidayFestival.text = holidays.festival

//            if(holidays.created != ""){
//                val new_date = holidays.created.replace(".000Z", "").split("T")
//                holidayDate.text = ctx.changeDateTimeFormat(new_date[0] +" "+ new_date[1])
//            }
        }
    }
}