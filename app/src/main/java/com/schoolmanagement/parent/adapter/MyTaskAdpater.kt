package com.schoolmanagement.parent.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.MyTaskListActivity
import com.schoolmanagement.parent.pojo.TeacherTaskDataPojo

/**
 * Created by upasna.mishra on 5/1/2018.
 */
class MyTaskAdpater(val context: Activity, private val teacherTaskList: ArrayList<TeacherTaskDataPojo>) : RecyclerView.Adapter<MyTaskAdpater.ViewHolder>() {

    var ctx: MyTaskListActivity = context as MyTaskListActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyTaskAdpater.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_parent_task_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: MyTaskAdpater.ViewHolder, position: Int) {
        holder.bindItems(teacherTaskList[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return teacherTaskList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(teacherTaskList: TeacherTaskDataPojo, ctx: MyTaskListActivity) {

            val title: TextView = itemView.findViewById(R.id.title)
            val description: TextView = itemView.findViewById(R.id.description)
            val date: TextView = itemView.findViewById(R.id.date)
            val progressSpinner: Spinner = itemView.findViewById(R.id.progressSpinner)
            var check = 0

            val progress = arrayOf("0%", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%")
            // Create an ArrayAdapter using a simple spinner layout and languages array
            val taskProgressSp = ArrayAdapter(ctx, R.layout.row_spinner_white, progress)
            // Set layout to use when the list of choices appear
            taskProgressSp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Set Adapter to Spinner
            progressSpinner.adapter = taskProgressSp
            if(teacherTaskList.dates != "")
                date.text = ctx.parentChangeDateTimeFormat(teacherTaskList.dates)

            title.text = teacherTaskList.title
            description.text = teacherTaskList.description

            val value = teacherTaskList.status + "%"
            val spinnerPos = taskProgressSp.getPosition(value)
            progressSpinner.setSelection(spinnerPos)
            progressSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                    if (++check > 1) {
                        val selectedValue = progressSpinner.selectedItem.toString().replace("%", "").toInt()
                        ctx.hideSoftKeyboard()
                        ctx.updateTask(teacherTaskList.id, selectedValue.toString())
                    }
                }

                override fun onNothingSelected(parent: AdapterView<out Adapter>?) {
                }
            }

        }
    }
}