package com.schoolmanagement.parent.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.ChatScreenActivity
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.activity.TeacherProfileActivity
import com.schoolmanagement.parent.pojo.TeacherDataPojo
import com.schoolmanagement.parent.util.ParentConstant
import de.hdodenhof.circleimageview.CircleImageView

class ContactAdapter(val context: Activity, private val teacherList: ArrayList<TeacherDataPojo>) : RecyclerView.Adapter<ContactAdapter.ViewHolder>() {

    var ctx: HomeActivity = context as HomeActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_contact_layout, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ContactAdapter.ViewHolder, position: Int) {
        holder.bindItems(teacherList[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return teacherList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("ResourceAsColor")
        fun bindItems(teacherList: TeacherDataPojo, ctx: HomeActivity) {

            val teacherProfilePic: CircleImageView = itemView.findViewById(R.id.teacherProfilePic)
            val teacherName: TextView = itemView.findViewById(R.id.teacherName)
            val image: ImageView = itemView.findViewById(R.id.image)
            image.visibility = View.GONE
            val lastMsg: TextView = itemView.findViewById(R.id.lastMsg)
            lastMsg.visibility = View.VISIBLE
            if (teacherList.profile_pic != null && teacherList.profile_pic != "") {
                if (teacherList.type == "0")
                    ctx.setProfileImageInLayout(ctx, ParentConstant.IMAGE_TEACHER_PROFILE + teacherList.profile_pic, teacherProfilePic)
                else
                    ctx.setProfileImageInLayout(ctx, ParentConstant.SCHOOL_PROFILE_PIC + teacherList.profile_pic, teacherProfilePic)
            } else {
                teacherProfilePic.setImageResource(R.mipmap.usericon)
            }

            lastMsg.text = teacherList.qualification
            if (teacherList.type == "0") {
                teacherName.text = getTeacherName(teacherList)
                teacherName.setTextColor(ContextCompat.getColor(ctx, R.color.black))

                itemView.setOnClickListener {
                    openTeacherDialog(ctx, teacherList)
                }
            } else {
                teacherName.text = ctx.resources.getString(R.string.admin_name, teacherList.fname)
                teacherName.setTextColor(ContextCompat.getColor(ctx, R.color.login_button_color))

                itemView.setOnClickListener {
                    ctx.startActivity(Intent(ctx, ChatScreenActivity::class.java).putExtra("reciver_id", teacherList.id).putExtra("username", getTeacherName(teacherList)))
                }
            }
        }
    }

    private fun openTeacherDialog(ctx: HomeActivity, teacherData: TeacherDataPojo) {
        val crossImage: ImageView
        val profilePic: CircleImageView
        val teacherName: TextView
        val teacherEmail: TextView
        val llInfo: LinearLayout
        val ivInfo: ImageView
        val llChat: LinearLayout
        val ivChat: ImageView
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar)
        dialog.setContentView(R.layout.teacher_dialog_layout)
        crossImage = dialog.findViewById(R.id.crossImage)
        profilePic = dialog.findViewById(R.id.profilePic)
        teacherName = dialog.findViewById(R.id.teacherName)
        teacherEmail = dialog.findViewById(R.id.teacherEmail)
        llInfo = dialog.findViewById(R.id.llInfo)
        ivInfo = dialog.findViewById(R.id.ivInfo)
        llChat = dialog.findViewById(R.id.llChat)
        ivChat = dialog.findViewById(R.id.ivChat)
        crossImage.setOnClickListener {
            dialog.dismiss()
        }
        if (teacherData.profile_pic != null && teacherData.profile_pic != "") {
            ctx.setProfileImageInLayout(ctx, ParentConstant.IMAGE_TEACHER_PROFILE + teacherData.profile_pic, profilePic)
        } else {
            profilePic.setImageResource(R.mipmap.usericon)
        }
        teacherName.text = getTeacherName(teacherData)
        teacherEmail.text = teacherData.email
        llInfo.setOnClickListener {
            //            llInfo.setBackgroundColor(ContextCompat.getColor(ctx, R.color.header_color))
//            llChat.setBackgroundColor(ContextCompat.getColor(ctx, R.color.white))

            ivInfo.setImageResource(R.mipmap.infoselect)
            ivChat.setImageResource(R.mipmap.chat_unselect)

            dialog.dismiss()

            ctx.startActivity(Intent(ctx, TeacherProfileActivity::class.java).putExtra("teacherData", teacherData))
        }
        llChat.setOnClickListener {
            //            llChat.setBackgroundColor(ContextCompat.getColor(ctx, R.color.header_color))
//            llInfo.setBackgroundColor(ContextCompat.getColor(ctx, R.color.white))

            ivInfo.setImageResource(R.mipmap.info)
            ivChat.setImageResource(R.mipmap.chat)
            dialog.dismiss()

            val receiverId: String = if (teacherData.type == "1") {
                teacherData.id
            } else {
                "T_" + teacherData.id
            }
            ctx.startActivity(Intent(ctx, ChatScreenActivity::class.java).putExtra("reciver_id", receiverId).putExtra("username", getTeacherName(teacherData)))
        }
        if (!ctx.isFinishing)
            dialog.show()
    }

    private fun getTeacherName(teacherData: TeacherDataPojo): String {
        return if (teacherData.mname != "")
            teacherData.fname + " " + teacherData.mname + " " + teacherData.lname
        else
            teacherData.fname + " " + teacherData.lname
    }

}