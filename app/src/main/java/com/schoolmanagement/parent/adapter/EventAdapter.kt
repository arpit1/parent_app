package com.schoolmanagement.parent.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.EventDetailActivity
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.pojo.EventDataPojo
import com.schoolmanagement.parent.util.ParentConstant
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by upasna.mishra on 2/23/2018.
 */
class EventAdapter(val context: Activity, val eventList: ArrayList<EventDataPojo>) : RecyclerView.Adapter<EventAdapter.ViewHolder>() {

    var ctx: HomeActivity = context as HomeActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_event, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: EventAdapter.ViewHolder, position: Int) {
        holder.bindItems(eventList[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return eventList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("ResourceAsColor")
        fun bindItems(eventList: EventDataPojo, ctx: HomeActivity) {

            val profilePic: CircleImageView = itemView.findViewById(R.id.profilePic)
            val eventName: TextView = itemView.findViewById(R.id.eventName)
            val dateTime: TextView = itemView.findViewById(R.id.dateTime)
            val venue: TextView = itemView.findViewById(R.id.venue)
            val postImage: ImageView = itemView.findViewById(R.id.postImage)
            val row_click: LinearLayout = itemView.findViewById(R.id.row_click)

            if (eventList.logo != null && eventList.logo != "") {
                ctx.setProfileImageInLayout(ctx, ParentConstant.SCHOOL_PROFILE_PIC + eventList.logo, profilePic)
            } else {
                profilePic.setImageResource(R.mipmap.usericon)
            }

            eventName.text = eventList.event_name
            dateTime.text = ctx.resources.getString(R.string.from_to, eventList.start_date, eventList.end_date)
            venue.text = eventList.details
            if (eventList.image != null && eventList.image != "") {
                postImage.visibility = View.VISIBLE
                if (eventList.image!!.contains(".gif")) {
                    ctx.setGifImage(ctx, ParentConstant.EVENT_IMAGE_URL + eventList.image, postImage)
                } else
                    ctx.setProfileImageInLayout(ctx, ParentConstant.EVENT_IMAGE_URL + eventList.image, postImage)
            } else {
                postImage.visibility = View.GONE
                venue.text = eventList.registrationDetails
                postImage.setImageResource(R.mipmap.ic_launcher)
            }

            row_click.setOnClickListener {
                ctx.startActivity(Intent(ctx, EventDetailActivity::class.java).putExtra("event_id", eventList.id))
            }
        }
    }
}