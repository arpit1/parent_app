package com.schoolmanagement.parent.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.ProfileActivity
import com.schoolmanagement.parent.util.ParentConstant

/**
 * Created by arpit.jain on 2/24/2018.
 */
class StudentPagerAdapter(val ctx: ProfileActivity) : PagerAdapter() {

    private var inflater: LayoutInflater? = null

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return 1
//        return 5
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val data = ctx.getStudentData()
        inflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater!!.inflate(R.layout.row_layout_student, container, false)

        val studentPic = itemView.findViewById(R.id.studentPic) as ImageView
        val studentName = itemView.findViewById(R.id.studentName) as TextView
        val gender = itemView.findViewById(R.id.gender) as TextView
        val birth_date = itemView.findViewById(R.id.birth_date) as TextView
        val program = itemView.findViewById(R.id.program) as TextView
        val batch = itemView.findViewById(R.id.batch) as TextView
        val grade = itemView.findViewById(R.id.grade) as TextView
        val phone = itemView.findViewById(R.id.phone) as TextView
        val email = itemView.findViewById(R.id.email) as TextView

        if(data!!.student_mname == ""){
            studentName.text = data.student_fname + " " + data.student_lname
        }else{
            studentName.text = data.student_fname + " " + data.student_mname + " " + data.student_lname
        }
        grade.text = data.classes
        birth_date.text = data.date_of_birth
        gender.text = data.gender
        program.text = data.programs
        batch.text = data.batch_name
        email.text = data.student_email
        phone.text = data.country_code + " " + data.home_phone
        ctx.setProfileImageInLayoutProfile(ctx, ParentConstant.IMAGE_STUDENT_PROFILE+data.profile_pic, studentPic)

        // Add viewpager_item.xml to ViewPager
        container.addView(itemView)

        return itemView
    }

    override fun getItemPosition(`object`: Any?): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        // Remove viewpager_item.xml from ViewPager
        container.removeView(`object` as View)

    }
}