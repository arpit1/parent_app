package com.schoolmanagement.parent.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.fragment.TasksFragment
import com.schoolmanagement.parent.pojo.TaskDataPojo

class TaskAdapter(val context: Activity, private val taskList: ArrayList<TaskDataPojo>, taskFragmentObj: TasksFragment) : RecyclerView.Adapter<TaskAdapter.ViewHolder>() {

    private var ctx: HomeActivity = context as HomeActivity
    private val taskFragObj: TasksFragment = taskFragmentObj

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_task_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: TaskAdapter.ViewHolder, position: Int) {
        holder.bindItems(taskList[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return taskList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("ResourceAsColor")
        fun bindItems(taskList: TaskDataPojo, ctx: HomeActivity) {

            val taskName: TextView = itemView.findViewById(R.id.taskName)
            val txtTaskProgress: TextView = itemView.findViewById(R.id.txt_taskProgress)
            val progressSpinner: Spinner = itemView.findViewById(R.id.progressSpinner)
            val taskDate: TextView = itemView.findViewById(R.id.taskDate)
            val teacherName: TextView = itemView.findViewById(R.id.teacherName)
            val teacherQualification: TextView = itemView.findViewById(R.id.teacherQualification)
            val taskDescription: TextView = itemView.findViewById(R.id.taskDescription)
            var check = 0

            val progress = arrayOf("0%", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%")
            // Create an ArrayAdapter using a simple spinner layout and languages array
            val taskProgressSp = ArrayAdapter(ctx, R.layout.row_spinner_white, progress)
            // Set layout to use when the list of choices appear
            taskProgressSp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Set Adapter to Spinner
            progressSpinner.adapter = taskProgressSp

            taskName.text = taskList.title
            if (taskList.start_date != "" && taskList.end_date != "") {
                taskDate.text = ctx.resources.getString(R.string.from_to, ctx.changeDateFormatWithoutTime(taskList.start_date), ctx.changeDateFormatWithoutTime(taskList.end_date))
            }

            teacherQualification.text = taskList.qualification

            teacherName.text = ctx.getTeacherName(taskList)

            taskDescription.text = taskList.description

            if (taskList.description == "") {
                taskDescription.visibility = View.GONE
            } else {
                taskDescription.visibility = View.VISIBLE
            }

            val completeStatus = taskList.complete_status.toInt()
            val value = taskList.complete_status + "%"
            val spinnerPos = taskProgressSp.getPosition(value)
            progressSpinner.setSelection(spinnerPos)
            txtTaskProgress.text = value
            setTextBackground(completeStatus, txtTaskProgress)
            progressSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                    if (++check > 1) {
                        val selectedValue = progressSpinner.selectedItem.toString().replace("%", "").toInt()
                        ctx.hideSoftKeyboard()
                        taskFragObj.updateTask(ctx, taskList.id, selectedValue.toString(), taskList.teacher_id)
                    }
                }

                override fun onNothingSelected(parent: AdapterView<out Adapter>?) {
                }
            }

            if (taskList.description != "")
                itemView.setOnClickListener { displayCommonDialog(taskList.description, taskList.title) }
        }
    }

    private fun displayCommonDialog(msg: String, title: String) {
        val btnYesLL: LinearLayout
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar)
        dialog.setContentView(R.layout.custom_dialog_withheader)

        val msgTextView = dialog.findViewById(R.id.text_exit) as TextView
        val header = dialog.findViewById(R.id.text_header) as TextView

        header.visibility = View.VISIBLE
        header.text = title
        msgTextView.text = msg
        btnYesLL = dialog.findViewById(R.id.btn_yes_exit_LL) as LinearLayout

        btnYesLL.setOnClickListener { dialog.dismiss() }

        if (!ctx.isFinishing)
            dialog.show()
    }

    fun setTextBackground(setValue: Int, txtProgress: TextView) {
        when {
            setValue <= 30 -> {
                txtProgress.setBackgroundResource(R.drawable.gradient_task_list)
            }
            setValue in 31..60 -> {
                txtProgress.setBackgroundResource(R.drawable.gradient_task_list_medium)
            }
            setValue > 60 -> {
                txtProgress.setBackgroundResource(R.drawable.gradient_task_list_heigh)
            }
        }
    }
}