package com.schoolmanagement.parent.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeAllVideo
import com.schoolmanagement.parent.pojo.HomePostFilePojo
import com.schoolmanagement.parent.util.ParentConstant
import com.squareup.picasso.Picasso

/**
 * Created by upasna.mishra on 2/14/2018.
 */
class HomeVideoAdapter(ctx: HomeAllVideo, videoList: ArrayList<HomePostFilePojo>) : BaseAdapter() {

    var ctx_home: HomeAllVideo = ctx
    var video_list = videoList
    private lateinit var postImage: ImageView
    private lateinit var playvideoicon: ImageView

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.activity_grid_row, parent, false)
        postImage = view.findViewById(R.id.img_profile)
        playvideoicon = view.findViewById(R.id.playvideoicon)
        Picasso.with(ctx_home).load(ParentConstant.THUMB_IMAGE_URL + "" + video_list[position].thumbnail).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(postImage)
        return view
    }

    override fun getItem(p0: Int): Any {
        return 0
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return video_list.size
    }

}