package com.schoolmanagement.parent.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.NoticeBoardAttachFileActivity
import com.schoolmanagement.parent.pojo.NoticeBoardFileDataPojo
import com.schoolmanagement.parent.util.ParentConstant

/**
 * Created by upasna.mishra on 2/22/2018.
 */
class NoticeAttachFileAdapter(val context: Activity, val fileList: ArrayList<NoticeBoardFileDataPojo>, val from: String) : RecyclerView.Adapter<NoticeAttachFileAdapter.ViewHolder>() {

    var ctx: NoticeBoardAttachFileActivity = context as NoticeBoardAttachFileActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoticeAttachFileAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_attach_file, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: NoticeAttachFileAdapter.ViewHolder, position: Int) {
        holder.bindItems(fileList[position], ctx, from)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return fileList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(fileList: NoticeBoardFileDataPojo, ctx: NoticeBoardAttachFileActivity, from: String) {

            val filePic: ImageView = itemView.findViewById(R.id.filePic)
            val fileName: TextView = itemView.findViewById(R.id.fileName)
            val fileTime: TextView = itemView.findViewById(R.id.fileTime)
            val row_layout: LinearLayout = itemView.findViewById(R.id.row_layout)

            val lastValue = after(fileList.file!!, ".").toLowerCase()
            if (lastValue == "pdf") {
                filePic.setImageResource(R.mipmap.pdficon)
                row_layout.visibility = View.VISIBLE
            } else if (lastValue == "docx" || lastValue == "doc") {
                filePic.setImageResource(R.mipmap.texticon)
                row_layout.visibility = View.VISIBLE
            } else if (lastValue == "png" || lastValue == "jpg" || lastValue == "jpeg") {
                val url: String = if (from == "Notice")
                    ParentConstant.ATTACH_IMAGE_URL
                else
                    ParentConstant.ATTACH_DIARY_IMAGE_URL
                if (fileList.file != null && fileList.file != "") {
                    ctx.setProfileImageInLayout(ctx, url + fileList.file, filePic)
                } else {
                    filePic.setImageResource(R.mipmap.ic_launcher)
                }
                row_layout.visibility = View.VISIBLE
            } else {
                row_layout.visibility = View.GONE
            }

            fileName.text = fileList.file
            if (fileList.created != "") {
                val new_date = fileList.created.replace(".000Z", "").split("T")
                fileTime.text = ctx.changeDateTimeFormat(new_date[0] + " " + new_date[1])
            } else if (fileList.created_at != "") {
                val new_date = fileList.created_at.replace(".000Z", "").split("T")
                fileTime.text = ctx.changeDateTimeFormat(new_date[0] + " " + new_date[1])
            }
            row_layout.setTag(R.string.data, fileList)
            row_layout.setOnClickListener(ctx)
        }

        private fun after(value: String, a: String): String {
            // Returns a substring containing all characters after a string.
            val posA = value.lastIndexOf(a)
            if (posA == -1) {
                return ""
            }
            val adjustedPosA = posA + a.length
            return if (adjustedPosA >= value.length) {
                ""
            } else value.substring(adjustedPosA)
        }
    }
}