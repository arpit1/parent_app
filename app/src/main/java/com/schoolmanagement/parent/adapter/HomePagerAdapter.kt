package com.schoolmanagement.parent.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.schoolmanagement.parent.fragment.*

/**
 * Created by arpit.jain on 2/16/2018.
 */
class HomePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return HomeFragment()
            1 -> return ChatFragment()
            2 -> return SchoolDiaryFragment()
            3 -> return CalendarFragment()
            4 -> return GalleryFragment()
        }
        return null
    }

    override fun getCount(): Int {
        return 5
    }
}