package com.schoolmanagement.parent.adapter

import android.app.Dialog
import android.content.Intent
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.FullVideoScreenActivity
import com.schoolmanagement.parent.activity.GalleryDetailActivity
import com.schoolmanagement.parent.activity.GifFullScreenActivity
import com.schoolmanagement.parent.pojo.HomePostFilePojo
import com.schoolmanagement.parent.util.ParentConstant

/**
 * Created by upasna.mishra on 2/28/2018.
 */
class GalleryFileAdapter(val ctx: GalleryDetailActivity, fileList: ArrayList<HomePostFilePojo>, type: String, albumName: String) : BaseAdapter() {

    var file_list: MutableList<HomePostFilePojo> = fileList
    var uploadType: String = type
    val album_name: String = albumName

    private lateinit var galleryImage: ImageView
    private lateinit var rlVideo: RelativeLayout

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val v = LayoutInflater.from(parent!!.context).inflate(R.layout.row_layout_galleryfiles, parent, false)
        galleryImage = v.findViewById(R.id.galleryImage)
        rlVideo = v.findViewById(R.id.rlVideo)
        val lastValue = after(file_list[position].file, ".").toLowerCase()
            if(lastValue == "gif") {
                rlVideo.visibility = View.GONE
                ctx.setHomePostImageInLayout(ctx, ParentConstant.IMAGE_URl + file_list[position].file, galleryImage)
                galleryImage.setOnClickListener {
                    ctx.startActivity(Intent(ctx, GifFullScreenActivity::class.java)
                            .putExtra("gif_image", file_list[position].file)
                            .putExtra("event_name", album_name))
                }

            }else if(lastValue == "png" || lastValue == "jpg" || lastValue == "jpeg") {
                rlVideo.visibility = View.GONE
                ctx.setHomePostImageInLayout(ctx, ParentConstant.IMAGE_URl + file_list[position].file, galleryImage)
                galleryImage.setOnClickListener {
                    openImageDialog(ctx, file_list as ArrayList<HomePostFilePojo>, album_name, position)
                }
            }else if(lastValue == "mp4") {
                rlVideo.visibility = View.VISIBLE
                ctx.setHomePostImage(ctx, ParentConstant.THUMB_IMAGE_URL + file_list[position].thumbnail, galleryImage)
                rlVideo.setOnClickListener {
                    ctx.startActivity(Intent(ctx, FullVideoScreenActivity::class.java)
                            .putExtra("Image", file_list[position].file)
                            .putExtra("event_name", album_name))
                }
            }
        return v
    }

    private fun after(value: String, a: String): String {
        // Returns a substring containing all characters after a string.
        val posA = value.lastIndexOf(a)
        if (posA == -1) {
            return ""
        }
        val adjustedPosA = posA + a.length
        return if (adjustedPosA >= value.length) {
            ""
        } else value.substring(adjustedPosA)
    }

    override fun getItem(p0: Int): Any {
        return 0
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return file_list.size
    }

    private fun openImageDialog(ctx: GalleryDetailActivity, imageList: ArrayList<HomePostFilePojo>, event_name: String, pos : Int) {
        lateinit var pager: ViewPager
        lateinit var tvHeader: TextView
        lateinit var crossImage: ImageView
        lateinit var ivShareIcon: ImageView
        var image_path : String
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar)
        dialog.setContentView(R.layout.activity_home_imageshowfull)
        pager = dialog.findViewById(R.id.pager)
        crossImage = dialog.findViewById(R.id.crossImage)
        tvHeader = dialog.findViewById(R.id.tvHeader)
        ivShareIcon = dialog.findViewById(R.id.ivShareIcon)
        val tvShowImageCount : TextView = dialog.findViewById(R.id.tvShowImageCount)
        tvHeader.text = event_name
        crossImage.setOnClickListener {
            dialog.dismiss()
        }
        image_path = imageList[pos].file
        pager.adapter = HomeImageAdapter(ctx, imageList)
        pager.currentItem = pos
        tvShowImageCount.text = (pos + 1).toString()+" " + ctx.resources.getString(R.string.of) + " " + imageList.size.toString()
        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                tvShowImageCount.text = (position + 1).toString() + " " + ctx.resources.getString(R.string.of) + " " + imageList.size.toString()
                image_path = imageList[position].file
            }

            override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

            override fun onPageScrollStateChanged(arg0: Int) {}
        })
        ivShareIcon.setOnClickListener {
            val path = ParentConstant.IMAGE_URl + "" + image_path
            val share = Intent(android.content.Intent.ACTION_SEND)
            share.type = "text/plain"
            share.putExtra(Intent.EXTRA_SUBJECT, event_name)
            share.putExtra(Intent.EXTRA_TEXT, path)
            ctx.startActivity(Intent.createChooser(share, "Share link!"))
        }
        if (!ctx.isFinishing)
            dialog.show()
    }
}