package com.schoolmanagement.parent.adapter

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.FeedbackActivity
import com.schoolmanagement.parent.activity.FeedbackDetailActivity
import com.schoolmanagement.parent.pojo.FeedbackDataPojo
import com.schoolmanagement.parent.util.ParentConstant

/**
 * Created by upasna.mishra on 2/16/2018.
 */
class FeedbackAdapter(val context: Activity, private val feedbackList: ArrayList<FeedbackDataPojo>) : RecyclerView.Adapter<FeedbackAdapter.ViewHolder>() {

    var ctx: FeedbackActivity = context as FeedbackActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedbackAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_feedback_layout, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: FeedbackAdapter.ViewHolder, position: Int) {
        holder.bindItems(feedbackList[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return feedbackList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(feedback_list: FeedbackDataPojo, ctx: FeedbackActivity) {

            val schoolProfilePic: ImageView = itemView.findViewById(R.id.schoolProfilePic)
            val topicName: TextView = itemView.findViewById(R.id.topicName)
            val feedbackDateTime: TextView = itemView.findViewById(R.id.feedbackDateTime)
            val studentName: TextView = itemView.findViewById(R.id.studentName)
            val teacherName: TextView = itemView.findViewById(R.id.teacherName)
            val description: TextView = itemView.findViewById(R.id.description)
            val rowFeedback: LinearLayout = itemView.findViewById(R.id.rowFeedback)

            if (feedback_list.created != "") {
                val new_date = feedback_list.created.replace(".000Z", "").split("T")
                feedbackDateTime.text = ctx.changeDateTimeFormat(new_date[0] + " " + new_date[1])
            }

            if (feedback_list.profile_pic != null && feedback_list.profile_pic != "") {
                ctx.setProfileImageInLayout(ctx, ParentConstant.IMAGE_TEACHER_PROFILE + feedback_list.profile_pic, schoolProfilePic)
            } else {
                schoolProfilePic.setImageResource(R.mipmap.usericon)
            }

            topicName.text = feedback_list.topic
            studentName.text = ctx.resources.getString(R.string.student_name) + ": " + ctx.getStudentName()
            teacherName.text = ctx.resources.getString(R.string.show_teacher_name) + ": " + ctx.getTeacherName(feedback_list)
            description.text = feedback_list.description

            rowFeedback.setOnClickListener {
                ctx.startActivity(Intent(ctx, FeedbackDetailActivity::class.java).putExtra("feedback_detail", feedback_list))
            }
        }
    }
}