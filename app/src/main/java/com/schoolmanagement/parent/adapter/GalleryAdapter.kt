package com.schoolmanagement.parent.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.GalleryDetailActivity
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.pojo.HomePostDataPojo
import com.schoolmanagement.parent.util.ParentConstant

/**
 * Created by upasna.mishra on 2/27/2018.
 */
class GalleryAdapter(var ctx: HomeActivity, galleryList: ArrayList<HomePostDataPojo>) : BaseAdapter() {

    private var galleryList: MutableList<HomePostDataPojo> = galleryList

    private lateinit var galleryImage: ImageView
    private lateinit var galleryName: TextView
    private lateinit var gallerTime: TextView
    private lateinit var tvGalleryDes: TextView
    private lateinit var rowClick: RelativeLayout

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val v = LayoutInflater.from(parent!!.context).inflate(R.layout.row_gallery_activity, parent, false)
        galleryImage = v.findViewById(R.id.galleryImage)
        galleryName = v.findViewById(R.id.galleryName)
        gallerTime = v.findViewById(R.id.gallerTime)
        tvGalleryDes = v.findViewById(R.id.tvGalleryDes)
        rowClick = v.findViewById(R.id.rowClick)
        if (galleryList[position].file != null && galleryList[position].file!!.size > 0) {
            if (galleryList[position].upload_type == "Images" || galleryList[position].upload_type == "Image") {
                ctx.setHomePostImageInLayout(ctx, ParentConstant.IMAGE_URl + galleryList[position].file!![0].file, galleryImage)
            } else if (galleryList[position].upload_type == "Video") {
                ctx.setHomePostImage(ctx, ParentConstant.THUMB_IMAGE_URL + galleryList[position].file!![0].thumbnail, galleryImage)
            }
        }
        galleryName.text = galleryList[position].event_name
        if (galleryList[position].dates != null)
            gallerTime.text = ctx.getChangedDate(galleryList[position].dates!!)
        tvGalleryDes.text = galleryList[position].discussion

        rowClick.setOnClickListener {
            if (galleryList[position].file!!.size > 0) {
                ctx.startActivity(Intent(ctx, GalleryDetailActivity::class.java).putExtra("galleryData", galleryList[position]))
            }
        }
        return v
    }

    override fun getItem(p0: Int): Any {
        return 0
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return galleryList.size
    }
}