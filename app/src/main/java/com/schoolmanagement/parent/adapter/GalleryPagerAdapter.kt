package com.schoolmanagement.parent.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.schoolmanagement.parent.fragment.ActivitiesFragment
import com.schoolmanagement.parent.fragment.AssignmentFragment

/**
 * Created by upasna.mishra on 2/27/2018.
 */
class GalleryPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return AssignmentFragment()
            1 -> return ActivitiesFragment()
        }
        return null
    }

    override fun getCount(): Int {
        return 2
    }
}