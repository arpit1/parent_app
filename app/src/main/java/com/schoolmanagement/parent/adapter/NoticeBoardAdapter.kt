package com.schoolmanagement.parent.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.NoticeBoardActivity
import com.schoolmanagement.parent.activity.NoticeBoardDetailActivity
import com.schoolmanagement.parent.pojo.NoticeBoardDataPojo
import com.schoolmanagement.parent.util.ParentConstant
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by upasna.mishra on 2/21/2018.
 */
class NoticeBoardAdapter(val context: Activity, val noticeBoardList: ArrayList<NoticeBoardDataPojo>) : RecyclerView.Adapter<NoticeBoardAdapter.ViewHolder>() {

    var ctx: NoticeBoardActivity = context as NoticeBoardActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoticeBoardAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_noticeboard, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: NoticeBoardAdapter.ViewHolder, position: Int) {
        holder.bindItems(noticeBoardList[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return noticeBoardList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("ResourceAsColor")
        fun bindItems(noticeBoardList : NoticeBoardDataPojo, ctx: NoticeBoardActivity) {

            val llNormalNotice : LinearLayout = itemView.findViewById(R.id.llNormalNotice)
            val adminProfile : CircleImageView = itemView.findViewById(R.id.adminProfile)
            val noticeTitle : TextView = itemView.findViewById(R.id.noticeTitle)
            val noticeTime : TextView = itemView.findViewById(R.id.noticeTime)
            val description : TextView = itemView.findViewById(R.id.descriptionNotice)
            val alertIcon : ImageView = itemView.findViewById(R.id.alertIcon)

            if(noticeBoardList.type == "0"){
                llNormalNotice.setBackgroundColor(ContextCompat.getColor(ctx, R.color.transparent))
                noticeTitle.setTextColor(ContextCompat.getColor(ctx, R.color.black))
                noticeTime.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
                description.setTextColor(ContextCompat.getColor(ctx, R.color.drawer_fragment_grey))
                alertIcon.visibility = View.GONE
                itemView.setOnClickListener {
                    ctx.removeActivity("NoticeBoardDetailActivity")
                    ctx.startActivity(Intent(ctx, NoticeBoardDetailActivity::class.java).putExtra("noticeBoardList", noticeBoardList))
                }
            }else{
                llNormalNotice.setBackgroundColor(Color.parseColor("#9B1617"))
                noticeTitle.setTextColor(ContextCompat.getColor(ctx, R.color.white))
                noticeTime.setTextColor(ContextCompat.getColor(ctx, R.color.white))
                description.setTextColor(ContextCompat.getColor(ctx, R.color.white))
                alertIcon.visibility = View.VISIBLE
            }

            if (noticeBoardList.logo != null && noticeBoardList.logo != "") {
                ctx.setProfileImageInLayout(ctx, ParentConstant.SCHOOL_PROFILE_PIC + noticeBoardList.logo, adminProfile)
            } else {
                adminProfile.setImageResource(R.mipmap.usericon)
            }
            noticeTitle.text = noticeBoardList.title
            noticeTime.text = noticeBoardList.created
            description.text = noticeBoardList.description
        }
    }
}