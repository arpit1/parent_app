package com.schoolmanagement.parent.adapter

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.EventDetailActivity
import com.schoolmanagement.parent.activity.GalleryDetailActivity
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.pojo.HomePostDataPojo
import com.schoolmanagement.parent.util.ParentConstant
import de.hdodenhof.circleimageview.CircleImageView


/**
 * Created by upasna.mishra on 2/15/2018.
 */
class HomePostAdapter(val context: Activity, private val home_post_list: ArrayList<HomePostDataPojo>?) : RecyclerView.Adapter<HomePostAdapter.ViewHolder>() {

    var ctx: HomeActivity = context as HomeActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomePostAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_home_layout, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: HomePostAdapter.ViewHolder, position: Int) {
        holder.bindItems(home_post_list!![position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return home_post_list!!.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(home_post: HomePostDataPojo, ctx: HomeActivity) {
            val profilePic: CircleImageView = itemView.findViewById(R.id.profilePic)
            val userName: TextView = itemView.findViewById(R.id.userName)
            val dateTime: TextView = itemView.findViewById(R.id.dateTime)
            val description: TextView = itemView.findViewById(R.id.description)
            val postImage: ImageView = itemView.findViewById(R.id.postImage)
            val llVideo: LinearLayout = itemView.findViewById(R.id.llVideo)
            val videoIcon: ImageView = itemView.findViewById(R.id.videoIcon)
            val imageLayout: RelativeLayout = itemView.findViewById(R.id.imageLayout)
            userName.text = home_post.event_name
            description.text = home_post.discussion

            if (home_post.created_at != "") {
                val newDate = home_post.created_at.replace(".000Z", "").split("T")
                dateTime.text = ctx.changeDateTimeFormat(newDate[0] + " " + newDate[1])
            }

            if (home_post.profile_pic != null && home_post.profile_pic != "") {
                if (home_post.uploaded_by == "0")
                    ctx.setProfileImageInLayout(ctx, ParentConstant.SCHOOL_PROFILE_PIC + home_post.profile_pic, profilePic)
                else
                    ctx.setProfileImageInLayout(ctx, ParentConstant.IMAGE_TEACHER_PROFILE + home_post.profile_pic, profilePic)
            } else {
                profilePic.setImageResource(R.mipmap.usericon)
            }

            if (home_post.file != null && home_post.file!!.size > 0) {
                val lastValue = after(home_post.file!![0].file, ".").toLowerCase()
                if (lastValue == "gif") {
                    videoIcon.visibility = View.GONE
                    llVideo.visibility = View.GONE
                    postImage.visibility = View.VISIBLE
                    if (home_post.type == "Events")
                        ctx.setGifImage(ctx, ParentConstant.EVENT_IMAGE_URL + home_post.file!![0].file, postImage)
                    else
                        ctx.setGifImage(ctx, ParentConstant.IMAGE_URl + home_post.file!![0].file, postImage)
                } else if (lastValue == "png" || lastValue == "jpg" || lastValue == "jpeg") {
                    videoIcon.visibility = View.GONE
                    llVideo.visibility = View.GONE
                    if (home_post.type == "Events")
                        ctx.setHomePostImageInLayout(ctx, ParentConstant.EVENT_IMAGE_URL + home_post.file!![0].file, postImage)
                    else
                        ctx.setHomePostImageInLayout(ctx, ParentConstant.IMAGE_URl + home_post.file!![0].file, postImage)
                } else if (lastValue == "mp4") {
                    videoIcon.visibility = View.VISIBLE
                    llVideo.visibility = View.VISIBLE
                    ctx.setHomePostImage(ctx, ParentConstant.THUMB_IMAGE_URL + home_post.file!![0].thumbnail, postImage)
                }
            }
            if (home_post.type == "Events") {
                if (home_post.file != null && home_post.file!!.size > 0 && home_post.file!![0].file == "") {
                    imageLayout.visibility = View.GONE
                    description.text = home_post.registrationDetails
                } else {
                    imageLayout.visibility = View.VISIBLE
                    description.text = home_post.details
                }
                itemView.setOnClickListener {
                    ctx.startActivity(Intent(ctx, EventDetailActivity::class.java).putExtra("event_id", home_post.id))
                }
            } else {
                imageLayout.visibility = View.VISIBLE
                itemView.setOnClickListener {
                    ctx.startActivity(Intent(ctx, GalleryDetailActivity::class.java).putExtra("galleryData", home_post).putExtra("from", "home"))
                }
            }
        }

        private fun after(value: String, a: String): String {
            // Returns a substring containing all characters after a string.
            val posA = value.lastIndexOf(a)
            if (posA == -1) {
                return ""
            }
            val adjustedPosA = posA + a.length
            return if (adjustedPosA >= value.length) {
                ""
            } else value.substring(adjustedPosA)
        }

    }


}