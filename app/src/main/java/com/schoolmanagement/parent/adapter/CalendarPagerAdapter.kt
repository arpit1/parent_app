package com.schoolmanagement.parent.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.schoolmanagement.parent.fragment.EventFragment
import com.schoolmanagement.parent.fragment.HolidaysFragment
import com.schoolmanagement.parent.fragment.LeavesFragment
import com.schoolmanagement.parent.fragment.TasksFragment

/**
 * Created by arpit.jain on 2/16/2018.
 */
class CalendarPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return TasksFragment()
            1 -> return EventFragment()
            2 -> return HolidaysFragment()
            3 -> return LeavesFragment()
        }
        return null
    }

    override fun getCount(): Int {
        return 4
    }
}