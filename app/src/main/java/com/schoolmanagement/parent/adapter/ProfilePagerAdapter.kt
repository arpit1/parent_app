package com.schoolmanagement.parent.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.schoolmanagement.parent.fragment.FeeFragment
import com.schoolmanagement.parent.fragment.ProfileFragment

/**
 * Created by arpit.jain on 2/21/2018.
 */
class ProfilePagerAdapter (fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return ProfileFragment()
            1 -> return FeeFragment()
        }
        return null
    }

    override fun getCount(): Int {
        return 2
    }
}