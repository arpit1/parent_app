package com.schoolmanagement.parent.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.pojo.UserListPojo
import com.schoolmanagement.parent.util.ParentConstant
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONObject
import java.util.*

/**
 * Created by hitesh.mathur on 3/27/2018.
 */
class ChatContactAdapter(val context: Activity, private var teacherList: ArrayList<UserListPojo>) : RecyclerView.Adapter<ChatContactAdapter.ViewHolder>() {

    var ctx: HomeActivity = context as HomeActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatContactAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_contact_layout, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ChatContactAdapter.ViewHolder, position: Int) {
        holder.bindItems(teacherList[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return teacherList.size
    }

    //the class is holding the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("ResourceAsColor")
        fun bindItems(teacherList: UserListPojo, ctx: HomeActivity) {

            val teacherProfilePic: CircleImageView = itemView.findViewById(R.id.teacherProfilePic)
            val teacherName: TextView = itemView.findViewById(R.id.teacherName)
            val qualification: TextView = itemView.findViewById(R.id.qualification)
            val lastMsg: TextView = itemView.findViewById(R.id.lastMsg)
            val image: ImageView = itemView.findViewById(R.id.image)
            val lastMsgTime: TextView = itemView.findViewById(R.id.lastMsgTime)
            val chatcount: TextView = itemView.findViewById(R.id.chatcount)
            qualification.visibility = View.VISIBLE
            if (teacherList.profile_pic != null && teacherList.profile_pic != "") {
                if (teacherList.type == "0")
                    ctx.setProfileImageInLayout(ctx, ParentConstant.IMAGE_TEACHER_PROFILE + teacherList.profile_pic, teacherProfilePic)
                else
                    ctx.setProfileImageInLayout(ctx, ParentConstant.SCHOOL_PROFILE_PIC + teacherList.profile_pic, teacherProfilePic)
            } else {
                teacherProfilePic.setImageResource(R.mipmap.usericon)
            }

            if (teacherList.message_counter > 0) {
                chatcount.visibility = View.VISIBLE
                chatcount.text = teacherList.message_counter.toString()
            } else {
                chatcount.visibility = View.GONE
            }
            // teacherName.text = teacherList.fname
            if (teacherList.type == "0") {
                teacherName.setTextColor(ContextCompat.getColor(ctx, R.color.black))
                setTeacherName(teacherList, teacherName)
            }
            else {
                teacherName.setTextColor(ContextCompat.getColor(ctx, R.color.login_button_color))
                teacherName.text = teacherList.fname
            }
            if (teacherList.isConnected == "1") {
                qualification.text = context.getString(R.string.online)
                qualification.setTextColor(ContextCompat.getColor(ctx, R.color.selected_gray))
            } else {
                qualification.text = context.getString(R.string.offline)
                qualification.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
            }

            val arr: JSONObject = teacherList.last_msg!!
            lastMsgTime.visibility = View.VISIBLE
            if (arr.optString("isSend") == "1") {
                if (arr.optString("isRead") == "0")
                    lastMsgTime.setTextColor(ContextCompat.getColor(ctx, R.color.selected_gray))
                else
                    lastMsgTime.setTextColor(ContextCompat.getColor(ctx, R.color.grey))
            }

            when {
                DateUtils.isToday(ctx.getDate(arr.optString("dates")).time) -> lastMsgTime.text = arr.optString("time")
                DateUtils.isToday(ctx.getDate(arr.optString("dates")).time + DateUtils.DAY_IN_MILLIS) -> lastMsgTime.text = ctx.resources.getString(R.string.yesterday)
                else -> lastMsgTime.text = arr.optString("dates")
            }
            lastMsg.visibility = View.VISIBLE
            when {
                arr.optString("type") == "text" -> {
                    image.visibility = View.GONE
                    lastMsg.text = ctx.decode(arr.optString("message"))
                }
                arr.optString("type") == "video" -> {
                    image.visibility = View.VISIBLE
                    lastMsg.text = ctx.resources.getString(R.string.video)
                }
                arr.optString("type") == "image" -> {
                    image.visibility = View.VISIBLE
                    lastMsg.text = ctx.resources.getString(R.string.photo)
                }
            }
        }

        private fun setTeacherName(teacherData: UserListPojo, teacherName: TextView) {
            if (teacherData.mname != "")
                teacherName.text = teacherData.fname + " " + teacherData.mname + " " + teacherData.lname
            else
                teacherName.text = teacherData.fname + " " + teacherData.lname
        }
    }
}