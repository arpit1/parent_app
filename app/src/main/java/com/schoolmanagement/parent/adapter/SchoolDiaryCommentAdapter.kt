package com.schoolmanagement.parent.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.NoticeBoardAttachFileActivity
import com.schoolmanagement.parent.pojo.CommentDataPojo
import com.schoolmanagement.parent.util.ParentConstant
import de.hdodenhof.circleimageview.CircleImageView

class SchoolDiaryCommentAdapter(val context: Activity, val feedbackCommentList: ArrayList<CommentDataPojo>,
                             val logo: String, val school_name: String) : RecyclerView.Adapter<SchoolDiaryCommentAdapter.ViewHolder>() {

    var ctx: NoticeBoardAttachFileActivity = context as NoticeBoardAttachFileActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolDiaryCommentAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_commentfeedback_parent, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: SchoolDiaryCommentAdapter.ViewHolder, position: Int) {
        holder.bindItems(ctx, feedbackCommentList[position], feedbackCommentList.size, position, logo, school_name)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return feedbackCommentList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(ctx: NoticeBoardAttachFileActivity, feedBackCommentList: CommentDataPojo,
                      sizeList: Int, position: Int, logo: String?, school_name: String) {

            val tvTime: TextView = itemView.findViewById(R.id.tvTime)
            val userName: TextView = itemView.findViewById(R.id.userName)
            val tvMessage: TextView = itemView.findViewById(R.id.tvMessage)
            val ivEdit: ImageView = itemView.findViewById(R.id.ivEdit)
            val ivDelete: ImageView = itemView.findViewById(R.id.ivDelete)
            val userImage: CircleImageView = itemView.findViewById(R.id.userImage)
            val ll_parent_commnet: LinearLayout = itemView.findViewById(R.id.ll_parent_commnet)
            val ll_admin_comment: LinearLayout = itemView.findViewById(R.id.ll_admin_comment)
            val adminProfile: CircleImageView = itemView.findViewById(R.id.adminProfile)
            val schoolName: TextView = itemView.findViewById(R.id.schoolName)
            val adminTime: TextView = itemView.findViewById(R.id.adminTime)
            val adminMessage: TextView = itemView.findViewById(R.id.adminMessage)

            if (feedBackCommentList.type == "1") {
                ll_parent_commnet.visibility = View.VISIBLE
                ll_admin_comment.visibility = View.GONE
                if (feedBackCommentList.created_at != "") {
                    val new_date = feedBackCommentList.created_at.replace(".000Z", "").split("T")
                    tvTime.text = ctx.changeDateTimeFormat(new_date[0] + " " + new_date[1])
                }

                tvMessage.text = feedBackCommentList.comment
                if (feedBackCommentList.p_profile_pic != null && feedBackCommentList.p_profile_pic != "") {
                    ctx.setProfileImageInLayout(ctx, ParentConstant.IMAGE_STUDENT_PROFILE + feedBackCommentList.p_profile_pic, userImage)
                } else {
                    userImage.setImageResource(R.mipmap.usericon)
                }
                userName.text = feedBackCommentList.p_name
                val pos = position + 1
                if (sizeList == pos) {
                    ivEdit.visibility = View.VISIBLE
                    ivDelete.visibility = View.VISIBLE
                } else {
                    ivEdit.visibility = View.GONE
                    ivDelete.visibility = View.GONE
                }
                ivEdit.setOnClickListener(ctx)
                ivEdit.setTag(R.string.edit_msg, feedBackCommentList)
                ivEdit.setTag(R.string.edit_pos, position)
                ivEdit.setTag(R.string.edit_from, "Edit")

                ivDelete.setOnClickListener(ctx)
                ivDelete.setTag(R.string.edit_msg, feedBackCommentList)
                ivDelete.setTag(R.string.edit_pos, position)

            } else {
                ll_parent_commnet.visibility = View.GONE
                ll_admin_comment.visibility = View.VISIBLE
                if (feedBackCommentList.created_at != "") {
                    val new_date = feedBackCommentList.created_at.replace(".000Z", "").split("T")
                    adminTime.text = ctx.changeDateTimeFormat(new_date[0] + " " + new_date[1])
                }

                adminMessage.text = feedBackCommentList.comment
                schoolName.text = feedBackCommentList.t_name
                if (feedBackCommentList.t_profile_pic != null && feedBackCommentList.t_profile_pic != "") {
                    ctx.setProfileImageInLayout(ctx, ParentConstant.IMAGE_TEACHER_PROFILE + feedBackCommentList.t_profile_pic, adminProfile)
                } else {
                    adminProfile.setImageResource(R.mipmap.usericon)
                }
            }
        }
    }
}