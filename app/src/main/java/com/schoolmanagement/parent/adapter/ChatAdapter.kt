package com.schoolmanagement.parent.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.ChatScreenActivity
import com.schoolmanagement.parent.pojo.ChatPojo
import java.util.*

/**
 * Created by hitesh.mathur on 3/27/2018.
 */
class ChatAdapter(private val context: ChatScreenActivity, chatlist: ArrayList<ChatPojo>, selectedIds: ArrayList<ChatPojo>) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {
    var chatList = ArrayList<ChatPojo>()
    var selected_usersList = ArrayList<ChatPojo>()

    init {
        this.chatList = chatlist
        this.selected_usersList = selectedIds
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatAdapter.ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val view: View
        val viewholder: ViewHolder

        if (viewType == MESSAGE_SENT) {
            view = inflater.inflate(R.layout.chat_activity_list_itemright, parent, false)
            viewholder = ViewHolder(view)
        } else {
            view = inflater.inflate(R.layout.chat_activity_list_item, parent, false)
            viewholder = ViewHolder(view)
        }
        return viewholder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val chatItem = chatList[position]
        holder.txtChatLeft.text = chatItem.message
        holder.txtdatetime.text = chatItem.time

        if (position == 0) {
            holder.dateVal.text = getText(chatItem.dates!!)
            holder.dateVal.visibility = View.VISIBLE
        } else if (position > 0 && !chatItem.dates.equals("")) {
            if (chatItem.dates != chatList[position - 1].dates) {
                holder.dateVal.text = getText(chatItem.dates!!)
                holder.dateVal.visibility = View.VISIBLE
            } else {
                holder.dateVal.visibility = View.GONE
            }
        }
        if (chatItem.type!!.equals("image", ignoreCase = true) || chatItem.type!!.equals("video", ignoreCase = true)) {
            if (chatItem.type!!.equals("video", ignoreCase = true)) {
                holder.videoIcon.visibility = View.VISIBLE
            } else {
                holder.videoIcon.visibility = View.GONE
            }
            holder.left_image.visibility = View.VISIBLE
            holder.left_ly.visibility = View.GONE

            holder.left_image.setTag(R.string.data, chatItem)
            holder.left_image.setOnClickListener(context)

            holder.left_image_time.text = chatItem.time
            holder.image_receive.setImageBitmap(decodeBase64(chatItem.image_thumb))
            setTickIcon(holder.left_image_time, chatItem)
        } else {
            holder.left_image.visibility = View.GONE
            holder.left_ly.visibility = View.VISIBLE
            setTickIcon(holder.txtdatetime, chatItem)
        }
    }

    private fun getText(date: String): String {
        return when {
            DateUtils.isToday(context.getDate(date).time) -> context.resources.getString(R.string.today)
            DateUtils.isToday(context.getDate(date).time + DateUtils.DAY_IN_MILLIS) -> context.resources.getString(R.string.yesterday)
            else -> date
        }
    }

    private fun setTickIcon(textView: TextView, chatItem: ChatPojo) {
        if (chatItem.isSend == "0" && chatItem.isRead == "2") {
            setCompoundDrawableText(textView, R.mipmap.chat_unread_singleicon)
        } else if (chatItem.isSend == "0" && chatItem.isRead == "0") {
            setCompoundDrawableText(textView, R.mipmap.chat_unread_icon)
        } else if (chatItem.isSend == "0" && chatItem.isRead == "1") {
            setCompoundDrawableText(textView, R.mipmap.chat_read_icon)
        }
    }

    private fun setCompoundDrawableText(messageTime: TextView, image: Int) {
        messageTime.setCompoundDrawablesWithIntrinsicBounds(0, 0, image, 0)
    }

    override fun getItemCount(): Int {
        return chatList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (chatList[position].message_side == "right") {
            MESSAGE_SENT
        } else {
            MESSAGE_RECEIVED
        }
    }

    private fun decodeBase64(input: String?): Bitmap {
        val decodedBytes = Base64.decode(input, 0)
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal val txtChatLeft: TextView
        internal val txtdatetime: TextView
        internal val dateVal: TextView
        internal val left_image_time: TextView
        internal val left_ly: LinearLayout
        internal val mainlayout: RelativeLayout
        internal val left_image: LinearLayout
        internal val image_receive: ImageView
        internal val videoIcon: ImageView

        init {
            left_ly = view.findViewById(R.id.left_ly)
            left_image = view.findViewById(R.id.left_image)
            txtChatLeft = view.findViewById(R.id.lefttext)
            txtdatetime = view.findViewById(R.id.left_datetime_tv)
            dateVal = view.findViewById(R.id.dateVal)
            mainlayout = view.findViewById(R.id.mainlayout)
            left_image_time = view.findViewById(R.id.left_image_time)
            image_receive = view.findViewById(R.id.image_receive)
            videoIcon = view.findViewById(R.id.videoIcon)
        }
    }

    companion object {
        private const val MESSAGE_SENT = 1
        private const val MESSAGE_RECEIVED = 2
    }
}
