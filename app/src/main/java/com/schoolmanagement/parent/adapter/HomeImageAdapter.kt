package com.schoolmanagement.parent.adapter

import android.app.Activity
import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.BaseActivity
import com.schoolmanagement.parent.pojo.HomePostFilePojo
import com.schoolmanagement.parent.util.ParentConstant

/**
 * Created by upasna.mishra on 2/14/2018.
 */
class HomeImageAdapter(ctx: Activity, imageList: ArrayList<HomePostFilePojo>) : PagerAdapter() {

    var ctx_home: Activity = ctx
    var image_list = imageList
    private lateinit var inflater: LayoutInflater

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {
        inflater = ctx_home.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.activity_home_image, container, false)
        val ivPostImage = itemView.findViewById<ImageView>(R.id.ivPostImage)
        (ctx_home as BaseActivity).setHomePostImage(ctx_home, ParentConstant.IMAGE_URl + image_list[position].file, ivPostImage)
        container!!.addView(itemView)
        return itemView
    }

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return image_list.size
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container!!.removeView(`object` as View)
    }
}