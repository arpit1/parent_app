package com.schoolmanagement.parent.adapter

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.activity.NoticeBoardAttachFileActivity
import com.schoolmanagement.parent.pojo.SchoolDiaryDataPojo
import com.schoolmanagement.parent.util.ParentConstant
import de.hdodenhof.circleimageview.CircleImageView

class SchoolDiaryAdapter(val context: Activity, private val diary_list: ArrayList<SchoolDiaryDataPojo>) : RecyclerView.Adapter<SchoolDiaryAdapter.ViewHolder>() {

    var ctx: HomeActivity = context as HomeActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolDiaryAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_diary, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: SchoolDiaryAdapter.ViewHolder, position: Int) {
        holder.bindItems(diary_list[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return diary_list.size
    }

    //the class is holding the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(diary: SchoolDiaryDataPojo, ctx: HomeActivity) {
            val teacherProfilePic: CircleImageView = itemView.findViewById(R.id.teacherProfilePic)
            val topicName: TextView = itemView.findViewById(R.id.topicName)
            val grade: TextView = itemView.findViewById(R.id.grade)
            val time: TextView = itemView.findViewById(R.id.time)
            val description: TextView = itemView.findViewById(R.id.description)
            val teacherName: TextView = itemView.findViewById(R.id.teacherName)
            val qualification: TextView = itemView.findViewById(R.id.qualification)

            if (diary.profile_pic != null && diary.profile_pic != "") {
                ctx.setProfileImageInLayout(ctx, ParentConstant.IMAGE_TEACHER_PROFILE + diary.profile_pic, teacherProfilePic)
            } else {
                teacherProfilePic.setImageResource(R.mipmap.usericonprofile)
            }
            topicName.text = diary.title
            grade.text = diary.classes
            description.text = diary.description
            time.text = diary.time

            val teacherNameVal = if (diary.mname == "") {
                diary.fname + " " + diary.lname
            } else {
                diary.fname + " " + diary.mname + " " + diary.lname
            }

            teacherName.text = teacherNameVal
            qualification.text = diary.qualification
            itemView.setOnClickListener {
                ctx.startActivity(Intent(ctx, NoticeBoardAttachFileActivity::class.java)
                        .putExtra("from", "Diary")
                        .putExtra("diary_id", diary.id)
                        .putExtra("teacher_id", diary.teacher_id)
                        .putExtra("desc", diary.description)
                        .putExtra("header", diary.title))
            }
        }
    }
}