package com.schoolmanagement.parent.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.NotificationActivity
import com.schoolmanagement.parent.pojo.NotificationDataPojo
import com.schoolmanagement.parent.util.ParentConstant
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by upasna.mishra on 3/29/2018.
 */
class NotificationAdapter(val context: Activity, var notificationList: ArrayList<NotificationDataPojo>) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    var ctx: NotificationActivity = context as NotificationActivity

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_notification, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: NotificationAdapter.ViewHolder, position: Int) {
        holder.bindItems(notificationList[position], ctx)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return notificationList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(notificationData: NotificationDataPojo, ctx: NotificationActivity) {
            val iv_profile_pic: CircleImageView = itemView.findViewById(R.id.iv_profile_pic)
            val tv_description: TextView = itemView.findViewById(R.id.tv_description)
            val createdAt: TextView = itemView.findViewById(R.id.createdAt)
            val type: TextView = itemView.findViewById(R.id.type)
            val row_click: LinearLayout = itemView.findViewById(R.id.row_click)

            when {
                notificationData.teacher_id == "0" -> ctx.setProfileImageInLayout(ctx, ParentConstant.SCHOOL_PROFILE_PIC + notificationData.logo, iv_profile_pic)
                notificationData.school_id == "0" -> ctx.setProfileImageInLayout(ctx, ParentConstant.IMAGE_TEACHER_PROFILE + notificationData.profile_pic, iv_profile_pic)
                else -> iv_profile_pic.setImageResource(R.mipmap.usericon)
            }

            val create_date = notificationData.created.split("T").toMutableList()
            val created_time = create_date[1].split(".").toMutableList()
            createdAt.text = ctx.changeDateTimeFormat(create_date[0] + " " + created_time[0])

            tv_description.text = notificationData.title

            type.text = notificationData.type

            row_click.setTag(R.string.data, notificationData)
            row_click.setOnClickListener(ctx)
        }
    }
}