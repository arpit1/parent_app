package com.schoolmanagement.parent.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.adapter.NoticeBoardAdapter
import com.schoolmanagement.parent.pojo.NoticeBoardDataPojo
import com.schoolmanagement.parent.pojo.NoticeBoardPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by arpit.jain on 2/16/2018.
 */
class NoticeBoardActivity : BaseActivity() {

    private val ctx: NoticeBoardActivity = this
    private lateinit var rvNoticBoard: RecyclerView
    private lateinit var noDataAvailable: TextView
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var adapter: NoticeBoardAdapter
    private var dialog: ParentDialog? = null
    private var noticeBoardList: ArrayList<NoticeBoardDataPojo>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notice_board)

        ParentConstant.ACTIVITIES.add(ctx)
        setDrawerAndToolbar(resources.getString(R.string.notice_board))
        initialize()
    }

    private fun initialize() {
        noticeBoardList = ArrayList()
        dialog = ParentDialog(ctx)
        mLayoutManager = LinearLayoutManager(ctx)
        rvNoticBoard = findViewById(R.id.rvNoticBoard)
        noDataAvailable = findViewById(R.id.noDataAvailable)
        rvNoticBoard.layoutManager = mLayoutManager
        noticeBoardList()
    }

    private fun noticeBoardList() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<NoticeBoardPojo> = apiService.getNoticeBoard(getFromPrefs(ParentConstant.SCHOOL_ID).toString(), getStudentData()!!.program, getStudentData()!!.class_id)
            call.enqueue(object : Callback<NoticeBoardPojo> {
                override fun onResponse(call: Call<NoticeBoardPojo>, response: Response<NoticeBoardPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            if (response.body()!!.data.size > 0) {
                                rvNoticBoard.visibility = View.VISIBLE
                                noDataAvailable.visibility = View.GONE
                                noticeBoardList!!.addAll(response.body()!!.data)
                                adapter = NoticeBoardAdapter(ctx, noticeBoardList!!)
                                rvNoticBoard.adapter = adapter
                            } else {
                                rvNoticBoard.visibility = View.GONE
                                noDataAvailable.visibility = View.VISIBLE
                            }
                        } else {
                            ctx.displayToast(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<NoticeBoardPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }
}