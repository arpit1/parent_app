package com.schoolmanagement.parent.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.adapter.NoticeAttachFileAdapter
import com.schoolmanagement.parent.adapter.SchoolDiaryCommentAdapter
import com.schoolmanagement.parent.pojo.*
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*


/**
 * Created by upasna.mishra on 2/22/2018.
 */
class NoticeBoardAttachFileActivity : BaseActivity(), View.OnClickListener {

    private var ctx: NoticeBoardAttachFileActivity = this
    private lateinit var rvAttachFile: RecyclerView
    private lateinit var rvCommentList: RecyclerView
    private lateinit var noDataAvailable: TextView
    private lateinit var descSchoolDiary: TextView
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var commentLayoutManager: LinearLayoutManager
    private lateinit var fileList: ArrayList<NoticeBoardFileDataPojo>
    private lateinit var commentList: ArrayList<CommentDataPojo>
    private lateinit var adapter: NoticeAttachFileAdapter
    private var folder: File? = null
    private var file_name: String? = null
    private lateinit var data: NoticeBoardFileDataPojo
    private var dialog: ParentDialog? = null
    private var from: String = ""
    private var d: ProgressDialog? = null
    private var commentAdapter: SchoolDiaryCommentAdapter? = null
    private lateinit var etComment: EditText
    private lateinit var sendComment: ImageView
    private lateinit var feedback_comment: CommentDataPojo
    private var from_comment: String = "Add"
    private var pos: Int = 0
    private lateinit var ll_comment_section: LinearLayout
    private lateinit var ll_add_comment_section: LinearLayout
    private lateinit var ll_attach_section: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_noticeboard_attachfile)
        from = intent.getStringExtra("from")
        ParentConstant.ACTIVITIES.add(ctx)
        initialize()
        setListener()
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        commentList = ArrayList()
        mLayoutManager = LinearLayoutManager(ctx)
        commentLayoutManager = LinearLayoutManager(ctx)
        rvAttachFile = findViewById(R.id.rvAttachFile)
        rvCommentList = findViewById(R.id.rvCommentList)
        noDataAvailable = findViewById(R.id.noDataAvailable)
        descSchoolDiary = findViewById(R.id.descSchoolDiary)
        etComment = findViewById(R.id.etComment)
        sendComment = findViewById(R.id.sendComment)
        ll_comment_section = findViewById(R.id.ll_comment_section)
        ll_add_comment_section = findViewById(R.id.ll_add_comment_section)
        ll_attach_section = findViewById(R.id.ll_attach_section)
        if (from == "Notice") {
            ll_attach_section.visibility = View.VISIBLE
            ll_comment_section.visibility = View.GONE
            ll_add_comment_section.visibility = View.GONE
            descSchoolDiary.visibility = View.GONE
            setHeader(resources.getString(R.string.attach_file))
            fileList = intent.getSerializableExtra("fileList") as ArrayList<NoticeBoardFileDataPojo>
            adapter = NoticeAttachFileAdapter(ctx, fileList, from)
            rvAttachFile.adapter = adapter
        } else {
            hideSoftKeyboard()
            ll_add_comment_section.visibility = View.VISIBLE
            setHeader(intent.getStringExtra("header"))
            descSchoolDiary.visibility = View.VISIBLE
            descSchoolDiary.movementMethod = ScrollingMovementMethod()
            descSchoolDiary.text = intent.getStringExtra("desc")
            getDiaryDetail()
        }
        rvAttachFile.layoutManager = mLayoutManager
        rvCommentList.layoutManager = commentLayoutManager

    }

    private fun setListener() {
        sendComment.setOnClickListener{
            hideSoftKeyboard()
            val comment = etComment.text.toString().trim()
            if (comment.isEmpty()) {
                displayToast(resources.getString(R.string.comment_msg))
            } else {
                if (from_comment == "Edit")
                    editFeedbackComment(feedback_comment.id, pos)
                else
                    addFeedbackComment()
            }
        }
    }

    private fun addFeedbackComment() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<AddFeedbackCommentPojo> = apiService.addDairyComment(intent.getStringExtra("diary_id"),
                    getFromPrefs(ParentConstant.SCHOOL_ID).toString(),
                    getFromPrefs(ParentConstant.USER_ID).toString(),
                    etComment.text.toString().trim(), "", intent.getStringExtra("teacher_id"))
            call.enqueue(object : Callback<AddFeedbackCommentPojo> {
                override fun onResponse(call: Call<AddFeedbackCommentPojo>, response: Response<AddFeedbackCommentPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            displayToast(response.body()!!.message)
                            etComment.setText("")
                            if (commentList.size == 0) {
                                ll_comment_section.visibility = View.VISIBLE
                            }
                            val commentDataPojo = CommentDataPojo()
                            commentDataPojo.id = response.body()!!.data.id
                            commentDataPojo.school_id = response.body()!!.data.school_id
                            commentDataPojo.diary_id = response.body()!!.data.diary_id
                            commentDataPojo.teacher_id = response.body()!!.data.teacher_id
                            commentDataPojo.parent_id = response.body()!!.data.parent_id
                            commentDataPojo.comment = response.body()!!.data.comment
                            commentDataPojo.type = response.body()!!.data.type
                            commentDataPojo.created_at = response.body()!!.data.created_at
                            commentDataPojo.t_name = response.body()!!.data.t_name
                            commentDataPojo.t_profile_pic = response.body()!!.data.t_profile_pic
                            commentDataPojo.p_name = response.body()!!.data.p_name
                            commentDataPojo.p_profile_pic = response.body()!!.data.p_profile_pic
                            commentList.add(commentDataPojo)
                            rvCommentList.visibility = View.VISIBLE
                            noDataAvailable.visibility = View.GONE
                            if (commentAdapter == null) {
                                commentAdapter = SchoolDiaryCommentAdapter(ctx, commentList, "", "")
                                rvCommentList.adapter = commentAdapter
                            }
                            commentAdapter!!.notifyDataSetChanged()
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<AddFeedbackCommentPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun editFeedbackComment(comment_id: String, pos: Int) {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<AddFeedbackCommentPojo> = apiService.addDairyComment(intent.getStringExtra("diary_id"),
                    getFromPrefs(ParentConstant.SCHOOL_ID).toString(),
                    getFromPrefs(ParentConstant.USER_ID).toString(), etComment.text.toString().trim(),
                    comment_id, intent.getStringExtra("teacher_id"))
            call.enqueue(object : Callback<AddFeedbackCommentPojo> {
                override fun onResponse(call: Call<AddFeedbackCommentPojo>, response: Response<AddFeedbackCommentPojo>?) {
                    if (response != null) {

                        if (response.body()!!.status == "1") {
                            from_comment = "Add"
                            displayToast(response.body()!!.message)
                            etComment.setText("")
                            val commentDataPojo = CommentDataPojo()
                            commentDataPojo.id = response.body()!!.data.id
                            commentDataPojo.school_id = response.body()!!.data.school_id
                            commentDataPojo.diary_id = response.body()!!.data.diary_id
                            commentDataPojo.teacher_id = response.body()!!.data.teacher_id
                            commentDataPojo.parent_id = response.body()!!.data.parent_id
                            commentDataPojo.comment = response.body()!!.data.comment
                            commentDataPojo.type = response.body()!!.data.type
                            commentDataPojo.created_at = response.body()!!.data.created_at
                            commentDataPojo.t_name = response.body()!!.data.t_name
                            commentDataPojo.t_profile_pic = response.body()!!.data.t_profile_pic
                            commentDataPojo.p_name = response.body()!!.data.p_name
                            commentDataPojo.p_profile_pic = response.body()!!.data.p_profile_pic
                            commentList[pos] = commentDataPojo
                            commentAdapter!!.notifyDataSetChanged()
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<AddFeedbackCommentPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun getDiaryDetail() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<FilesPojo> = apiService.getSchoolDiaryDetail(intent.getStringExtra("diary_id"),
                    intent.getStringExtra("teacher_id"), getFromPrefs(ParentConstant.USER_ID).toString())
            call.enqueue(object : Callback<FilesPojo> {
                override fun onResponse(call: Call<FilesPojo>, response: Response<FilesPojo>?) {

                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            fileList = response.body()!!.data
                            if(fileList[0].file == "") {
                                ll_attach_section.visibility = View.GONE
                            } else {
                                ll_attach_section.visibility = View.VISIBLE
                                adapter = NoticeAttachFileAdapter(ctx, fileList, from)
                                rvAttachFile.adapter = adapter
                            }
                            if(response.body()!!.comment.size>0){
                                ll_comment_section.visibility = View.VISIBLE
                                commentList = response.body()!!.comment
                                if (commentAdapter == null) {
                                    commentAdapter = SchoolDiaryCommentAdapter(ctx, commentList, "", "")
                                    rvCommentList.adapter = commentAdapter
                                }
                            } else {
                                ll_comment_section.visibility = View.GONE
                            }



                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<FilesPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.ivEdit -> {
                    editFunction(v)
                }
                R.id.ivDelete -> {
                    feedback_comment = v.getTag(R.string.edit_msg) as CommentDataPojo
                    pos = v.getTag(R.string.edit_pos) as Int
                    val builder = AlertDialog.Builder(ctx)
                    builder.setCancelable(false)
                    builder.setMessage(ctx.resources.getString(R.string.delete_msg))
                    builder.setPositiveButton("Yes", { _, _ ->
                        feedbackDelete(ctx, feedback_comment.id, pos)
                    })
                    builder.setNegativeButton("No", { dialog, _ ->
                        dialog.cancel()
                    })
                    val alert = builder.create()
                    alert.show()
                }
                R.id.row_layout -> {
                    data = v.getTag(R.string.data) as NoticeBoardFileDataPojo
                    viewAndDownload()
                }
            }
        }

    }

    private fun feedbackDelete(feedback_ctx: NoticeBoardAttachFileActivity, id: String, position: Int) {
        if (ConnectionDetector.isConnected(feedback_ctx)) {
            val d = ParentDialog.showLoading(feedback_ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.deleteDiaryComment(id)
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            ctx.displayToast(response.body()!!.message)
                            commentList.removeAt(position)
                            commentAdapter?.notifyDataSetChanged()
                            if (commentList.size == 0) {
                                ll_comment_section.visibility = View.GONE
                                //rvCommentList.visibility = View.GONE
                            }
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog?.displayCommonDialog(feedback_ctx.resources.getString(R.string.no_internet_connection))
        }
    }

    @SuppressLint("NewApi")
    private fun editFunction(view: View?) {
        feedback_comment = view!!.getTag(R.string.edit_msg) as CommentDataPojo
        pos = view.getTag(R.string.edit_pos) as Int
        from_comment = view.getTag(R.string.edit_from) as String
        etComment.setText(feedback_comment.comment)
        etComment.requestFocus()
        etComment.setSelection(etComment.text.length)
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(etComment, InputMethodManager.SHOW_IMPLICIT)
    }

    private fun viewAndDownload() {
        folder = File(Environment.getExternalStorageDirectory().toString() + "/schoolManagement")
        if (folder != null && !folder!!.exists())
            folder!!.mkdir()
        file_name = data.file
        val file = File(folder.toString() + "/" + file_name)
        if (!file.exists()) {
            if (isStoragePermissionGranted())
                download()
        } else
            if (isStoragePermissionGranted())
                view()
    }

    private fun isStoragePermissionGranted(): Boolean {
        return if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v("PER", "Permission is granted")
                true
            } else {
                Log.v("PER", "Permission is revoked")
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                false
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("PER", "Permission is granted")
            true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v("per", "Permission: " + permissions[0] + "was " + grantResults[0])
            download()
            //resume tasks needing this permission
        }
    }

    private fun download() {
        if (ConnectionDetector.isConnected(this)) {
            val downloadService = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val url: String = if (from == "Notice")
                ParentConstant.ATTACH_IMAGE_URL
            else
                ParentConstant.ATTACH_DIARY_IMAGE_URL
            val call = downloadService.downloadFileWithDynamicUrlSync(url + file_name)
            d = ParentDialog.showLoading(this)
            d?.setCanceledOnTouchOutside(false)
            call.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    Log.d("Hello", "server contacted and has file")
                    val writtenToDisk = writeResponseBodyToDisk(response.body()!!)
                    println("xhxhxhhxx file download was a success? $writtenToDisk")
                    if (!writtenToDisk && d!!.isShowing) {
                        d?.dismiss()
                        displayToast(resources.getString(R.string.file_download))
                    }
                }

                override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                    println(t.toString())
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun writeResponseBodyToDisk(body: ResponseBody): Boolean {
        try {
            val futureStudioIconFile = File(Environment.getExternalStorageDirectory().toString() + "/schoolManagement/" + file_name)

            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)

                var fileSizeDownloaded: Long = 0

                inputStream = body.byteStream()
                outputStream = FileOutputStream(futureStudioIconFile)

                while (true) {
                    val read = inputStream!!.read(fileReader)

                    if (read == -1) {
                        break
                    }

                    outputStream.write(fileReader, 0, read)

                    fileSizeDownloaded += read.toLong()

//                    Log.d("Hello", "file download: $fileSizeDownloaded of $fileSize")
                    if (d != null && d!!.isShowing)
                        d?.dismiss()
                }
                view()
                outputStream.flush()

                return true
            } catch (e: IOException) {
                return false
            } finally {
                if (inputStream != null) {
                    inputStream.close()
                }

                if (outputStream != null) {
                    outputStream.close()
                }
            }
        } catch (e: IOException) {
            return false
        }

    }

    private fun view() {
        val pdfFile = File(Environment.getExternalStorageDirectory().toString() + "/schoolManagement/" + file_name)
        //        Uri path = Uri.fromFile(pdfFile);
        val photoURI = FileProvider.getUriForFile(ctx, applicationContext.packageName + ".my.package.name.provider", pdfFile)
        val pdfIntent = Intent(Intent.ACTION_VIEW)
        if (file_name.toString().toLowerCase().contains(".pdf"))
            pdfIntent.setDataAndType(photoURI, "application/pdf")
        else if (file_name.toString().toLowerCase().contains(".doc") || file_name.toString().toLowerCase().contains(".docx"))
            pdfIntent.setDataAndType(photoURI, "application/msword")
        else if (file_name.toString().toLowerCase().contains(".jpg") || file_name.toString().toLowerCase().contains(".jpeg") || file_name.toString().toLowerCase().contains(".png"))
            pdfIntent.setDataAndType(photoURI, "image/jpeg")
        pdfIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        try {
            startActivity(pdfIntent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(ctx, resources.getString(R.string.no_app), Toast.LENGTH_SHORT).show()
        }
    }
}