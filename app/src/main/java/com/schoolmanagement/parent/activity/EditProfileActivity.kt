package com.schoolmanagement.parent.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.pojo.*
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by arpit.jain on 2/14/2018.
 */
class EditProfileActivity : BaseActivity() {

    private val ctx: EditProfileActivity = this
    private var dialog: ParentDialog? = null

    private lateinit var iv_notification: ImageView
    private lateinit var iv_back: ImageView

    private lateinit var profilePic: CircleImageView
    private lateinit var userName: TextView
    private lateinit var primary_email: TextView
    private lateinit var primary_phone: TextView
    private lateinit var father_name: EditText
    private lateinit var father_email: EditText
    private lateinit var father_phone: EditText
    private lateinit var father_occup: EditText
    private lateinit var father_work_place: EditText
    private lateinit var mother_name: EditText
    private lateinit var mother_email: EditText
    private lateinit var mother_phone: EditText
    private lateinit var mother_occup: EditText
    private lateinit var mother_work_place: EditText
    private lateinit var etEmergencyName: EditText
    private lateinit var etEmergencyContact: EditText
    private lateinit var tvId: TextView
    private lateinit var etPassportNumber: EditText
    private lateinit var etAddress1: EditText
    private lateinit var etAddress2: EditText
    private lateinit var etAddress3: EditText
    private lateinit var etCity: EditText
    private lateinit var etState: EditText
    private lateinit var etZipcode: EditText
    private lateinit var etCountry: EditText
    private lateinit var tvSubmit: TextView

    private var country_id: String = ""
    var country_name: ArrayList<String>? = null
    private lateinit var country_list: ArrayList<CountryDataPojo>
    private var stringsOrNulls = arrayOfNulls<String>(10)
    private var profileFrom: String = "edit"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        setHeader(resources.getString(R.string.edit_profile))
        profileFrom = intent.getStringExtra("from")
        initialize()
        setListener()
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        country_list = ArrayList()
        iv_notification = findViewById(R.id.iv_notification)
        iv_back = findViewById(R.id.iv_back)
        profilePic = findViewById(R.id.profilePic)
        userName = findViewById(R.id.userName)
        primary_email = findViewById(R.id.primary_email)
        primary_phone = findViewById(R.id.primary_phone)
        father_name = findViewById(R.id.father_name)
        father_email = findViewById(R.id.father_email)
        father_phone = findViewById(R.id.father_phone)
        father_occup = findViewById(R.id.father_occup)
        father_work_place = findViewById(R.id.father_work_place)
        mother_name = findViewById(R.id.mother_name)
        mother_email = findViewById(R.id.mother_email)
        mother_phone = findViewById(R.id.mother_phone)
        mother_occup = findViewById(R.id.mother_occup)
        mother_work_place = findViewById(R.id.mother_work_place)
        etEmergencyName = findViewById(R.id.etEmergencyName)
        etEmergencyContact = findViewById(R.id.etEmergencyContact)
        tvId = findViewById(R.id.tvId)
        etPassportNumber = findViewById(R.id.etPassportNumber)
        etAddress1 = findViewById(R.id.etAddress1)
        etAddress2 = findViewById(R.id.etAddress2)
        etAddress3 = findViewById(R.id.etAddress3)
        etCity = findViewById(R.id.etCity)
        etState = findViewById(R.id.etState)
        etZipcode = findViewById(R.id.etZipcode)
        etCountry = findViewById(R.id.etCountry)
        tvSubmit = findViewById(R.id.tvSubmit)

        if (profileFrom == "first_login") {
            iv_notification.visibility = View.GONE
            iv_back.visibility = View.GONE
        } else {
            iv_notification.visibility = View.VISIBLE
            iv_back.visibility = View.VISIBLE
        }

        countryList()
        getProfile()
    }

    private fun setListener() {
        tvSubmit.setOnClickListener {
            validate()
        }

        etCountry.setOnClickListener {
            selectCountryDialog()
        }
    }

    private fun countryList() {
        if (ConnectionDetector.isConnected(this)) {
            val d = ParentDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            country_name = ArrayList()
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<CountryPojo> = apiService.country_list()
            call.enqueue(object : Callback<CountryPojo> {
                override fun onResponse(call: Call<CountryPojo>, response: Response<CountryPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            country_list.addAll(response.body()!!.data)
                            stringsOrNulls = arrayOfNulls(country_list.size)
                            for (i in 0 until country_list.size) {
                                country_name!!.add(country_list[i].country)
                                stringsOrNulls[i] = country_list[i].country
                            }

                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<CountryPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun selectCountryDialog() {
        var select: String
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.select_country))
        builder.setItems(stringsOrNulls,
                { _, position ->
                    select = stringsOrNulls[position].toString()
                    etCountry.setText(select)
                    country_id = country_list[position].id
                })
        val alert = builder.create()
        alert.show()
        return
    }

    private fun getProfile() {
        if (ConnectionDetector.isConnected(this)) {
            val d = ParentDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<LoginPojo> = apiService.getProfile(getFromPrefs(ParentConstant.USER_ID).toString())
            call.enqueue(object : Callback<LoginPojo> {
                override fun onResponse(call: Call<LoginPojo>, response: Response<LoginPojo>?) {
                    if (response != null) {
                        if (response.body()?.status == "1" && response.body()?.data != null) {
                            setData(response)
                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<LoginPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setData(response: Response<LoginPojo>) {
        val loginDataPojo = response.body()?.data
        country_id = loginDataPojo!!.country_id
        setProfileImageInLayoutProfile(ctx, ParentConstant.IMAGE_STUDENT_PROFILE + loginDataPojo.students!!.profile_pic, profilePic)
        userName.text = getStudentName()
        primary_email.text = loginDataPojo.primary_email
        primary_phone.text = loginDataPojo.primary_phone
        father_name.setText(loginDataPojo.father_name)
        father_email.setText(loginDataPojo.father_email)
        father_phone.setText(loginDataPojo.father_phone)
        father_occup.setText(loginDataPojo.father_occupation)
        father_work_place.setText(loginDataPojo.father_place_of_work)
        mother_name.setText(loginDataPojo.mother_name)
        mother_email.setText(loginDataPojo.mother_email)
        mother_phone.setText(loginDataPojo.mother_phone)
        mother_occup.setText(loginDataPojo.mother_occupation)
        mother_work_place.setText(loginDataPojo.mother_place_of_work)
        etEmergencyName.setText(loginDataPojo.students!!.emergency_contact_name)
        etEmergencyContact.setText(loginDataPojo.students!!.emergency_contact_number)
        tvId.text = loginDataPojo.id
        etPassportNumber.setText(loginDataPojo.passport_number)
        etAddress1.setText(loginDataPojo.students!!.address_line1)
        etAddress2.setText(loginDataPojo.students!!.address_line2)
        etAddress3.setText(loginDataPojo.students!!.address_line3)
        etCity.setText(loginDataPojo.students!!.city)
        etState.setText(loginDataPojo.students!!.state)
        etZipcode.setText(loginDataPojo.students!!.pincode)
        etCountry.setText(loginDataPojo.country_name)
    }

    private fun validate() {
        val fa_name = father_name.text.toString().trim()
        val fa_email = father_email.text.toString().trim()
        val fa_phone = father_phone.text.toString().trim()
        val fa_occ = father_occup.text.toString().trim()
        val fa_work = father_work_place.text.toString().trim()
        val ma_name = mother_name.text.toString().trim()
        val ma_email = mother_email.text.toString().trim()
        val ma_phone = mother_phone.text.toString().trim()
        val ma_occ = mother_occup.text.toString().trim()
        val ma_work = mother_work_place.text.toString().trim()
        val emergency_name = etEmergencyName.text.toString().trim()
        val emergency_no = etEmergencyContact.text.toString()
        val passport = etPassportNumber.text.toString().trim()
        val address = etAddress1.text.toString().trim()
        val city_val = etCity.text.toString().trim()
        val state_val = etState.text.toString().trim()
        val pincode = etZipcode.text.toString().trim()
        val selected_country = etCountry.text.toString()

        if (fa_name.isEmpty()) {
            displayToast(resources.getString((R.string.fa_name_msg)))
        } else if (fa_email.isEmpty()) {
            displayToast(resources.getString(R.string.fa_email_msg))
        } else if (!isValidEmail(fa_email)) {
            displayToast(resources.getString(R.string.valid_email_id))
        } else if (fa_phone.isEmpty()) {
            displayToast(resources.getString(R.string.fa_phone_msg))
        } else if (fa_phone != "" && !fa_phone.matches("[0-9]+".toRegex())) run {
            displayToast(resources.getString(R.string.valid_mobile_no_msg))
        } else if (fa_phone.length < 8) {
            displayToast(resources.getString(R.string.no_between_msg))
        } else if (fa_occ.isEmpty()) {
            displayToast(resources.getString(R.string.fa_occ_msg))
        } else if (fa_work.isEmpty()) {
            displayToast(resources.getString(R.string.fa_work_place_msg))
        } else if (ma_name.isEmpty()) {
            displayToast(resources.getString(R.string.ma_name_msg))
        } else if (ma_email.isEmpty()) {
            displayToast(resources.getString(R.string.ma_email_msg))
        } else if (!isValidEmail(ma_email)) {
            displayToast(resources.getString(R.string.valid_email_id))
        } else if (ma_phone.isEmpty()) {
            displayToast(resources.getString(R.string.ma_phone_msg))
        } else if (ma_phone != "" && !ma_phone.matches("[0-9]+".toRegex())) run {
            displayToast(resources.getString(R.string.valid_mobile_no_msg))
        } else if (ma_phone.length < 8) {
            displayToast(resources.getString(R.string.no_between_msg))
        } else if (ma_occ.isEmpty()) {
            displayToast(resources.getString(R.string.ma_occ_msg))
        } else if (ma_work.isEmpty()) {
            displayToast(resources.getString(R.string.ma_work_place_msg))
        } else if (emergency_name.isEmpty()) {
            displayToast(resources.getString(R.string.emergency_name_msg))
        } else if (emergency_no.isEmpty()) {
            displayToast(resources.getString(R.string.emergency_mobile_no_msg))
        } else if (emergency_no.length < 8) {
            displayToast(resources.getString(R.string.no_between_msg))
        } else if (passport.isEmpty()) {
            displayToast(resources.getString(R.string.passport_id_msg))
        } else if (address.isEmpty()) {
            displayToast(resources.getString(R.string.address_msg))
        } else if (city_val.isEmpty()) {
            displayToast(resources.getString(R.string.city_msg))
        } else if (state_val.isEmpty()) {
            displayToast(resources.getString(R.string.state_msg))
        } else if (pincode.isEmpty()) {
            displayToast(resources.getString(R.string.zipcode_msg))
        } else if (pincode.length < 3) {
            displayToast(resources.getString(R.string.zipcode_btwn_msg))
        } else if (selected_country.isEmpty()) {
            displayToast(resources.getString(R.string.select_country_msg))
        } else {
            saveProfile()
            hideSoftKeyboard()
        }
    }

    private fun saveProfile() {
        if (ConnectionDetector.isConnected(this)) {
            val d = ParentDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.editProfile(getFromPrefs(ParentConstant.USER_ID).toString(), father_name.text.toString().trim(),
                    father_email.text.toString().trim(), father_phone.text.toString(), father_occup.text.toString().trim(), father_work_place.text.toString().trim(),
                    mother_name.text.toString().trim(), mother_email.text.toString(), mother_phone.text.toString(), mother_occup.text.toString().trim(),
                    mother_work_place.text.toString().trim(), etEmergencyName.text.toString().trim(), etEmergencyContact.text.toString(),
                    etPassportNumber.text.toString().trim(), etAddress1.text.toString().trim(), etAddress2.text.toString().trim(),
                    etAddress3.text.toString().trim(), etCity.text.toString().trim(), etState.text.toString().trim(), etZipcode.text.toString().trim(), country_id)
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            displayToast(response.body()!!.message)
                            if (profileFrom == "first_login") {
                                startActivity(Intent(applicationContext, HomeActivity::class.java))
                                saveIntoPrefs(ParentConstant.ENHANCED_PROFILE, "1")
                            } else {
                                ParentConstant.PROFILE_REFRESH = true
                            }
                            studentList()
                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun studentList() {
        if (ConnectionDetector.isConnected(ctx)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<StudentPojo> = apiService.studentList(getFromPrefs(ParentConstant.USER_ID)!!)
            call.enqueue(object : Callback<StudentPojo> {
                override fun onResponse(call: Call<StudentPojo>, response: Response<StudentPojo>) {
                    if (response.body() != null) {
                        if (response.body()!!.status == "1") {
                            saveStudentData(response.body()!!.data)
                            finish()
                        }
                    }
                }

                override fun onFailure(call: Call<StudentPojo>, t: Throwable) {
                    // Log error here since request failed
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }
}