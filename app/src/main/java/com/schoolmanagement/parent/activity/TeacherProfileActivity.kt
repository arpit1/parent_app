package com.schoolmanagement.parent.activity

import android.os.Bundle
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.pojo.TeacherDataPojo
import com.schoolmanagement.parent.util.ParentConstant
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by upasna.mishra on 3/6/2018.
 */
class TeacherProfileActivity : BaseActivity() {

    private var ctx: TeacherProfileActivity = this
    private lateinit var profilePic: CircleImageView
    private lateinit var teacherName: TextView
    private lateinit var teacherEmail: TextView
    private lateinit var mobile: TextView
    private lateinit var address: TextView
    private lateinit var city: TextView
    private lateinit var state: TextView
    private lateinit var zipCode: TextView
    private lateinit var country: TextView
    private lateinit var qualification: TextView

    private lateinit var specialization: TextView

    private lateinit var teacherData: TeacherDataPojo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_teacher_profile)
        initialize()
    }

    private fun initialize() {
        setHeader(resources.getString(R.string.teacher_profile))
        teacherData = intent.getSerializableExtra("teacherData") as TeacherDataPojo
        profilePic = findViewById(R.id.profilePic)
        teacherName = findViewById(R.id.teacherName)
        teacherEmail = findViewById(R.id.teacherEmail)
        qualification = findViewById(R.id.qualification)
        mobile = findViewById(R.id.mobile)
        address = findViewById(R.id.address)
        city = findViewById(R.id.city)
        state = findViewById(R.id.state)
        zipCode = findViewById(R.id.zipCode)
        country = findViewById(R.id.country)
        specialization = findViewById(R.id.specialization)
        if (teacherData.profile_pic != null && teacherData.profile_pic != "") {
            ctx.setProfileImageInLayout(ctx, ParentConstant.IMAGE_TEACHER_PROFILE + teacherData.profile_pic, profilePic)
        } else {
            profilePic.setImageResource(R.mipmap.usericon)
        }
        if (teacherData.mname != "")
            teacherName.text = teacherData.fname + " " + teacherData.mname + " " + teacherData.lname
        else
            teacherName.text = teacherData.fname + " " + teacherData.lname
        teacherEmail.text = teacherData.email
        specialization.text = teacherData.specialization
        qualification.text = teacherData.qualification
        mobile.text = teacherData.mobile_number
        country.text = teacherData.country_name
        var addressVal: String
        addressVal = teacherData.address1
        if (teacherData.address2 != null && teacherData.address2 != "")
            addressVal = addressVal + ", " + teacherData.address2
        if (teacherData.address3 != null && teacherData.address3 != "")
            addressVal = addressVal + ", " + teacherData.address3
        address.text = addressVal
        city.text = teacherData.city
        state.text = teacherData.state
        zipCode.text = teacherData.pincode
    }
}