package com.schoolmanagement.parent.activity

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.pojo.BasePojo
import com.schoolmanagement.parent.pojo.TeacherDataPojo
import com.schoolmanagement.parent.pojo.TeacherPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddFeedbackActivity : BaseActivity() {

    private lateinit var feedbackTopic: EditText
    private lateinit var description: EditText
    private lateinit var submit: TextView
    private var ctx: AddFeedbackActivity = this
    private lateinit var spTeacher: Spinner
    private var dialog: ParentDialog? = null
    private lateinit var teacherList: ArrayList<TeacherDataPojo>
    private lateinit var mInflator: LayoutInflater
    private var teacherId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_feedback)
        initialize()
        setListener()
    }

    private fun initialize() {
        teacherList = ArrayList()
        dialog = ParentDialog(ctx)
        setHeader(resources.getString(R.string.add_feedback))
        spTeacher = findViewById(R.id.spTeacher)
        feedbackTopic = findViewById(R.id.feedbackTopic)
        description = findViewById(R.id.description)
        description.setOnTouchListener({ v, motionEvent ->
            if (v.id == R.id.description) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(
                            false)
                }
            }
            false
        })
        submit = findViewById(R.id.submit)
        teacherList()
    }

    private fun setListener() {
        spTeacher.onItemSelectedListener = teacherSelectedListener
        submit.setOnClickListener {
            hideSoftKeyboard()
            if (validate()) {
                addFeedback()
            }
        }
    }

    private fun validate(): Boolean {
        val topic = feedbackTopic.text.toString().trim()
        val description = description.text.toString().trim()

        return when {
            (teacherId != null && teacherId == "") -> {
                displayToast(resources.getString(R.string.teacher_name))
                false
            }
            topic.isEmpty() -> {
                displayToast(resources.getString(R.string.feedback_topic_msg))
                false
            }
            description.isEmpty() -> {
                displayToast(resources.getString(R.string.description_msg))
                false
            }
            else -> true
        }
    }

    private fun addFeedback() {
        if (ConnectionDetector.isConnected(this)) {
            val d = ParentDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.addFeedback(getFromPrefs(ParentConstant.SCHOOL_ID).toString()
                    , getFromPrefs(ParentConstant.USER_ID).toString(), teacherId!!, feedbackTopic.text.toString().trim(), description.text.toString().trim())
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            displayToast(response.body()!!.message)
                            ParentConstant.FEEDBACK_REFRESH = true
                            finish()
                        }
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    /*Type = 1 to get the Admin
            * Type = 0 to get normal list without Admin
            * */
    private fun teacherList() {
        if (ConnectionDetector.isConnected(this)) {
            val d = ParentDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<TeacherPojo> = apiService.teacherList(getFromPrefs(ParentConstant.USER_ID).toString()
                    , getFromPrefs(ParentConstant.SCHOOL_ID).toString(), "0")
            call.enqueue(object : Callback<TeacherPojo> {
                override fun onResponse(call: Call<TeacherPojo>, response: Response<TeacherPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            val teacherDataPojo = TeacherDataPojo()
                            teacherDataPojo.id = ("-1")
                            teacherDataPojo.fname = resources.getString(R.string.name_of_teacher)
                            teacherList = response.body()!!.data
                            teacherList.add(0, teacherDataPojo)

                            spTeacher.adapter = teacherSpinnerAdapter

                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<TeacherPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private val teacherSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            mInflator = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget)
            ftext!!.text = getTeacherName(teacherList[position])
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return getTeacherName(teacherList[position])
        }

        override fun getCount(): Int {
            return teacherList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget)
            ftext!!.text = getTeacherName(teacherList[position])
            return convertView as View
        }
    }

    private fun getTeacherName(teacherData: TeacherDataPojo): String {
        return if (teacherData.mname != "")
            teacherData.fname + " " + teacherData.mname + " " + teacherData.lname
        else
            teacherData.fname + " " + teacherData.lname
    }

    private val teacherSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
            teacherId = if (position != 0) {
                teacherList[position].id
            } else {
                ""
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }
}