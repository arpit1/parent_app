package com.schoolmanagement.parent.activity

import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import com.github.rtoshiro.view.video.FullscreenVideoView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.util.ParentConstant
import java.io.IOException

/**
 * Created by upasna.mishra on 2/13/2018.
 */
class FullVideoScreenActivity : BaseActivity(){

    private lateinit var image_path: String
    private lateinit var videoviewfullscreen : FullscreenVideoView
    private lateinit var ivShareIcon : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.full_video_activity)
        initialize()
    }

    private fun initialize() {
        setHeader(intent.getStringExtra("event_name"))
        image_path = intent.getStringExtra("Image")
        videoviewfullscreen = findViewById(R.id.videoview_fullscreen)
        ivShareIcon = findViewById(R.id.ivShareIcon)
        ivShareIcon.visibility = View.VISIBLE

        videoviewfullscreen.setActivity(this)
        videoviewfullscreen.isShouldAutoplay = true

        loadVideo(image_path)

        ivShareIcon.setOnClickListener {
            shareContent()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        videoviewfullscreen.resize()
    }

    private fun loadVideo(videoPath : String) {
        val videoUri = Uri.parse(ParentConstant.IMAGE_URl + "" +videoPath)
        try {
            videoviewfullscreen.setVideoURI(videoUri)

        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun shareContent() {
        println("image path in share  "+image_path)
        val path = ParentConstant.IMAGE_URl + "" + image_path
        println("video path is   "+path)
        val share = Intent(Intent.ACTION_SEND)
        share.type = "text/plain"
        share.putExtra(Intent.EXTRA_SUBJECT, intent.getStringExtra("event_name"))
        share.putExtra(Intent.EXTRA_TEXT, path)
        startActivity(Intent.createChooser(share, "Share link!"))
    }
}