package com.schoolmanagement.parent.activity

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.adapter.FeedbackCommentAdapter
import com.schoolmanagement.parent.pojo.*
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by upasna.mishra on 2/19/2018.
 */
class FeedbackDetailActivity : BaseActivity(), View.OnClickListener {

    private lateinit var rvCommentList: RecyclerView
    private lateinit var noDataAvailable: TextView
    private lateinit var etComment: EditText
    private lateinit var sendComment: ImageView
    private lateinit var schoolProfilePic: CircleImageView
    private lateinit var topicName: TextView
    private lateinit var feedbackDateTime: TextView
    private lateinit var studentName: TextView
    private lateinit var teacherName: TextView
    private lateinit var description: TextView
    private lateinit var feedback_detial: FeedbackDataPojo
    private var ctx: FeedbackDetailActivity = this
    private var dialog: ParentDialog? = null
    private lateinit var mLayoutManager: LinearLayoutManager
    private var adapter: FeedbackCommentAdapter? = null
    private var feedback_comment_list: ArrayList<CommentDataPojo>? = null
    private var from: String = "Add"
    private var pos: Int = 0
    private lateinit var feedback_comment: CommentDataPojo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback_details)
        initialize()
        setListener()
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        mLayoutManager = LinearLayoutManager(ctx)
        feedback_comment_list = ArrayList()
        feedback_detial = intent.getSerializableExtra("feedback_detail") as FeedbackDataPojo
        setHeader(resources.getString(R.string.feedback_detail))
        rvCommentList = findViewById(R.id.rvCommentList)
        noDataAvailable = findViewById(R.id.noDataAvailable)
        noDataAvailable.text = resources.getString(R.string.no_comments_available)
        etComment = findViewById(R.id.etComment)
        sendComment = findViewById(R.id.sendComment)
        schoolProfilePic = findViewById(R.id.schoolProfilePic)
        topicName = findViewById(R.id.topicName)
        feedbackDateTime = findViewById(R.id.feedbackDateTime)
        studentName = findViewById(R.id.studentName)
        teacherName = findViewById(R.id.teacherName)
        description = findViewById(R.id.description)
        rvCommentList.layoutManager = mLayoutManager
        if (feedback_detial.created != "") {
            val new_date = feedback_detial.created.replace(".000Z", "").split("T")
            feedbackDateTime.text = ctx.changeDateTimeFormat(new_date[0] + " " + new_date[1])
        }

        if (feedback_detial.profile_pic != null && feedback_detial.profile_pic != "") {
            ctx.setProfileImageInLayout(ctx, ParentConstant.IMAGE_TEACHER_PROFILE + feedback_detial.profile_pic, schoolProfilePic)
        } else {
            schoolProfilePic.setImageResource(R.mipmap.usericon)
        }

        topicName.text = feedback_detial.topic
        studentName.text = ctx.resources.getString(R.string.student_name) + ": " + ctx.getStudentName()
        teacherName.text = ctx.resources.getString(R.string.show_teacher_name) + ": " + getTeacherName(feedback_detial)
        description.text = feedback_detial.description
        feedbackDetails()
    }

    private fun setListener() {
        sendComment.setOnClickListener() {
            hideSoftKeyboard()
            val comment = etComment.text.toString().trim()
            if (comment.isEmpty()) {
                displayToast(resources.getString(R.string.comment_msg))
            } else {
                if (from == "Edit")
                    editFeedbackComment(feedback_comment.id, pos)
                else
                    addFeedbackComment()
            }
        }
    }

    override fun onClick(view: View?) {
        if (view != null) {
            when (view.id) {
                R.id.ivEdit -> {
                    editFunction(view)
                }
                R.id.ivDelete -> {
                    feedback_comment = view.getTag(R.string.edit_msg) as CommentDataPojo
                    pos = view.getTag(R.string.edit_pos) as Int
                    val builder = AlertDialog.Builder(ctx)
                    builder.setCancelable(false)
                    builder.setMessage(ctx.resources.getString(R.string.delete_msg))
                    builder.setPositiveButton("Yes", { _, _ ->
                        feedbackDelete(ctx, feedback_comment.id, pos)
                    })
                    builder.setNegativeButton("No", { dialog, _ ->
                        dialog.cancel()
                    })
                    val alert = builder.create()
                    alert.show()
                }
            }
        }
    }

    @SuppressLint("NewApi")
    private fun editFunction(view: View?) {
        feedback_comment = view!!.getTag(R.string.edit_msg) as CommentDataPojo
        pos = view.getTag(R.string.edit_pos) as Int
        from = view.getTag(R.string.edit_from) as String
        etComment.setText(feedback_comment.comment)
        etComment.requestFocus()
        etComment.setSelection(etComment.text.length)
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(etComment, InputMethodManager.SHOW_IMPLICIT)
    }

    private fun editFeedbackComment(comment_id: String, pos: Int) {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<AddFeedbackCommentPojo> = apiService.editFeedbackComment(comment_id, etComment.text.toString())
            call.enqueue(object : Callback<AddFeedbackCommentPojo> {
                override fun onResponse(call: Call<AddFeedbackCommentPojo>, response: Response<AddFeedbackCommentPojo>?) {
                    if (response != null) {

                        if (response.body()!!.status == "1") {
                            from = "Add"
                            displayToast(response.body()!!.message)
                            etComment.setText("")
                            val commentDataPojo = CommentDataPojo()
                            commentDataPojo.id = response.body()!!.data.id
                            commentDataPojo.feedback_id = response.body()!!.data.feedback_id
                            commentDataPojo.user_id = response.body()!!.data.user_id
                            commentDataPojo.comment = response.body()!!.data.comment
                            commentDataPojo.created = response.body()!!.data.created
                            feedback_comment_list!![pos] = commentDataPojo
                            adapter!!.notifyDataSetChanged()
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<AddFeedbackCommentPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun addFeedbackComment() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<AddFeedbackCommentPojo> = apiService.addFeedbackComment(feedback_detial.id, ctx.getFromPrefs(ParentConstant.USER_ID).toString(),
                    etComment.text.toString())
            call.enqueue(object : Callback<AddFeedbackCommentPojo> {
                override fun onResponse(call: Call<AddFeedbackCommentPojo>, response: Response<AddFeedbackCommentPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            displayToast(response.body()!!.message)
                            etComment.setText("")
                            val commentDataPojo = CommentDataPojo()
                            commentDataPojo.id = response.body()!!.data.id
                            commentDataPojo.feedback_id = response.body()!!.data.feedback_id
                            commentDataPojo.user_id = response.body()!!.data.user_id
                            commentDataPojo.comment = response.body()!!.data.comment
                            commentDataPojo.created = response.body()!!.data.created
                            feedback_comment_list!!.add(commentDataPojo)
                            rvCommentList.visibility = View.VISIBLE
                            noDataAvailable.visibility = View.GONE
                            if (adapter == null) {
                                adapter = FeedbackCommentAdapter(ctx, feedback_comment_list!!, "", "")
                                rvCommentList.adapter = adapter
                            }
                            adapter!!.notifyDataSetChanged()
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<AddFeedbackCommentPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun feedbackDetails() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<CommentPojo> = apiService.feedbackDetail(feedback_detial.id, ctx.getFromPrefs(ParentConstant.SCHOOL_ID).toString())
            call.enqueue(object : Callback<CommentPojo> {
                override fun onResponse(call: Call<CommentPojo>, response: Response<CommentPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            if (response.body()!!.data.size > 0) {
                                rvCommentList.visibility = View.VISIBLE
                                noDataAvailable.visibility = View.GONE
                                feedback_comment_list!!.addAll(response.body()!!.data)
                                adapter = FeedbackCommentAdapter(ctx, feedback_comment_list!!, response.body()!!.school.logo,
                                        response.body()!!.school.name)
                                rvCommentList.adapter = adapter
                            } else {
                                rvCommentList.visibility = View.GONE
                                noDataAvailable.visibility = View.VISIBLE
                            }
                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<CommentPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun feedbackDelete(feedback_ctx: FeedbackDetailActivity, id: String, position: Int) {
        if (ConnectionDetector.isConnected(feedback_ctx)) {
            val d = ParentDialog.showLoading(feedback_ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.deleteFeedbackComment(id)
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            ctx.displayToast(response.body()!!.message)
                            feedback_comment_list?.removeAt(position)
                            adapter?.notifyDataSetChanged()
                            if (feedback_comment_list?.size == 0) {
                                rvCommentList.visibility = View.GONE
                                noDataAvailable.visibility = View.VISIBLE
                            }
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog?.displayCommonDialog(feedback_ctx.resources.getString(R.string.no_internet_connection))
        }
    }
}