package com.schoolmanagement.parent.activity

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.View
import android.widget.GridView
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.adapter.GalleryFileAdapter
import com.schoolmanagement.parent.pojo.HomePostDataPojo
import com.schoolmanagement.parent.util.ParentConstant

class GalleryDetailActivity : BaseActivity() {

    private lateinit var albumNameTxt: TextView
    private lateinit var albumTimeTxt: TextView
    private lateinit var albumTypeTxt: TextView
    private lateinit var descSchoolDiary: TextView
    private lateinit var gridFiles: GridView
    private lateinit var galleryList: HomePostDataPojo
    private lateinit var adapter: GalleryFileAdapter
    private var ctx: GalleryDetailActivity = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.gallery_detail_activity)
        initialize()
        setListener()

        ParentConstant.ACTIVITIES.add(ctx)
    }

    private fun initialize() {
        galleryList = intent.getSerializableExtra("galleryData") as HomePostDataPojo
        val from = intent.getStringExtra("from")
        albumNameTxt = findViewById(R.id.albumNameTxt)
        if (from != null && from == "home") {
            val blackStrip: LinearLayout = findViewById(R.id.blackStrip)
            blackStrip.visibility = View.GONE
            albumNameTxt.visibility = View.GONE
            setHeader(resources.getString(R.string.home))
        } else {
            setHeader(resources.getString(R.string.gallery))
        }
        albumTimeTxt = findViewById(R.id.albumTimeTxt)
        albumTypeTxt = findViewById(R.id.albumTypeTxt)
        descSchoolDiary = findViewById(R.id.descSchoolDiary)
        gridFiles = findViewById(R.id.gridFiles)
        if (galleryList.dates != null)
            albumTimeTxt.text = getChangedDate(galleryList.dates!!)

        albumNameTxt.text = galleryList.event_name
        albumTypeTxt.text = galleryList.category
        descSchoolDiary.text = galleryList.discussion
        descSchoolDiary.movementMethod = ScrollingMovementMethod()
        adapter = GalleryFileAdapter(ctx, galleryList.file!!, galleryList.upload_type, galleryList.event_name)
        gridFiles.adapter = adapter
    }

    private fun setListener() {

    }
}