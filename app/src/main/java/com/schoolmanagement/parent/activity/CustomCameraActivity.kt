package com.schoolmanagement.parent.activity

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.hardware.Camera
import android.media.CamcorderProfile
import android.media.MediaRecorder
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.VideoView
import com.schoolmanagement.parent.R
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.sql.Timestamp
import java.util.*

/**
 * Created by hitesh.mathur on 3/27/2018.
 */
class CustomCameraActivity : BaseActivity(), SurfaceHolder.Callback {

    private var surfaceView: SurfaceView? = null
    private var surfaceHolder: SurfaceHolder? = null
    private var cameraId = 0
    private var cnt: Int = 0
    private var rotation: Int = 0
    private var setMax_Timer = 0
    private var takeActualRotation: Int = 0
    private var i = 0
    private var rotate: Int = 0
    private var camera: Camera? = null
    private var captureImagee: ImageView? = null
    private var flipCamera: ImageView? = null
    private var setCapture_image: ImageView? = null
    private var iv_back_icon: ImageView? = null
    private var isFrontFacing: Boolean = false
    private var recording = false
    private var filePath: String? = null
    private var tvRecordAudio: TextView? = null
    private var mediaRecorder: MediaRecorder? = null
    private var videoPreview: VideoView? = null
    private val VIDEO_CAPTURE = 101
    private val SELECT_PHOTO = 457
    internal lateinit var ff: String
    private val mHandler = Handler()
    private var safeToTakePicture = false

    private val mHandlerTask = object : Runnable {
        val TAG = "mHandler"

        override fun run() {
            setTimeElapsed()
            if (cnt < 30) {//Record video only 30 second
                mHandler.postDelayed(this, INTERVAL.toLong())
                ++cnt
            }
        }

        private fun setTimeElapsed() {
            setMax_Timer = cnt
            tvRecordAudio!!.text = String.format("%02d:%02d", 0, setMax_Timer)
            Log.e(TAG, "setTimeElapsed: " + tvRecordAudio!!.text)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.custom_camera)
        init()

        //set camera button click
        captureImagee!!.setOnClickListener {
            //use for camera capture image
            if (i == 0) {
                if(safeToTakePicture){
                    takeImage()
                    safeToTakePicture = false
                }
            } else {
                //use for recording video
                if (recording) {

                    setMax_Timer = cnt
                    tvRecordAudio!!.text = String.format("%02d:%02d", 0, setMax_Timer)
                    mHandler.removeCallbacks(mHandlerTask)

                    camera!!.setDisplayOrientation(90) // use for set the orientation of the preview

                    // stop recording and release camera
                    mediaRecorder!!.stop() // stop the recording
                    releaseMediaRecorder() // release the MediaRecorder object
                    videoPreview!!.visibility = View.VISIBLE
                    setCapture_image!!.visibility = View.GONE


                    videoPreview!!.setVideoPath(ff)
                    videoPreview!!.start()
                    videoPreview!!.resume()


                    i = 0
                    surfaceHolder = surfaceView!!.holder
                    surfaceHolder!!.addCallback(this@CustomCameraActivity)
                    cameraId = Camera.CameraInfo.CAMERA_FACING_BACK
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                    captureImagee!!.setImageResource(R.drawable.customcamera)

                    recording = false

                    val id = if (cameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
                        Camera.CameraInfo.CAMERA_FACING_BACK
                    else
                        Camera.CameraInfo.CAMERA_FACING_BACK
                    if (!openCamera(id)) {
                        alertCameraDialog()
                    }

                    val intent = Intent()
                    intent.putExtra("code", ff)
                    intent.putExtra("type", "video")
                    setResult(VIDEO_CAPTURE, intent)
                    finish()

                } else {
                    if (!prepareMediaRecorder()) {
                        finish()
                    }
                    // work on UiThread for better performance
                    runOnUiThread(Runnable {
                        // If there are stories, add them to the table
                        try {
                            mediaRecorder!!.start()
                            startRepeatingTask()
                        } catch (ex: Exception) {
                            // Log.i("---","Exception in thread");
                        }
                    })

                    recording = true
                }
            }
        }
        //set video recording image and open recording screen
        captureImagee!!.setOnLongClickListener {
            captureImagee!!.setImageResource(R.drawable.play)
            i = 1
            tvRecordAudio!!.visibility = View.VISIBLE
            false
        }
        //open front and back camera
        flipCamera!!.setOnClickListener { flipCamera() }
        iv_back_icon!!.setOnClickListener {
            val intent = Intent()
            setResult(300, intent)
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        camera!!.stopPreview()
        camera!!.release()
    }

    private fun init() {
        //find id
        surfaceView = findViewById(R.id.surfaceView)
        captureImagee = findViewById(R.id.captureImagee)
        iv_back_icon = findViewById(R.id.iv_back_icon)
        flipCamera = findViewById(R.id.flipCamera)
        tvRecordAudio = findViewById(R.id.tvRecordAudio)
        setCapture_image = findViewById(R.id.iv_captured_img)
        videoPreview = findViewById(R.id.videoPreview)

        tvRecordAudio!!.text = "00:00"
        surfaceHolder = surfaceView!!.holder
        surfaceHolder!!.addCallback(this)
        cameraId = Camera.CameraInfo.CAMERA_FACING_BACK
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

    }

    //flip camera method
    private fun flipCamera() {
        val id = if (cameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
            Camera.CameraInfo.CAMERA_FACING_FRONT
        else
            Camera.CameraInfo.CAMERA_FACING_BACK
        if (!openCamera(id)) {
            alertCameraDialog()
        }
    }

    //camera error dialog
    private fun alertCameraDialog() {}

    //capture image
    private fun takeImage() {
        tvRecordAudio!!.visibility = View.GONE

        camera!!.takePicture(null, null, object : Camera.PictureCallback {
            private var imageFile: File? = null
            override fun onPictureTaken(data: ByteArray, camera: Camera) {
                try {
                    // convert byte array into bitmap
                    var loadedImage: Bitmap? = null
                    var rotatedBitmap: Bitmap? = null
                    loadedImage = BitmapFactory.decodeByteArray(data, 0, data.size)

                    val rotateMatrix = Matrix()

                    if (isFrontFacing && takeActualRotation == 90) {
                        takeActualRotation = 270
                        rotateMatrix.postRotate(takeActualRotation.toFloat())
                    } else {
                        rotateMatrix.postRotate(rotation.toFloat())
                    }
                    rotatedBitmap = Bitmap.createBitmap(loadedImage!!, 0, 0, loadedImage.width, loadedImage.height, rotateMatrix, false)
                    val state = Environment.getExternalStorageState()
                    var folder: File? = null
                    if (state.contains(Environment.MEDIA_MOUNTED)) {
                        folder = File(Environment.getExternalStorageDirectory().toString() + "/SocketChat")
                    } else {
                        folder = File(Environment.getExternalStorageDirectory().toString() + "/SocketChat")
                    }
                    var success = true
                    if (!folder.exists()) {
                        success = folder.mkdirs()
                    }
                    if (success) {
                        val date = Date()
                        imageFile = File(folder.absolutePath + File.separator + Timestamp(date.time).toString() + "Image.jpg")
                        imageFile!!.createNewFile()

                    } else {
                        return
                    }

                    val ostream = ByteArrayOutputStream()

                    // save image into gallery
                    rotatedBitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, ostream)

                    val fout = FileOutputStream(imageFile!!)
                    fout.write(ostream.toByteArray())
                    fout.close()
                    val values = ContentValues()

                    //byte[] byteArray = stream.toByteArray();

                    values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())
                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
                    values.put(MediaStore.MediaColumns.DATA, imageFile!!.absolutePath)
                    filePath = imageFile!!.absolutePath
                    this@CustomCameraActivity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
                    Log.d("filePath.toString()", filePath!!.toString())

                    videoPreview!!.visibility = View.GONE
                    setCapture_image!!.visibility = View.VISIBLE
                    setCapture_image!!.setImageBitmap(rotatedBitmap)

                    val intent = Intent()
                    intent.putExtra("code", filePath)
                    intent.putExtra("type", "image")
                    setResult(VIDEO_CAPTURE, intent)
                    finish()

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        })
    }


    //start Time counter
    private fun startRepeatingTask() {
        cnt = 0
        mHandlerTask.run()
    }

    //release media recorder
    private fun releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder!!.reset() // clear recorder configuration
            mediaRecorder!!.release() // release the recorder object
            mediaRecorder = null
            camera!!.lock() // lock camera for later use
        }
    }

    @TargetApi(26)
    private fun prepareMediaRecorder(): Boolean {

        mediaRecorder = MediaRecorder()
        camera!!.unlock()
        mediaRecorder!!.setCamera(camera)
        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.CAMCORDER)
        mediaRecorder!!.setVideoSource(MediaRecorder.VideoSource.CAMERA)
        mediaRecorder!!.setOrientationHint(rotate)
        mediaRecorder!!.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW))
        val date = Date()
        val folder = File(Environment.getExternalStorageDirectory().toString() + "/SocketChat")
        mediaRecorder!!.setOutputFile(folder.absolutePath + Timestamp(date.time).toString() + "socketchat.mp4")
        ff = folder.absolutePath + Timestamp(date.time).toString() + "socketchat.mp4"
        mediaRecorder!!.setMaxDuration(600000) // Set max duration 60 sec.
        mediaRecorder!!.setMaxFileSize(50000000) // Set max file size 50M
        try {
            mediaRecorder!!.prepare()
        } catch (e: IllegalStateException) {
            releaseMediaRecorder()
            return false
        } catch (e: IOException) {
            releaseMediaRecorder()
            return false
        }

        return true
    }

    override fun surfaceCreated(surfaceHolder: SurfaceHolder) {
        if (!openCamera(Camera.CameraInfo.CAMERA_FACING_BACK)) {
            alertCameraDialog()
        }
    }

    //open camera by camera id
    private fun openCamera(id: Int): Boolean {
        var result = false
        cameraId = id
        releaseCamera()
        try {
            camera = Camera.open(cameraId)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (camera != null) {
            try {
                setUpCamera(camera!!)
                camera!!.setErrorCallback { error, camera -> }
                camera!!.setPreviewDisplay(surfaceHolder)
                camera!!.startPreview()
                result = true
            } catch (e: IOException) {
                e.printStackTrace()
                result = false
                releaseCamera()
            }

        }
        return result
    }

    private fun releaseCamera() {
        try {
            if (camera != null) {
                camera!!.setPreviewCallback(null)
                camera!!.setErrorCallback(null)
                camera!!.stopPreview()
                camera!!.release()
                camera = null
            }
        } catch (e: Exception) {
            e.printStackTrace()
            camera = null
        }

    }

    fun refreshCamera(camera: Camera?) {
        if (surfaceHolder!!.surface == null) {
            // preview surface does not exist
            return
        }
        // stop preview before making changes
        try {
            camera!!.stopPreview()
        } catch (e: Exception) {
            // ignore: tried to stop a non-existent preview
        }

        setCamera(camera!!)
        try {
            camera!!.setPreviewDisplay(surfaceHolder)
            camera.startPreview()
            safeToTakePicture = true
        } catch (e: Exception) {
        }

    }

    fun setCamera(camera1: Camera) {
        //method to set a camera instance
        camera = camera1
    }

    override fun onPointerCaptureChanged(hasCapture: Boolean) {}
    override fun surfaceChanged(surfaceHolder: SurfaceHolder, i: Int, i1: Int, i2: Int) {
        refreshCamera(camera)
    }

    override fun surfaceDestroyed(surfaceHolder: SurfaceHolder) {}
    //set front and back camera orientation
    @SuppressLint("WrongConstant")
    private fun setUpCamera(c: Camera) {
        val info = Camera.CameraInfo()
        Camera.getCameraInfo(cameraId, info)
        rotation = getWindowManager().getDefaultDisplay().getRotation()
        var degree = 0
        when (rotation) {
            Surface.ROTATION_0 -> degree = 0
            Surface.ROTATION_90 -> degree = 90
            Surface.ROTATION_180 -> degree = 180
            Surface.ROTATION_270 -> degree = 270
            else -> {
            }
        }
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            // frontFacing
            isFrontFacing = true
            rotation = (info.orientation + degree) % 360
            rotation = (360 - rotation) % 360
        } else {
            // Back-facing
            isFrontFacing = false
            rotation = (info.orientation - degree + 360) % 360
        }

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            rotate = (info.orientation + degree) % 360
        } else {
            rotate = (info.orientation - degree + 360) % 360
        }
        takeActualRotation = rotation
        c.setDisplayOrientation(rotation)
        val params = c.parameters
        params.setRotation(rotate)
    }

    companion object {
        private val INTERVAL = 1000 //1 seconds
    }
}