package com.schoolmanagement.parent.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.adapter.NotificationAdapter
import com.schoolmanagement.parent.pojo.NotificationDataPojo
import com.schoolmanagement.parent.pojo.NotificationPojo
import com.schoolmanagement.parent.pojo.StudentDetailsPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by upasna.mishra on 3/7/2018.
 */
class NotificationActivity : BaseActivity(), View.OnClickListener {

    private lateinit var iv_notification: ImageView
    private lateinit var rvNotifictionList: RecyclerView
    private lateinit var tv_no_data: TextView
    private lateinit var notificationList: ArrayList<NotificationDataPojo>
    private lateinit var adapter: NotificationAdapter
    private lateinit var mLayoutManager: LinearLayoutManager
    private var ctx: NotificationActivity = this
    private var searchView: android.support.v7.widget.SearchView? = null
    private var notificationDataFilter: ArrayList<NotificationDataPojo>? = null
    private lateinit var student_data: StudentDetailsPojo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        ParentConstant.ACTIVITIES.add(ctx)
        initialize()
    }

    private fun initialize() {
        setHeader(resources.getString(R.string.notification))
        student_data = getStudentData()!!
        iv_notification = findViewById(R.id.iv_notification)
        iv_notification.visibility = View.GONE
        notificationList = ArrayList()
        notificationDataFilter = ArrayList()
        mLayoutManager = LinearLayoutManager(this)
        rvNotifictionList = findViewById(R.id.rvNotifictionList)
        searchView = findViewById(R.id.search_box)
        tv_no_data = findViewById(R.id.tv_no_data)
        rvNotifictionList.layoutManager = mLayoutManager
        getNotification()
    }

    private fun setListener() {
        searchView!!.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                //Log.e("onQueryTextChange", "called");
                val task: ArrayList<NotificationDataPojo> = notificationList
                notificationDataFilter!!.clear()
                if (newText.isNotEmpty()) {
                    (0 until task.size)
                            .filter {
                                task[it].title.toLowerCase().contains(newText.toLowerCase()) ||
                                        task[it].created.toLowerCase().contains(newText.toLowerCase())
                            }
                            .forEach { notificationDataFilter!!.add(task[it]) }
                } else {
                    notificationDataFilter!!.addAll(task)
                }
                adapter = NotificationAdapter(ctx, notificationDataFilter!!)
                rvNotifictionList.adapter = adapter
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // Do your task here
                ctx.hideSoftKeyboard()
                return false
            }
        })
    }

    private fun getNotification() {
        if (ConnectionDetector.isConnected(this)) {
            val d = ParentDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<NotificationPojo> = apiService.getAllNotification(student_data.id)
            call.enqueue(object : Callback<NotificationPojo> {
                override fun onResponse(call: Call<NotificationPojo>, response: Response<NotificationPojo>?) {
                    if (response != null) {
                        if (response.body()!!.data.size > 0) {
                            rvNotifictionList.visibility = View.VISIBLE
                            tv_no_data.visibility = View.GONE
                            if (response.body()!!.status == "1") {
                                notificationList.addAll(response.body()!!.data)
                                adapter = NotificationAdapter(ctx, notificationList)
                                rvNotifictionList.adapter = adapter
                            } else {
                                displayToast(response.body()!!.message)
                            }
                        } else {
                            rvNotifictionList.visibility = View.GONE
                            tv_no_data.visibility = View.VISIBLE
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<NotificationPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    override fun onClick(view: View?) {
        if (view != null) {
            when (view.id) {
                R.id.row_click -> {
                    val notificationData = view.getTag(R.string.data) as NotificationDataPojo
                    redirectActivity(notificationData)
                }
            }
        }
    }

    private fun redirectActivity(notificationData: NotificationDataPojo) {
        val type = notificationData.type
        val id = notificationData.notification_id
        val description = notificationData.description
        val title = notificationData.notify_title
        val teacher_id = notificationData.teacher_id
        var intent: Intent?
        if (type == "AddProgress") {
            val handler = Handler()
            handler.postDelayed({
                removeActivity("StudentProgressActivity")
                intent = Intent(ctx, StudentProgressActivity::class.java)
                intent!!.putExtra("id", id)
                intent!!.putExtra("type", type)
                startActivity(intent)
            }, 150)
        } else if (type == "AddTask" || type == "UpdateTask") {
            val handler = Handler()
            handler.postDelayed({
                removeAllActivitiesWithHome()
                val intentHome = Intent(this, HomeActivity::class.java)
                intentHome.putExtra("content_type", type)
                startActivity(intentHome)
                finish()
            }, 0)
        } else if (type == "AddDiary") {
            val handler = Handler()
            handler.postDelayed({
                removeActivity("NoticeBoardAttachFileActivity")
                startActivity(Intent(ctx, NoticeBoardAttachFileActivity::class.java).putExtra("from", "Diary").putExtra("diary_id", id).putExtra("desc", description).putExtra("header", title).putExtra("teacher_id", teacher_id))
            }, 150)
        } else if (type == "AddNotice") {
            val handler = Handler()
            handler.postDelayed({
                removeActivity("NoticeBoardActivity")
                startActivity(Intent(ctx, NoticeBoardActivity::class.java))
            }, 150)
        } else if (type == "AddEvent") {
            val handler = Handler()
            handler.postDelayed({
                removeActivity("EventDetailActivity")
                startActivity(Intent(ctx, EventDetailActivity::class.java).putExtra("event_id", id))
            }, 150)
        }

    }
}