package com.schoolmanagement.parent.activity

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.pojo.AboutSchoolDataPojo
import com.schoolmanagement.parent.pojo.AboutSchoolPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by arpit.jain on 2/16/2018.
 */
class AboutSchoolActivity : BaseActivity() {

    private var ctx: AboutSchoolActivity = this
    private var dialog: ParentDialog? = null
    private lateinit var phone: TextView
    private lateinit var email: TextView
    private lateinit var mobile: TextView
    private lateinit var webAddress: TextView
    private lateinit var fbAddress: TextView
    private lateinit var twitterAddress: TextView
    private lateinit var address: TextView
    private lateinit var workingHours: TextView
    private lateinit var theStory: TextView
    private lateinit var foundingDate: TextView
    private lateinit var aboutFounder: TextView
    private lateinit var workingHoursLayout: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)
        initialize()
        setListener()
        ParentConstant.ACTIVITIES.add(ctx)
        setDrawerAndToolbar(resources.getString(R.string.about))
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        phone = findViewById(R.id.phone)
        email = findViewById(R.id.email)
        mobile = findViewById(R.id.mobile)
        webAddress = findViewById(R.id.web)
        fbAddress = findViewById(R.id.fb)
        twitterAddress = findViewById(R.id.twitter)
        address = findViewById(R.id.address)
        workingHours = findViewById(R.id.working_hours)
        theStory = findViewById(R.id.the_story)
        foundingDate = findViewById(R.id.founding_date)
        aboutFounder = findViewById(R.id.about_founder)
        workingHoursLayout = findViewById(R.id.working_hours_layout)

        aboutUs()
    }

    private fun setListener() {

    }

    private fun aboutUs() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<AboutSchoolPojo> = apiService.aboutUs(ctx.getFromPrefs(ParentConstant.SCHOOL_ID).toString())
            call.enqueue(object : Callback<AboutSchoolPojo> {
                override fun onResponse(call: Call<AboutSchoolPojo>, response: Response<AboutSchoolPojo>?) {
                    if (response != null) {
                        workingHoursLayout.removeAllViews()
                        if (response.body()!!.status == "1") {
                            val data: AboutSchoolDataPojo = response.body()!!.data
                            phone.text = "+"+data.country_code+" "+data.area_code+" "+data.phone
                            email.text = data.email
                            mobile.text = data.mobile
                            webAddress.text = data.web_address
                            fbAddress.text = data.fb_address
                            twitterAddress.text = data.twitter_address
                            if(data.address2 == " "){
                                address.text = data.address1+", "+data.city+", "+data.state+", "+data.country+", "+data.pin
                            }else{
                                address.text = data.address1+", "+data.address2+", "+data.city+", "+data.state+", "+data.country+", "+data.pin
                            }

                            try {
                                val obj = JSONObject(data.working_hours)
                                val keys = obj.keys()
                                while (keys.hasNext()) {
                                    val itemView = LayoutInflater.from(ctx).inflate(R.layout.row_layout_working_hours, null, false)
                                    val day : TextView = itemView.findViewById(R.id.day)
                                    val time : TextView = itemView.findViewById(R.id.time)
                                    val key = keys.next() as String
                                    if (obj.get(key) is JSONObject) {
                                        day.text = key
                                        val objVal = obj.getJSONObject(key)
                                        val timeVal = objVal.opt("start").toString()+" - "+objVal.opt("end").toString()
                                        time.text = timeVal
                                    }
                                    workingHoursLayout.addView(itemView)
                                }

                                Log.d("My App", obj.toString())
                            } catch (t: Throwable) {
                                Log.e("My App", "Could not parse malformed JSON: \"" + data.working_hours + "\"")
                                workingHours.text = data.working_hours
                            }

                            theStory.text = data.story
                            if(data.founding_date != "") {
                                val new_date = data.founding_date.replace(".000Z", "").split("T")
                                foundingDate.text = ctx.changeEventDetailDate(new_date[0]/* +" "+ new_date[1]*/)
                            }
                            aboutFounder.text = data.about_founder
                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<AboutSchoolPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }
}