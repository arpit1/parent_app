package com.schoolmanagement.parent.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.adapter.FeedbackAdapter
import com.schoolmanagement.parent.pojo.FeedbackDataPojo
import com.schoolmanagement.parent.pojo.FeedbackPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by arpit.jain on 2/14/2018.
 */
class FeedbackActivity : BaseActivity() {

    private var ctx: FeedbackActivity = this
    private lateinit var rvFeedback: RecyclerView
    private lateinit var ivAddIcon: ImageView
    private lateinit var noDataAvailable: TextView
    private lateinit var adapter: FeedbackAdapter
    private var feedbackList: ArrayList<FeedbackDataPojo>? = null
    private var dialog: ParentDialog? = null
    private lateinit var mLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)
        ParentConstant.ACTIVITIES.add(ctx)
        setDrawerAndToolbar(resources.getString(R.string.feedback))
        initialize()
        setListener()
    }

    override fun onResume() {
        super.onResume()
        feedbackList = ArrayList()
        if (ParentConstant.FEEDBACK_REFRESH)
            feedbackList()
    }

    private fun initialize() {
        mLayoutManager = LinearLayoutManager(ctx)

        dialog = ParentDialog(ctx)

        ivAddIcon = findViewById(R.id.iv_add_icon)
        rvFeedback = findViewById(R.id.rvFeedback)
        noDataAvailable = findViewById(R.id.noDataAvailable)

        ivAddIcon.visibility = View.VISIBLE
        rvFeedback.layoutManager = mLayoutManager

        feedbackList()
    }

    private fun setListener() {
        ivAddIcon.setOnClickListener {
            startActivity(Intent(this, AddFeedbackActivity::class.java))
        }
    }

    private fun feedbackList() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<FeedbackPojo> = apiService.feedbackList(ctx.getFromPrefs(ParentConstant.USER_ID).toString())
            call.enqueue(object : Callback<FeedbackPojo> {
                override fun onResponse(call: Call<FeedbackPojo>, response: Response<FeedbackPojo>?) {
                    if (response != null) {
                        ParentConstant.FEEDBACK_REFRESH = false
                        if (response.body()!!.data.size > 0) {
                            rvFeedback.visibility = View.VISIBLE
                            noDataAvailable.visibility = View.GONE
                            if (response.body()!!.status == "1") {
                                feedbackList!!.addAll(response.body()!!.data)
                                adapter = FeedbackAdapter(ctx, feedbackList!!)
                                rvFeedback.adapter = adapter
                            } else {
                                ctx.displayToast(response.body()!!.message)
                            }
                        } else {
                            rvFeedback.visibility = View.GONE
                            noDataAvailable.visibility = View.VISIBLE
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<FeedbackPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }
}