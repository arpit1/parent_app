package com.schoolmanagement.parent.activity

import android.os.Bundle
import android.os.Handler
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.pojo.StudentDetailsPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.schoolmanagement.parent.util.ViewAnimationUtils
import com.teacherapp.pojo.*
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by arpit.jain on 2/14/2018.
 */
class StudentProgressActivity : BaseActivity() {

    private var row_student_progress: LinearLayout? = null
    private var ctx: StudentProgressActivity = this
    private lateinit var mInflator: LayoutInflater
    private lateinit var arrQuestionAnswer: ArrayList<QuestionFieldPojo>
    private var dialog: ParentDialog? = null
    private lateinit var student_name: TextView
    private lateinit var student_data: StudentDetailsPojo
    private lateinit var sp_selectTemplate: Spinner
    private lateinit var templateList: ArrayList<TemplateDataPojo>
    private var templateId: String? = null
    private lateinit var ll_rmark: LinearLayout
    private lateinit var remark: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student_progress)
        initialize()
        setListener()
        ParentConstant.ACTIVITIES.add(ctx)

        setDrawerAndToolbar(resources.getString(R.string.student_progress))
    }

    private fun initialize() {
        arrQuestionAnswer = ArrayList()
        templateList = ArrayList()
        dialog = ParentDialog(ctx)
        mInflator = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        student_data = getStudentData()!!
        row_student_progress = findViewById(R.id.row_student_progress)
        sp_selectTemplate = findViewById(R.id.sp_selectTemplate)
        ll_rmark = findViewById(R.id.ll_rmark)

        remark = findViewById(R.id.remark)
        remark.movementMethod = ScrollingMovementMethod()
        student_name = findViewById(R.id.student_name)
        student_name.text = getStudentName()
        getTemplate()
    }

    private fun setListener() {
        sp_selectTemplate.onItemSelectedListener = templateSelectedListener
        sp_selectTemplate.adapter = templateSpinnerAdapter
    }

    private fun getTemplate() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<TemplatePojo> = apiService.getTamplate(student_data.id)
            call.enqueue(object : Callback<TemplatePojo> {
                override fun onResponse(call: Call<TemplatePojo>, response: Response<TemplatePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            templateList.clear()
                            val templateDataPojo = TemplateDataPojo()
                            templateDataPojo.id = ("-1")
                            templateDataPojo.name = resources.getString(R.string.select_template)
                            templateList.add(0, templateDataPojo)
                            templateList.addAll(response.body()!!.data)
                            templateSpinnerAdapter.notifyDataSetChanged()
                            templateId = intent.getStringExtra("id")
                            if (templateId != null && templateId != "") {
                                for (i in 0 until templateList.size) {
                                    if (templateId == templateList[i].id) {
                                        sp_selectTemplate.setSelection(i)
                                        templateSpinnerAdapter.notifyDataSetChanged()
                                        break
                                    }
                                }
                            }
                        }
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<TemplatePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private val templateSelectedListener = object : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
            if (position != 0) {
                templateId = templateList[position].id
                studentProgress()
            } else {
                templateId = ""
                ll_rmark.visibility = View.GONE
                if (row_student_progress != null)
                    row_student_progress?.removeAllViews()
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private val templateSpinnerAdapter = object : BaseAdapter() {

        private var ftext: TextView? = null

        override fun getView(position: Int, _convertView: View?, parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = templateList[position].name
            return convertView
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return templateList[position].name
        }

        override fun getCount(): Int {
            return templateList.size
        }

        override fun getDropDownView(position: Int, _convertView: View?,
                                     parent: ViewGroup): View {
            var convertView = _convertView
            if (convertView == null) {
                convertView = mInflator.inflate(
                        R.layout.row_spinner, parent, false)
            }
            ftext = convertView!!.findViewById(R.id.spinnerTarget) as TextView
            ftext!!.text = templateList[position].name
            return convertView as View
        }
    }

    private fun studentProgress() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<QuestionPojo> = apiService.getQuestionAnswer(student_data.id, templateId.toString())
            call.enqueue(object : Callback<QuestionPojo> {
                override fun onResponse(call: Call<QuestionPojo>, response: Response<QuestionPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            remark.text = response.body()!!.data[0].questions[0].remarks
                            arrQuestionAnswer = response.body()!!.data
                            val handler = Handler()
                            handler.postDelayed({
                                setQuestionData(response)
                            }, 100)
                        } else {
                            row_student_progress?.visibility = View.GONE
                            ll_rmark.visibility = View.GONE
                        }
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<QuestionPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setQuestionData(response: Response<QuestionPojo>) {
        row_student_progress?.removeAllViews()

        for (i in 0 until response.body()!!.data.size) {
            val itemView = LayoutInflater.from(ctx).inflate(R.layout.row_layout_student_progress, null, false)
            val lin_child_title = itemView.findViewById(R.id.lin_child_title) as LinearLayout
            val tv_header_name = itemView.findViewById<TextView>(R.id.tv_header_name) as TextView
            val lin_header_title = itemView.findViewById<LinearLayout>(R.id.lin_header_title) as LinearLayout
            val header_plus_icon = itemView.findViewById<ImageView>(R.id.header_plus_icon) as ImageView
            tv_header_name.text = response.body()!!.data[i].subject
            lin_header_title.setOnClickListener {
                clickPlusIcon(lin_child_title, header_plus_icon)
            }
            val arr: ArrayList<QuestionDataFieldPojo> = response.body()!!.data[i].questions
            for (j in 0 until arr.size) {
                val childView = LayoutInflater.from(ctx).inflate(R.layout.activity_child_student_progress, null, false)
                val tv_child_name = childView.findViewById(R.id.tv_child_name) as TextView
                val answer = childView.findViewById(R.id.answer) as TextView
                tv_child_name.text = arr[j].question
                answer.text = arr[j].answer
                lin_child_title.addView(childView)
            }
            row_student_progress?.addView(itemView)
        }
        row_student_progress?.visibility = View.VISIBLE
        ll_rmark.visibility = View.VISIBLE
    }

    private fun clickPlusIcon(lin_child_title: LinearLayout, header_plus_icon: ImageView) {
        if (lin_child_title.visibility == View.GONE) {
            ViewAnimationUtils.expand(lin_child_title)
            header_plus_icon.setImageResource(R.mipmap.minus_icon_s)
        } else {
            ViewAnimationUtils.collapse(lin_child_title)
            header_plus_icon.setImageResource(R.mipmap.plus_icon_s)
        }
    }
}