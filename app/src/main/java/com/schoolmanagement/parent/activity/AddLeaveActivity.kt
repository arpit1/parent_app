package com.schoolmanagement.parent.activity

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.pojo.BasePojo
import com.schoolmanagement.parent.pojo.StudentDetailsPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by upasna.mishra on 2/21/2018.
 */
class AddLeaveActivity : BaseActivity() {

    private lateinit var studentProfilePic: CircleImageView
    private lateinit var studentPrimaryEmail: TextView
    private lateinit var llStartDate: LinearLayout
    private lateinit var startDate: TextView
    private lateinit var llEndDate: LinearLayout
    private lateinit var endDate: TextView
    private lateinit var description: TextView
    private lateinit var tv_submit: TextView
    private var year: Int = 0
    private var month: Int = 0
    private var day: Int = 0
    private lateinit var calendar: Calendar
    private lateinit var calendarEnd: Calendar
    private var yearEnd: Int = 0
    private var monthEnd: Int = 0
    private var dayEnd: Int = 0
    private var ctx: AddLeaveActivity = this
    private var startDateSelect: String = null.toString()
    private var endDateSelect: String = null.toString()
    private lateinit var iv_navi: ImageView
    private lateinit var iv_back: ImageView
    private lateinit var student_list: ArrayList<StudentDetailsPojo>
    private lateinit var mInflator: LayoutInflater
    private var dialog: ParentDialog? = null
    private lateinit var student_name: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_leave)
        initialize()
        setListener()
    }

    private fun initialize() {
        setHeader(resources.getString(R.string.add_leave))
        dialog = ParentDialog(ctx)
        mInflator = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        calendar = Calendar.getInstance()
        calendarEnd = Calendar.getInstance()
        year = calendar.get(Calendar.YEAR)
        month = calendar.get(Calendar.MONTH)
        day = calendar.get(Calendar.DAY_OF_MONTH)
        student_list = ArrayList()
        studentProfilePic = findViewById(R.id.studentProfilePic)
        studentPrimaryEmail = findViewById(R.id.studentPrimaryEmail)
        llStartDate = findViewById(R.id.llStartDate)
        startDate = findViewById(R.id.startDate)
        llEndDate = findViewById(R.id.llEndDate)
        endDate = findViewById(R.id.endDate)
        description = findViewById(R.id.description)
        description.setOnTouchListener({ v, motionEvent ->
            if (v.id == R.id.description) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(
                            false)
                }
            }
            false
        })
        tv_submit = findViewById(R.id.tv_submit)
        iv_navi = findViewById(R.id.iv_navi)
        iv_back = findViewById(R.id.iv_back)
        student_name = findViewById(R.id.student_name)
        iv_navi.visibility = View.GONE
        iv_back.visibility = View.VISIBLE

        //set user data
        if (getStudentData()!!.profile_pic != null && getStudentData()!!.profile_pic != "") {
            setProfileImageInLayout(ctx, ParentConstant.IMAGE_STUDENT_PROFILE + getStudentData()!!.profile_pic, studentProfilePic)
        } else {
            studentProfilePic.setImageResource(R.mipmap.usericon)
        }
        student_name.text = getStudentName()
        studentPrimaryEmail.text = getFromPrefs(ParentConstant.PRIMARY_EMAIL)

    }

    private fun setListener() {
        llStartDate .setOnClickListener {
            showDatePicker()
        }

        llEndDate.setOnClickListener {
            showDatePickerEndDate()
        }

        tv_submit.setOnClickListener {
            hideSoftKeyboard()
            if (validate()) {
                addStudentLeave()
            }
        }
    }

    private fun validate(): Boolean {
        val start_date = startDate.text.toString()
        val end_date = endDate.text.toString()
        val description = description.text.toString().trim()
        return when {
            start_date == resources.getString(R.string.start_date) -> {
                displayToast(resources.getString(R.string.start_date_msg))
                false
            }
            end_date == resources.getString(R.string.end_date) -> {
                displayToast(resources.getString(R.string.end_date_msg))
                false
            }
            description.isEmpty() -> {
                displayToast(resources.getString(R.string.description_msg))
                false
            }
            else -> true
        }
    }

    private fun addStudentLeave() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.addStudentLeave(getFromPrefs(ParentConstant.SCHOOL_ID).toString(),
                    getFromPrefs(ParentConstant.USER_ID).toString(), getStudentData()!!.id, startDate.text.toString(), endDate.text.toString(),
                    description.text.toString().trim(), getStudentName())
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            ParentConstant.LEAVES_REFRESH = true
                            finish()
                        }
                        displayToast(response.body()!!.message)
                    }
                    d.dismiss()
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun showDatePicker() {
        val dialog = DatePickerDialog(ctx, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendar.set(year, month, day)
            ctx.year = year
            ctx.month = month
            ctx.day = day

            ctx.yearEnd = year
            ctx.monthEnd = month
            ctx.dayEnd = day
            updateLabel(startDate, calendar)
            endDate.text = resources.getString(R.string.end_date)
            startDateSelect = getDateFormat().format(calendar.time)
            endDateSelect = ""
        }, year, month, day)
        val cal = Calendar.getInstance()
        cal.add(Calendar.MONTH, 2)
        dialog.datePicker.minDate = System.currentTimeMillis() - 1000
        dialog.datePicker.maxDate = cal.timeInMillis
        dialog.setTitle(resources.getString(R.string.start_date))
        dialog.show()
    }

    private fun showDatePickerEndDate() {
        val dialog = DatePickerDialog(ctx, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendarEnd.set(year, month, day)
            ctx.yearEnd = year
            ctx.monthEnd = month
            ctx.dayEnd = day
            updateLabel(endDate, calendarEnd)
            endDateSelect = getDateFormat().format(calendarEnd.time)
        }, yearEnd, monthEnd, dayEnd)
        val cal = Calendar.getInstance()
        cal.add(Calendar.MONTH, 2)
        dialog.datePicker.minDate = calendar.timeInMillis
        dialog.datePicker.maxDate = cal.timeInMillis
        dialog.setTitle(resources.getString(R.string.end_date))
        dialog.show()
    }

    private fun updateLabel(textView: TextView, cal: Calendar) {
        textView.text = getDateFormat().format(cal.time)
    }
}