package com.schoolmanagement.parent.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.adapter.ProfilePagerAdapter
import com.schoolmanagement.parent.util.ParentConstant

/**
 * Created by arpit.jain on 2/14/2018.
 */
class ProfileActivity : BaseActivity(), View.OnClickListener {

    private val ctx: ProfileActivity = this
    private lateinit var profileViewPager: ViewPager
    private lateinit var mAdapter: ProfilePagerAdapter
    private lateinit var profileLayout: LinearLayout
    private lateinit var programLayout: LinearLayout
    private lateinit var profileSelector: LinearLayout
    private lateinit var programSelector: LinearLayout
    private lateinit var profileText: TextView
    private lateinit var programText: TextView
    private lateinit var iv_edit_profile: ImageView
    private lateinit var ivChangePassword: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        ParentConstant.ACTIVITIES.add(ctx)
        setDrawerAndToolbar(resources.getString(R.string.profile))

        initialize()
        setListener()
    }

    private fun initialize() {
        profileLayout = findViewById(R.id.profileLayout)
        programLayout = findViewById(R.id.programLayout)
        profileSelector = findViewById(R.id.profileSelector)
        programSelector = findViewById(R.id.programSelector)
        profileText = findViewById(R.id.profileText)
        programText = findViewById(R.id.programText)
        iv_edit_profile = findViewById(R.id.iv_edit_profile)
        ivChangePassword = findViewById(R.id.ivChangePassword)
        iv_edit_profile.visibility = View.VISIBLE
        ivChangePassword.visibility = View.VISIBLE
        profileViewPager = findViewById(R.id.profile_view_pager)
        mAdapter = ProfilePagerAdapter(supportFragmentManager)
        profileViewPager.adapter = mAdapter
        profileViewPager.offscreenPageLimit = 1
    }

    private fun setListener() {
        profileViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                setStyle(position)
            }

            override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

            override fun onPageScrollStateChanged(arg0: Int) {}
        })

        iv_edit_profile.setOnClickListener{
            startActivity(Intent(ctx, EditProfileActivity::class.java).putExtra("from", "edit"))
        }

        ivChangePassword.setOnClickListener {
            startActivity(Intent(ctx, ChangePasswordActivity::class.java))
        }
        profileLayout.setOnClickListener(ctx)
        programLayout.setOnClickListener(ctx)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.profileLayout -> if (profileViewPager.currentItem != 0) {
                profileViewPager.currentItem = 0
                setStyle(0)
            }

            R.id.programLayout -> if (profileViewPager.currentItem != 1) {
                profileViewPager.currentItem = 1
                setStyle(1)
            }
            else -> {
            }
        }
    }

    fun setStyle(position: Int) {
        when (position) {
            0 -> {
                iv_edit_profile.visibility = View.VISIBLE
                profileText.setTextColor(ContextCompat.getColor(ctx, R.color.header_color))
                programText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))

                profileSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.header_color))
                programSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
            }
            1 -> {
                iv_edit_profile.visibility = View.GONE
                profileText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
                programText.setTextColor(ContextCompat.getColor(ctx, R.color.header_color))

                profileSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
                programSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.header_color))
            }
        }
    }
}