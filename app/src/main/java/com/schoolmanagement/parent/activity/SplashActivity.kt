package com.schoolmanagement.parent.activity

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.pojo.FilterPojo
import com.schoolmanagement.parent.service.ExitService
import com.schoolmanagement.parent.util.ParentConstant
import com.schoolmanagement.parent.util.ParentConstant.ACTIVITIES
import com.splunk.mint.Mint
import com.teacherapp.util.ParentDialog


class SplashActivity : BaseActivity() {

    private var splashTimeHandler: Handler? = null
    private var finalizer: Runnable? = null
    private var dialog: ParentDialog? = null
    private val ctx: SplashActivity = this
    private lateinit var filterPojo: FilterPojo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initialize()
        Mint.initAndStartSession(this.application, "72176152")

        val intent = Intent(this, ExitService::class.java)
        startService(intent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
    }

    private fun initialize() {
        filterPojo = FilterPojo()
        saveFilterData(filterPojo)
        splashTimeHandler = Handler()
        dialog = ParentDialog(ctx)
        ACTIVITIES = ArrayList()
        ACTIVITIES.clear()
        println("xhxhxhxhxxh " + getFromPrefs(ParentConstant.DEVICE_ID))
        finalizer = Runnable {
            when {
                getFromPrefs(ParentConstant.USER_ID).equals("") -> {
                    startActivity(Intent(ctx, LoginActivity::class.java))
                }
                getFromPrefs(ParentConstant.FORCE_CHANGE) != "1" -> {
                    startActivity(Intent(ctx, ForceChangePasswordActivity::class.java))
                }
                getFromPrefs(ParentConstant.ENHANCED_PROFILE) != "1" -> {
                    startActivity(Intent(ctx, EditProfileActivity::class.java)
                            .putExtra("from", "first_login"))
                }
                else -> {
                    subscribeSocket()
                    val intent = Intent(ctx, HomeActivity::class.java)
                    intent.putExtra("content_id", getIntent().getStringExtra("content_id"))
                    intent.putExtra("content_type", getIntent().getStringExtra("content_type"))
                    intent.putExtra("description", getIntent().getStringExtra("description"))
                    intent.putExtra("title", getIntent().getStringExtra("title"))
                    intent.putExtra("sender_id", getIntent().getStringExtra("sender_id"))
                    intent.putExtra("teacher_id", getIntent().getStringExtra("teacher_id"))
                    startActivity(intent)
                }
            }
            finish()
        }
        splashTimeHandler!!.postDelayed(finalizer, 2000)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (splashTimeHandler != null && finalizer != null)
            splashTimeHandler!!.removeCallbacks(finalizer)
    }
}