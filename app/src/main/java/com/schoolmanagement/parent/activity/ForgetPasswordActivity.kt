package com.schoolmanagement.parent.activity

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.pojo.BasePojo
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by arpit.jain on 2/14/2018.
 */
class ForgetPasswordActivity : BaseActivity() {

    private lateinit var iv_navi : ImageView
    private lateinit var iv_back : ImageView
    private lateinit var iv_notification : ImageView
    private lateinit var iv_logout : ImageView
    private lateinit var et_email : EditText
    private lateinit var tv_submit : TextView
    private var dialog: ParentDialog? = null
    private var ctx : ForgetPasswordActivity = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_password)
        initialize()
        setListener()
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        setHeader(resources.getString(R.string.header_forgot))
        iv_navi = findViewById(R.id.iv_navi)
        iv_back = findViewById(R.id.iv_back)
        iv_notification = findViewById(R.id.iv_notification)
        iv_logout = findViewById(R.id.iv_logout)
        et_email = findViewById(R.id.et_email)
        tv_submit = findViewById(R.id.tv_submit)
        iv_navi.visibility = View.GONE
        iv_back.visibility = View.VISIBLE
        iv_notification.visibility = View.GONE
        iv_logout.visibility = View.GONE
    }

    private fun setListener() {
        tv_submit.setOnClickListener() {
            hideSoftKeyboard()
            if(validate()){
                forgotPassword(et_email.text.toString())
            }
        }
    }

    private fun validate(): Boolean {
        val email = et_email.text.toString().trim()
        return if (email.isEmpty()) {
            displayToast(resources.getString(R.string.user_name_msg))
            false
        } else {
            true
        }
    }

    private fun forgotPassword(email: String) {
        if (ConnectionDetector.isConnected(this)) {
            val d = ParentDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.forgotPassword(email)
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            displayToast(response.body()!!.message)
                            finish()
                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }
}