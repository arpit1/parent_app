package com.schoolmanagement.parent.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.adapter.HomePagerAdapter
import com.schoolmanagement.parent.util.ParentConstant.ACTIVITIES
import kotlinx.android.synthetic.main.app_bar.*


class HomeActivity : BaseActivity(), View.OnClickListener {

    private val ctx: HomeActivity = this
    private var doubleBackToExitPressedOnce = false
    private lateinit var homeViewPager: ViewPager
    private lateinit var mAdapter: HomePagerAdapter
    private lateinit var tabHome: LinearLayout
    private lateinit var tabChat: LinearLayout
    private lateinit var schoolDiary: LinearLayout
    private lateinit var tabCalendar: LinearLayout
    private lateinit var tabGallery: LinearLayout
    private lateinit var homeText: TextView
    private lateinit var chatText: TextView
    private lateinit var schoolDiaryText: TextView
    private lateinit var calendarText: TextView
    private lateinit var galleryText: TextView
    private lateinit var homeImage: ImageView
    private lateinit var chatImage: ImageView
    private lateinit var schoolDiaryImage: ImageView
    private lateinit var calendarImage: ImageView
    private lateinit var galleryImage: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ACTIVITIES.add(ctx)
        setDrawerAndToolbar(resources.getString(R.string.home))

        initialize()
        setListener()
    }

    private fun initialize() {
        tabHome = findViewById(R.id.tab_home)
        tabChat = findViewById(R.id.tab_chat)
        schoolDiary = findViewById(R.id.school_diary)
        tabCalendar = findViewById(R.id.tab_calendar)
        tabGallery = findViewById(R.id.tab_gallery)

        homeImage = findViewById(R.id.homeImage)
        chatImage = findViewById(R.id.chatImage)
        schoolDiaryImage = findViewById(R.id.schoolDiaryImage)
        calendarImage = findViewById(R.id.calendarImage)
        galleryImage = findViewById(R.id.galleryImage)

        homeText = findViewById(R.id.homeText)
        chatText = findViewById(R.id.chatText)
        schoolDiaryText = findViewById(R.id.schoolDiaryText)
        calendarText = findViewById(R.id.calendarText)
        galleryText = findViewById(R.id.galleryText)
        homeViewPager = findViewById(R.id.home_view_pager)
        mAdapter = HomePagerAdapter(supportFragmentManager)
        homeViewPager.adapter = mAdapter
        homeViewPager.offscreenPageLimit = 4
        mSocket.connect()
    }

    private fun setListener() {
        homeViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                setStyle(position)
            }

            override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

            override fun onPageScrollStateChanged(arg0: Int) {}
        })

        tabHome.setOnClickListener(this)
        tabChat.setOnClickListener(this)
        schoolDiary.setOnClickListener(this)
        tabCalendar.setOnClickListener(this)
        tabGallery.setOnClickListener(this)

        val type = intent.getStringExtra("content_type")
        val id = intent.getStringExtra("content_id")
        val description = intent.getStringExtra("description")
        val title = intent.getStringExtra("title")
        val sender_id = intent.getStringExtra("sender_id")
        val teacher_id = intent.getStringExtra("teacher_id")
        println("teacher id is   "+teacher_id)
        var intent: Intent?
        if (type != null) {
            if (type == "AddProgress") {
                val handler = Handler()
                handler.postDelayed({
                    removeActivity("StudentProgressActivity")
                    intent = Intent(ctx, StudentProgressActivity::class.java)
                    intent!!.putExtra("id", id)
                    intent!!.putExtra("type", type)
                    startActivity(intent)
                }, 150)
            } else if (type == "AddTask" || type == "UpdateTask") {
                val handler = Handler()
                handler.postDelayed({
                    tabCalendar.performClick()
                }, 150)
            } else if (type == "AddDiary") {
                val handler = Handler()
                handler.postDelayed({
                    removeActivity("NoticeBoardAttachFileActivity")
                    startActivity(Intent(ctx, NoticeBoardAttachFileActivity::class.java).putExtra("from", "Diary").putExtra("diary_id", id).putExtra("desc", description).putExtra("header", title).putExtra("teacher_id", teacher_id))
                }, 150)
            } else if (type == "AddNotice") {
                val handler = Handler()
                handler.postDelayed({
                    removeActivity("NoticeBoardActivity")
                    startActivity(Intent(ctx, NoticeBoardActivity::class.java))
                }, 150)
            } else if (type == "AddEvent") {
                val handler = Handler()
                handler.postDelayed({
                    removeActivity("EventDetailActivity")
                    startActivity(Intent(ctx, EventDetailActivity::class.java).putExtra("event_id", id))
                }, 150)
            } else if (type == "chat") {
                val handler = Handler()
                handler.postDelayed({
                    if (homeViewPager.currentItem != 1) {
                        homeViewPager.currentItem = 1
                        setStyle(1)
                    }
                }, 150)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        /* val jsonObject = JSONObject()
         try {
             val userID = "P_"+getFromPrefs(ParentConstant.USER_ID)
             jsonObject.put("user_id",userID )
             mSocket.emit("disconnect", jsonObject)
         } catch (e: JSONException) {
             e.printStackTrace()
         }*/
        mSocket.disconnect()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tab_home -> if (homeViewPager.currentItem != 0) {
                homeViewPager.currentItem = 0
                setStyle(0)
            }
            R.id.tab_chat -> if (homeViewPager.currentItem != 1) {
                homeViewPager.currentItem = 1
                setStyle(1)
            }
            R.id.school_diary -> if (homeViewPager.currentItem != 2) {
                homeViewPager.currentItem = 2
                setStyle(2)
            }
            R.id.tab_calendar -> if (homeViewPager.currentItem != 3) {
                homeViewPager.currentItem = 3
                setStyle(3)
            }
            R.id.tab_gallery -> if (homeViewPager.currentItem != 4) {
                homeViewPager.currentItem = 4
                setStyle(4)
            }
            else -> {
            }
        }
    }

    private fun setStyle(position: Int) {
        hideSoftKeyboard()
        when (position) {
            0 -> {
                setFooterColor(homeText, chatText, calendarText, schoolDiaryText, galleryText)
                homeImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.home_h))
                chatImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.chat))
                schoolDiaryImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.schooldiary_tab_icon))
                calendarImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.calendar))
                galleryImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.gallery))
                setDrawerAndToolbar(resources.getString(R.string.home))
                iv_filter.visibility = View.GONE
            }
            1 -> {
                setFooterColor(chatText, homeText, calendarText, schoolDiaryText, galleryText)
                homeImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.home))
                chatImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.chat_h))
                schoolDiaryImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.schooldiary_tab_icon))
                calendarImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.calendar))
                galleryImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.gallery))
                setDrawerAndToolbar(resources.getString(R.string.chat))
                iv_filter.visibility = View.GONE
            }
            2 -> {
                setFooterColor(schoolDiaryText, homeText, calendarText, chatText, galleryText)
                homeImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.home))
                chatImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.chat))
                schoolDiaryImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.schooldiary_tab_icon_h))
                calendarImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.calendar))
                galleryImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.gallery))
                setDrawerAndToolbar(resources.getString(R.string.school_diary))
                iv_filter.visibility = View.GONE
            }
            3 -> {
                setFooterColor(calendarText, homeText, chatText, schoolDiaryText, galleryText)
                homeImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.home))
                chatImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.chat))
                schoolDiaryImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.schooldiary_tab_icon))
                calendarImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.calendar_h))
                galleryImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.gallery))
                setDrawerAndToolbar(resources.getString(R.string.calendar))
                iv_filter.visibility = View.GONE
            }
            4 -> {
                setFooterColor(galleryText, homeText, calendarText, schoolDiaryText, chatText)
                homeImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.home))
                chatImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.chat))
                schoolDiaryImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.schooldiary_tab_icon))
                calendarImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.calendar))
                galleryImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.gallery_h))
                setDrawerAndToolbar(resources.getString(R.string.gallery))
                iv_filter.visibility = View.VISIBLE
            }
        }
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExitPressedOnce = true
        displayToast(resources.getString(R.string.back_exit))
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }
}
