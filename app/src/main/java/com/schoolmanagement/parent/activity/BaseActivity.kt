package com.schoolmanagement.parent.activity

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.content.ContextCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.GestureDetector
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.fragment.DrawerFragment
import com.schoolmanagement.parent.listener.NetworkStateReceiver
import com.schoolmanagement.parent.pojo.*
import com.schoolmanagement.parent.util.ChatApplication
import com.schoolmanagement.parent.util.ParentConstant
import com.squareup.picasso.Picasso
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import io.socket.client.Socket
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.UnsupportedEncodingException
import java.text.DateFormatSymbols
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

/**
 * Created by arpit.jain on 2/14/2018.
 */
open class BaseActivity : AppCompatActivity(), NetworkStateReceiver.NetworkStateReceiverListener {

    private var mDrawerLayout: DrawerLayout? = null
    private var sharedPreferences: SharedPreferences? = null
    private val ctx: BaseActivity = this
    lateinit var mSocket: Socket
    private var network_available = true
    private var networkStateReceiver: NetworkStateReceiver? = NetworkStateReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        val app = application as ChatApplication
        mSocket = app.socket!!
    }

    fun setHeader(header: String) {
        val tvHeader = findViewById<TextView>(R.id.tv_header)
        tvHeader.text = header
        val drawerButton = findViewById<ImageView>(R.id.iv_navi)
        drawerButton.visibility = View.GONE

        val back = findViewById<ImageView>(R.id.iv_back)
        back.visibility = View.VISIBLE

        val ivLogout: ImageView = findViewById(R.id.iv_logout)
        val ivNotification: ImageView = findViewById(R.id.iv_notification)

        ivNotification.setOnClickListener {
            removeActivity("NotificationActivity")
            startActivity(Intent(ctx, NotificationActivity::class.java))
        }

        back.setOnClickListener {
            hideSoftKeyboard()
            ctx.onBackPressed()
        }
        ivLogout.setOnClickListener {
            appLogout()
        }
    }

    interface ClickListener {
        fun onClick(view: View, position: Int)

        fun onLongClick(view: View?, position: Int)
    }

    class RecyclerTouchListener(context: Context, recyclerView: RecyclerView, private val clickListener: ClickListener?) : RecyclerView.OnItemTouchListener {

        private val gestureDetector: GestureDetector

        init {
            gestureDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapUp(e: MotionEvent): Boolean {
                    return true
                }

                override fun onLongPress(e: MotionEvent) {
                    val child = recyclerView.findChildViewUnder(e.x, e.y)
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildAdapterPosition(child))
                    }
                }
            })
        }

        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {

            val child = rv.findChildViewUnder(e.x, e.y)
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildAdapterPosition(child))
            }
            return false
        }

        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

        }
    }

    fun encode(message: String): String? {
        var base64: String? = null
        try {
            val data = message.toByteArray(charset("UTF-8"))
            base64 = Base64.encodeToString(data, Base64.DEFAULT)
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        return base64
    }

    fun decode(message: String): String? {
        var text: String? = null
        val data = Base64.decode(message, Base64.DEFAULT)
        try {
            text = String(data)
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        return text
    }

    fun setDrawerAndToolbar(name: String): Toolbar {
        val appbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(appbar)
        supportActionBar!!.title = ""

        val drawerButton = findViewById<ImageView>(R.id.iv_navi)
        val header = findViewById<TextView>(R.id.tv_header)
        val ivLogout: ImageView = findViewById(R.id.iv_logout)
        val ivNotification: ImageView = findViewById(R.id.iv_notification)
        val ivFilter: ImageView = findViewById(R.id.iv_filter)

        header.text = name
        mDrawerLayout = findViewById(R.id.drawer_layout)
        drawerButton.setOnClickListener {
            hideSoftKeyboard()
            if (mDrawerLayout != null && !mDrawerLayout!!.isDrawerOpen(Gravity.START))
                mDrawerLayout!!.openDrawer(Gravity.START)
            else if (mDrawerLayout != null)
                mDrawerLayout!!.closeDrawers()
        }
        if (mDrawerLayout != null) {
            val fragment = supportFragmentManager.findFragmentById(R.id.fragment_drawer) as DrawerFragment
            fragment.setUp(mDrawerLayout!!)
        }

        ivLogout.setOnClickListener {
            appLogout()
        }
        ivNotification.setOnClickListener {
            removeActivity("NotificationActivity")
            startActivity(Intent(ctx, NotificationActivity::class.java))
        }
        ivFilter.setOnClickListener {
            val intent = Intent(this, FilterGalleryActivity::class.java)
            startActivity(intent)
        }
        return appbar
    }


    fun displayToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun hideSoftKeyboard() {
        if (currentFocus != null) {
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
    }

    fun getDateFormat(): SimpleDateFormat {
        val myFormat = "yyy-MM-dd" //In which you need put here
        return SimpleDateFormat(myFormat, Locale.US)
    }

    fun isValidEmail(target: CharSequence): Boolean {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    fun saveIntoPrefs(key: String, value: String) {
        val prefs = getSharedPreferences(ParentConstant.PREF_NAME, Context.MODE_PRIVATE)
        val edit = prefs.edit()
        edit.putString(key, value)
        edit.apply()
    }

    fun saveStudentData(studentData: StudentDetailsPojo?) {
        val sharedPreferences = getSharedPreferences(ParentConstant.PREF_NAME, Activity.MODE_PRIVATE)
        val prefsEditor = sharedPreferences.edit()
        val gson = Gson()
        val json = gson.toJson(studentData)
        prefsEditor.putString("student_details", json)
        prefsEditor.apply()
    }

    fun getStudentData(): StudentDetailsPojo? {
        val sharedPreferences = getSharedPreferences(ParentConstant.PREF_NAME, Activity.MODE_PRIVATE)
        val gson = Gson()
        val json = sharedPreferences.getString("student_details", "")
        return gson.fromJson<StudentDetailsPojo>(json, StudentDetailsPojo::class.java)
    }

    fun saveFilterData(filterData: FilterPojo?) {
        val sharedPreferences = getSharedPreferences(ParentConstant.PREF_NAME, Activity.MODE_PRIVATE)
        val prefsEditor = sharedPreferences.edit()
        val gson = Gson()
        val json = gson.toJson(filterData)
        prefsEditor.putString("filter_details", json)
        prefsEditor.apply()
    }

    fun getFilterData(): FilterPojo? {
        val sharedPreferences = getSharedPreferences(ParentConstant.PREF_NAME, Activity.MODE_PRIVATE)
        val gson = Gson()
        val json = sharedPreferences.getString("filter_details", "")
        return gson.fromJson<FilterPojo>(json, FilterPojo::class.java)
    }

    fun getTeacherName(teacherData: FeedbackDataPojo): String {
        return if (teacherData.mname != "")
            teacherData.fname + " " + teacherData.mname + " " + teacherData.lname
        else
            teacherData.fname + " " + teacherData.lname
    }

    fun getTeacherName(teacherData: TaskDataPojo): String {
        return if (teacherData.mname != "")
            teacherData.fname + " " + teacherData.mname + " " + teacherData.lname
        else
            teacherData.fname + " " + teacherData.lname
    }

    fun getStudentName(): String {
        val studentPojo: StudentDetailsPojo = getStudentData()!!
        return if (studentPojo.student_mname != "")
            studentPojo.student_fname + " " + studentPojo.student_mname + " " + studentPojo.student_lname
        else
            studentPojo.student_fname + " " + studentPojo.student_lname
    }

    fun getFromPrefs(key: String): String? {
        val prefs = getSharedPreferences(ParentConstant.PREF_NAME, Context.MODE_PRIVATE)
        return prefs.getString(key, ParentConstant.DEFAULT_VALUE)
    }

    /*fun getDayOfMonthSuffix(n: Int): String {
        checkArgument(n in 1..31)
        if (n in 11..13) {
            return "th"
        }
        return when (n % 10) {
            1 -> "st"
            2 -> "nd"
            3 -> "rd"
            else -> "th"
        }
    }*/

    fun setFooterColor(tab1: TextView, tab2: TextView, tab3: TextView, tab4: TextView, tab5: TextView) {
        tab1.setTextColor(ContextCompat.getColor(ctx, R.color.tab_selected_color))
        tab2.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
        tab3.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
        tab4.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
        tab5.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
    }

    fun getClassShortName(): String {
        val am = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val cn = am.getRunningTasks(1)[0].topActivity
        return cn.shortClassName
    }

    fun removeAllActivities() {
        (0 until ParentConstant.ACTIVITIES.size)
                .filter { ParentConstant.ACTIVITIES[it] != null && !ParentConstant.ACTIVITIES[it].toString().contains(resources.getString(R.string.package_name) + "HomeActivity") }
                .forEach { ParentConstant.ACTIVITIES[it]?.finish() }
    }

    fun removeAllActivitiesWithHome() {
        (0 until ParentConstant.ACTIVITIES.size)
                .filter { ParentConstant.ACTIVITIES[it] != null }
                .forEach { ParentConstant.ACTIVITIES[it]?.finish() }
    }

    fun getHomeActivity(): HomeActivity? {
        return (ParentConstant.ACTIVITIES.size - 1 downTo 0)
                .firstOrNull { ParentConstant.ACTIVITIES[it] != null && ParentConstant.ACTIVITIES[it].toString().contains(resources.getString(R.string.package_name) + "HomeActivity") }
                ?.let { ParentConstant.ACTIVITIES[it] as HomeActivity }
    }

    fun removeActivity(activity: String) {
        for (i in ParentConstant.ACTIVITIES.size - 1 downTo 0) {
            if (ParentConstant.ACTIVITIES[i] != null && ParentConstant.ACTIVITIES[i].toString().contains(resources.getString(R.string.package_name) + activity)) {
                ParentConstant.ACTIVITIES[i]?.finish()
                ParentConstant.ACTIVITIES.removeAt(i)
//                break
            }
        }
    }

    fun passCombination(pass: String): Boolean {
        val expression = "(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@\$%^&*-]).{6,}\$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(pass)
        return matcher.matches()
    }

    fun setProfileImageInLayout(ctx: Context, url: String?, image: ImageView) {
        if (url != null && url != "")
            Picasso.with(ctx).load(url).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(image)
    }

    fun setProfileImageInLayoutProfile(ctx: Context, url: String?, image: ImageView) {
        if (url != null && url != "")
            Picasso.with(ctx).load(url).placeholder(R.mipmap.usericonprofile).error(R.mipmap.usericonprofile).into(image)
    }

    fun setHomePostImageInLayout(ctx: Context, url: String?, image: ImageView) {
        if (url != null && url != "")
            Picasso.with(ctx).load(url).placeholder(R.mipmap.loginlogo).error(R.mipmap.loginlogo).into(image)
    }

    fun setHomePostImage(ctx: Context, url: String?, image: ImageView) {
        println("url is Base Activity  " + url)
        if (url != null && url != "") {
            Log.e("image url ", "!!!" + url);
            //Picasso.with(ctx).load(url).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).centerCrop().fit().into(image)
            Picasso.with(ctx).load(url).placeholder(R.mipmap.loginlogo).error(R.mipmap.loginlogo)
                    .into(image)
        }
        Picasso.with(ctx).load(url).placeholder(R.mipmap.loginlogo).error(R.mipmap.loginlogo).into(image)
    }

    fun setGifImage(ctx: Context, url: String?, image: ImageView) {
        if (url != null && url != "")
            Glide.with(ctx).load(url).asGif().placeholder(R.mipmap.loginlogo).into(image)
    }

    fun appLogout() {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setMessage(resources.getString(R.string.logout_msg))
        builder.setPositiveButton("Yes", { _, _ ->
            logout()
        })
        builder.setNegativeButton("No", { dialog, _ ->
            dialog.cancel()
        })
        val alert = builder.create()
        alert.show()
    }

    override fun onResume() {
        super.onResume()

        networkStateReceiver?.addListener(this)
        this.registerReceiver(networkStateReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        if (!getFromPrefs(ParentConstant.USER_ID).equals("")) {
            if (!mSocket.connected()) {
                mSocket.connect()
                subscribeSocket()
            }
        }
    }

    override fun onPause() {
        try {
            this.unregisterReceiver(networkStateReceiver)
        } catch (e: IllegalArgumentException) {
        }

        super.onPause()
    }

    override fun networkAvailable() {
        if (!network_available) {
            network_available = true
            displayToast("Network available")
            if (!mSocket.connected()) {
                mSocket.connect()
                subscribeSocket()
            }
        }
    }

    override fun networkUnavailable() {
        network_available = false
        displayToast("Network not available")
    }

    fun subscribeSocket() {
        if (!getFromPrefs(ParentConstant.USER_ID).equals("")) {
            mSocket.emit("subscribe", "P_" + getFromPrefs(ParentConstant.USER_ID).toString())
        }
    }

    private fun logout() {
        if (ConnectionDetector.isConnected(this)) {
            val d = ParentDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.logout(getFromPrefs(ParentConstant.USER_ID).toString())
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            saveIntoPrefs(ParentConstant.USER_ID, "")
                            saveIntoPrefs(ParentConstant.NAME, "")
                            saveIntoPrefs(ParentConstant.EMAIL, "")
                            saveIntoPrefs(ParentConstant.IMAGE, "")
                            val intent = Intent(ctx, LoginActivity::class.java)
                            startActivity(intent)
                            (0 until ParentConstant.ACTIVITIES.size)
                                    .filter { ParentConstant.ACTIVITIES[it] != null }
                                    .forEach { ParentConstant.ACTIVITIES[it]?.finish() }
                            mSocket.disconnect()
                            finish()
                        } else {
                            displayToast(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            displayToast(resources.getString(R.string.no_internet_connection))
        }
    }

    override fun onBackPressed() {
        if (mDrawerLayout != null && mDrawerLayout!!.isDrawerOpen(Gravity.START)) {
            mDrawerLayout!!.closeDrawers()
        } else
            super.onBackPressed()
    }

    fun changeDateTimeFormat(date: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val outputFormat = SimpleDateFormat("dd MMMM yyyy hh:mm a", Locale.US)
        val dateVal = inputFormat.parse(date)
        return outputFormat.format(dateVal)
    }

    fun parentChangeDateTimeFormat(date: String): String {
        val inputFormat = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.US)
        val outputFormat = SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US)
        val dateVal = inputFormat.parse(date)
        return outputFormat.format(dateVal)
    }

    fun changeEventDetailDate(date: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val outputFormat = SimpleDateFormat("dd MMMM yyyy", Locale.US)
        val dateVal = inputFormat.parse(date)
        return outputFormat.format(dateVal)
    }

    fun getDate(date: String): Date {
        val dateFormat = SimpleDateFormat("dd MMM, yyyy", Locale.US)
        var convertedDate = Date()
        try {
            convertedDate = dateFormat.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return convertedDate
    }

    fun changeDateFormatWithoutTime(date: String): String {
        val inputFormat = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        val outputFormat = SimpleDateFormat("dd MMMM yyyy", Locale.US)
        val dateVal = inputFormat.parse(date)
        return outputFormat.format(dateVal)
    }

    /**
     * Just a check to see if we have marshmallows (version 23)
     *
     * @return
     */
    private fun canMakeSmores(): Boolean {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
    }

    /**
     * a method that will centralize the showing of a snackbar
     */
    /*fun makePostRequestSnack(message: String, size: Int) {

        Toast.makeText(applicationContext, size.toString() + " " + message, Toast.LENGTH_SHORT).show()

        finish()
    }*/

    private fun hasPermission(permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canMakeSmores()) {
                return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
            }
        }
        return true
    }

    /**
     * method to determine whether we have asked
     * for this permission before.. if we have, we do not want to ask again.
     * They either rejected us or later removed the permission.
     *
     * @param permission
     * @return
     */
    private fun shouldWeAsk(permission: String): Boolean {
        return sharedPreferences!!.getBoolean(permission, true)
    }

    /**
     * we will save that we have already asked the user
     *
     * @param permission
     */
    fun markAsAsked(permission: String, sharedPreferences: SharedPreferences) {
        sharedPreferences.edit().putBoolean(permission, false).apply()
    }

    /**
     * We may want to ask the user again at their request.. Let's clear the
     * marked as seen preference for that permission.
     *
    //     * @param permission
     */
    /*fun clearMarkAsAsked(permission: String) {
        sharedPreferences?.edit()?.putBoolean(permission, true)?.apply()
    }*/

    fun findUnAskedPermissions(wanted: ArrayList<String>): ArrayList<String> {
        return wanted.filterNotTo(ArrayList()) { hasPermission(it) }
    }

    fun findRejectedPermissions(wanted: ArrayList<String>): ArrayList<String> {
        return wanted.filterTo(ArrayList()) { !hasPermission(it) && !shouldWeAsk(it) }
    }

    fun getChangedDate(date: String): String? {
        var result: String? = null
        val sds = date.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (sds.size > 2) {
            val day = sds[0]
            val month = getMonth(sds[1].toInt())

            val year = sds[2]
            result = "$year $month $day"
        }
        return result
    }

    private fun getMonth(month: Int): String {
        return DateFormatSymbols().shortMonths[month - 1]
    }
}