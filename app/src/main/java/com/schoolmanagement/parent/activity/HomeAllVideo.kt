package com.schoolmanagement.parent.activity

import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView
import android.widget.GridView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.adapter.HomeVideoAdapter
import com.schoolmanagement.parent.pojo.HomePostFilePojo

/**
 * Created by upasna.mishra on 2/14/2018.
 */
class HomeAllVideo : BaseActivity(){

    private lateinit var videoList : GridView
    private lateinit var video_list : ArrayList<HomePostFilePojo>
    private lateinit var homeVideoAdapter : HomeVideoAdapter
    private var ctx : HomeAllVideo = this
    private lateinit var event_name : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_allvideo)
        initialize()
        setListener()
    }

    private fun initialize() {
        event_name = intent.getStringExtra("event_name")
        setHeader(intent.getStringExtra("event_name"))
        video_list = intent.getSerializableExtra("video") as ArrayList<HomePostFilePojo>
        videoList = findViewById(R.id.videoList)
        homeVideoAdapter = HomeVideoAdapter(ctx, video_list)
        videoList.adapter = homeVideoAdapter
    }

    private fun setListener() {
        videoList.onItemClickListener = AdapterView.OnItemClickListener { _, _, pos, _ ->
            startActivity(Intent(ctx, FullVideoScreenActivity::class.java).putExtra("Image", video_list[pos].file)
                    .putExtra("event_name",event_name))
        }
    }
}