package com.schoolmanagement.parent.activity

import android.os.Bundle
import android.widget.ImageView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.util.ParentConstant

/**
 * Created by upasna.mishra on 4/14/2018.
 */
class GifFullScreenActivity : BaseActivity(){

    private lateinit var imageViewGif : ImageView
    private var gif_image : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gif_fullscreen)
        initialize()
    }

    private fun initialize() {
        setHeader(intent.getStringExtra("event_name"))
        imageViewGif = findViewById(R.id.imageViewGif)
        gif_image = ParentConstant.IMAGE_URl+intent.getStringExtra("gif_image")
        setGifImage(this, gif_image, imageViewGif)
    }
}