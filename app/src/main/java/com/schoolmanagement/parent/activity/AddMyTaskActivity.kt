package com.schoolmanagement.parent.activity

import android.app.AlarmManager
import android.app.DatePickerDialog
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.widget.EditText
import android.widget.TextView
import android.widget.TimePicker
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.pojo.BasePojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by upasna.mishra on 5/1/2018.
 */
class AddMyTaskActivity : BaseActivity() {

    private lateinit var et_title: EditText
    private lateinit var et_description: EditText
    private lateinit var tv_date: TextView
    private lateinit var tv_time: TextView
    private lateinit var submit: TextView
    private var calendar: Calendar? = null
    private var year: Int = 0
    private var month: Int = 0
    private var day: Int = 0
    private var ctx: AddMyTaskActivity = this
    private var dialog: ParentDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_teacher_task)
        initialize()
        setListener()
        setHeader(resources.getString(R.string.add_task))
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        et_title = findViewById(R.id.et_title)
        et_description = findViewById(R.id.description)
        et_description.setOnTouchListener({ v, motionEvent ->
            if (v.id == R.id.description) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(
                            false)
                }
            }
            false
        })
        tv_date = findViewById(R.id.tv_date)
        tv_time = findViewById(R.id.tv_time)
        submit = findViewById(R.id.submit)
        calendar = Calendar.getInstance()
        val c = Calendar.getInstance()
        year = c.get(Calendar.YEAR)
        month = c.get(Calendar.MONTH)
        day = c.get(Calendar.DAY_OF_MONTH)
    }

    private fun setListener() {

        tv_date.setOnClickListener {
            hideSoftKeyboard()
            showDatePicker()
        }

        tv_time.setOnClickListener {
            hideSoftKeyboard()
            showTimePicker()
        }

        submit.setOnClickListener {
            validate()
        }
    }

    private fun validate() {
        val date = tv_date.text.toString()
        val time = tv_time.text.toString()
        title = et_title.text.toString().trim()
        val description = et_description.text.toString().trim()
        when {
            title.isEmpty() -> displayToast(getString(R.string.enter_title))
            description.isEmpty() -> displayToast(resources.getString(R.string.description_msg))
            date.isEmpty() -> displayToast(resources.getString(R.string.select_date))
            time.isEmpty() -> displayToast(resources.getString(R.string.select_time))
            else -> taskList()
        }
    }

    private fun showDatePicker() {
        val dialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendar?.set(year, month, day)
            this.year = year
            this.month = month
            this.day = day
            updateLabel()
        }, year, month, day)
        dialog.datePicker.minDate = System.currentTimeMillis() - 1000
        dialog.show()
    }

    private fun showTimePicker() {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mcurrentTime.get(Calendar.MINUTE)
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(this@AddMyTaskActivity, object : TimePickerDialog.OnTimeSetListener {
            override fun onTimeSet(timePicker: TimePicker, selectedHour: Int, selectedMinute: Int) {
                val taskTime = "$selectedHour:$selectedMinute"

                updateTimeLabel(taskTime)
            }
        }, hour, minute, true)//Yes 24 hour time
        mTimePicker.show()

    }

    private fun updateLabel() {
        val myFormat = getString(R.string.dateformat)//In which you need put here

        val sdf = SimpleDateFormat(myFormat, Locale.US)
        tv_date.text = sdf.format(calendar!!.time)
    }

    private fun updateTimeLabel(taskTime: String) {
        val dtaskTime = SimpleDateFormat(getString(R.string.timeformat), Locale.US)
        try {
            val taskdate = dtaskTime.parse(taskTime)
            tv_time.text = dtaskTime.format(taskdate)
        } catch (e: ParseException) {
            Log.e("exception parsing ", "!!!" + e.toString())
        }
    }

    private fun taskList() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val taskDateTime = tv_date.text.toString() + " " + tv_time.text.toString()
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.addTask(getFromPrefs(ParentConstant.USER_ID).toString(),
                    et_title.text.toString().trim(), et_description.text.toString().trim(), taskDateTime)
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            ParentConstant.TASK_REFRESH = true
                            ctx.displayToast(response.body()!!.message)

                            //setting remindar...
                            //val remindarDate = tv_date.text.toString();
                            val reminadrTitle = et_title.text.toString().trim()
                            setReminderOnTask(taskDateTime, reminadrTitle)

                            finish()
                        } else {
                            ctx.displayToast(response.body()!!.message)
                        }

                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    //function for set remindar on teacher task...
    private fun setReminderOnTask(reminderDate: String, reminderTitle: String) {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val notificationIntent = Intent("android.media.action.PARENT_NOTIFICATION")
        notificationIntent.addCategory("android.intent.category.DEFAULT")
        notificationIntent.putExtra("notification_title", reminderTitle)
        val broadcast = PendingIntent.getBroadcast(this, 10001, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val Datesdf = SimpleDateFormat("dd-MM-yyyy hh:mm", Locale.US)
        val remindarDate = Datesdf.parse(reminderDate)

        val cal = Calendar.getInstance()
        cal.timeInMillis = remindarDate.time

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.timeInMillis, broadcast)
        }
    }
}