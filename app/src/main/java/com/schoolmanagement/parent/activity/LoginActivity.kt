package com.schoolmanagement.parent.activity

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.pojo.LoginDataPojo
import com.schoolmanagement.parent.pojo.LoginPojo
import com.schoolmanagement.parent.pojo.StudentPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginActivity : BaseActivity() {

    private lateinit var et_email: EditText
    private lateinit var et_password: EditText
    private lateinit var tv_login: TextView
    private lateinit var lin_forgot: LinearLayout
    private var ctx: LoginActivity = this
    private var dialog: ParentDialog? = null
    private lateinit var d : ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initialize()
        setListener()
    }

    private fun initialize() {
        et_email = findViewById(R.id.et_email)
        et_password = findViewById(R.id.et_password)
        tv_login = findViewById(R.id.tv_login)
        lin_forgot = findViewById(R.id.lin_forgot)
        dialog = ParentDialog(ctx)
    }

    private fun setListener() {
        lin_forgot.setOnClickListener {
            startActivity(Intent(ctx, ForgetPasswordActivity::class.java))
        }

        tv_login.setOnClickListener() {
            hideSoftKeyboard()
            if (validate()) {
                login(et_email.text.toString(), et_password.text.toString())
            }
        }
    }

    private fun validate(): Boolean {
        val email = et_email.text.toString().trim()
        val password = et_password.text.toString()
        return if (email.isEmpty()) {
            displayToast(resources.getString(R.string.user_name_msg))
            false
        }  else if (password.isEmpty()) {
            displayToast(resources.getString(R.string.password_msg))
            false
        } else {
            true
        }
    }

    private fun login(email: String, pass: String) {
        if (ConnectionDetector.isConnected(this)) {
            d = ParentDialog.showLoading(this)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<LoginPojo> = apiService.login(email,getFromPrefs(ParentConstant.DEVICE_ID).toString(), pass, "android")
            call.enqueue(object : Callback<LoginPojo> {
                override fun onResponse(call: Call<LoginPojo>, response: Response<LoginPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1" && response.body()!!.data != null) {
                            val data: LoginDataPojo? = response.body()!!.data
                            saveIntoPrefs(ParentConstant.USER_ID, data!!.id)
                            saveIntoPrefs(ParentConstant.SCHOOL_ID, data.school_id)
                            saveIntoPrefs(ParentConstant.NAME, data.name)
                            saveIntoPrefs(ParentConstant.IMAGE, data.profile_pic)
                            saveIntoPrefs(ParentConstant.EMAIL, data.email)
                            saveIntoPrefs(ParentConstant.FORCE_CHANGE, data.force_change_status)
                            saveIntoPrefs(ParentConstant.ENHANCED_PROFILE, data.profile_update_status)
                            saveIntoPrefs(ParentConstant.PRIMARY_EMAIL, data.primary_email)
                            subscribeSocket()
                            studentList()
                        } else {
                            d.dismiss()
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                       // d.dismiss()
                    }
                }

                override fun onFailure(call: Call<LoginPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun studentList() {
        if (ConnectionDetector.isConnected(ctx)) {
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<StudentPojo> = apiService.studentList(getFromPrefs(ParentConstant.USER_ID)!!)
            call.enqueue(object : Callback<StudentPojo> {
                override fun onResponse(call: Call<StudentPojo>, response: Response<StudentPojo>) {
                    if (response.body() != null) {
                        if (response.body()!!.status == "1") {
                            saveStudentData(response.body()!!.data)
                            when {
                                getFromPrefs(ParentConstant.FORCE_CHANGE) != "1" -> {
                                    startActivity(Intent(ctx, ForceChangePasswordActivity::class.java))
                                }
                                getFromPrefs(ParentConstant.ENHANCED_PROFILE) != "1" -> {
                                    startActivity(Intent(ctx, EditProfileActivity::class.java)
                                            .putExtra("from", "first_login"))
                                }
                                else -> {
                                    startActivity(Intent(ctx, HomeActivity::class.java))
                                }
                            }
                            finish()
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<StudentPojo>, t: Throwable) {
                    // Log error here since request failed
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }
}