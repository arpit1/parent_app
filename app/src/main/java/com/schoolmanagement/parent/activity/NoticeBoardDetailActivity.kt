package com.schoolmanagement.parent.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.pojo.NoticeBoardDataPojo
import com.schoolmanagement.parent.util.ParentConstant
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by upasna.mishra on 2/22/2018.
 */
class NoticeBoardDetailActivity : BaseActivity(){

    private lateinit var noticeBoardDetail : NoticeBoardDataPojo
    private lateinit var schoolProfilePic : CircleImageView
    private lateinit var noticeTitle : TextView
    private lateinit var noticeTime : TextView
    private lateinit var description : TextView
    private var ctx : NoticeBoardDetailActivity = this
    private lateinit var attach_icon : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notice_board_detail)
        setHeader(resources.getString(R.string.notice_board_detail))
        ParentConstant.ACTIVITIES.add(ctx)
        initialize()
        setListener()
    }

    private fun initialize() {
        noticeBoardDetail = intent.getSerializableExtra("noticeBoardList") as NoticeBoardDataPojo
        schoolProfilePic = findViewById(R.id.schoolProfilePic)
        noticeTitle = findViewById(R.id.noticeTitle)
        noticeTime = findViewById(R.id.noticeTime)
        description = findViewById(R.id.description)
        attach_icon = findViewById(R.id.attach_icon)
        if (noticeBoardDetail.logo != null && noticeBoardDetail.logo != "") {
            setProfileImageInLayout(ctx, ParentConstant.SCHOOL_PROFILE_PIC + noticeBoardDetail.logo, schoolProfilePic)
        } else {
            schoolProfilePic.setImageResource(R.mipmap.usericon)
        }
        noticeTitle.text = noticeBoardDetail.title
        noticeTime.text = noticeBoardDetail.created
        description.text = noticeBoardDetail.description
        if(noticeBoardDetail.file.size > 0 )
            attach_icon.visibility = View.VISIBLE
        else
            attach_icon.visibility =View.GONE
    }

    private fun setListener() {
        attach_icon.setOnClickListener {
            removeActivity("NoticeBoardAttachFileActivity")
            startActivity(Intent(ctx, NoticeBoardAttachFileActivity::class.java).putExtra("fileList", noticeBoardDetail.file).putExtra("from", "Notice"))
        }
    }
}