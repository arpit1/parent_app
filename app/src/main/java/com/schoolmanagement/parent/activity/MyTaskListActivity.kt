package com.schoolmanagement.parent.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.adapter.MyTaskAdpater
import com.schoolmanagement.parent.pojo.BasePojo
import com.schoolmanagement.parent.pojo.TeacherTaskDataPojo
import com.schoolmanagement.parent.pojo.TeacherTaskPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by upasna.mishra on 5/1/2018.
 */
class MyTaskListActivity : BaseActivity() {

    private lateinit var rvTaskList: RecyclerView
    private lateinit var noDataAvailable: TextView
    private var dialog: ParentDialog? = null
    private lateinit var mLayoutManager: LinearLayoutManager
    private var ctx: MyTaskListActivity = this
    private lateinit var teacher_task_list: ArrayList<TeacherTaskDataPojo>
    private lateinit var adapter: MyTaskAdpater
    private lateinit var iv_add_icon: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_teacher_task_list)
        initialize()
        setListener()
        ParentConstant.ACTIVITIES.add(ctx)
    }

    private fun initialize() {
        setDrawerAndToolbar(resources.getString(R.string.teacher_task))
        dialog = ParentDialog(ctx)

        mLayoutManager = LinearLayoutManager(ctx)
        rvTaskList = findViewById(R.id.rvTaskList)
        noDataAvailable = findViewById(R.id.noDataAvailable)
        iv_add_icon = findViewById(R.id.iv_add_icon)

        rvTaskList.layoutManager = mLayoutManager
        iv_add_icon.visibility = View.VISIBLE
        taskList()
    }

    private fun setListener() {
        iv_add_icon.setOnClickListener {
            startActivity(Intent(ctx, AddMyTaskActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        teacher_task_list = ArrayList()
        if (ParentConstant.TASK_REFRESH)
            taskList()
    }

    private fun taskList() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<TeacherTaskPojo> = apiService.parenttaskList(getFromPrefs(ParentConstant.USER_ID).toString())
            call.enqueue(object : Callback<TeacherTaskPojo> {
                override fun onResponse(call: Call<TeacherTaskPojo>, response: Response<TeacherTaskPojo>?) {
                    if (response != null) {
                        if (response.body()!!.data.size > 0) {
                            ParentConstant.TASK_REFRESH = false
                            rvTaskList.visibility = View.VISIBLE
                            noDataAvailable.visibility = View.GONE
                            if (response.body()!!.status == "1") {
                                teacher_task_list.addAll(response.body()!!.data)
                                adapter = MyTaskAdpater(ctx, teacher_task_list)
                                rvTaskList.adapter = adapter
                            } else {
                                ctx.displayToast(response.body()!!.message)
                            }
                        } else {
                            rvTaskList.visibility = View.GONE
                            noDataAvailable.visibility = View.VISIBLE
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<TeacherTaskPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    fun updateTask(taskId: String, completeStatus: String) {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.teacherTaskUpdate(taskId, completeStatus)
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            ctx.displayToast(response.body()!!.message)
                        }
                        //adapter.notifyDataSetChanged()
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog?.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }
}