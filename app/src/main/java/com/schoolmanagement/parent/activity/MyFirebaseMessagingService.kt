package com.schoolmanagement.parent.activity

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.ActivityManager
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.AsyncTask
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.util.Base64
import android.util.Log
import android.view.View
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.schoolmanagement.parent.R
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

@Suppress("DEPRECATION")
/**
 * Created by arpit.jain on 3/27/2018.
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {
    private val TAG = "MyFirebaseMsgService"

    private var image: String? = null
    private var video: String? = null
    private var pi: PendingIntent? = null

    private var bitmap: Bitmap? = null
    private var ctx: Context? = null
    private var message: String? = null
    private var title: String? = null
    private var type: String? = null
    private var id: String? = null
    private var description: String? = null
    private var titleHeader: String? = null
    private var sender_id: String? = null
    private var teacher_id: String? = null

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        println("xhxhxhxhxhxhx onMessageReceived ${remoteMessage.data["type"]}")
        Log.d(TAG, "From: " + remoteMessage.from)
        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
        }

        ctx = applicationContext

        message = remoteMessage.data["body"]
        title = remoteMessage.data["title"]

        sender_id = remoteMessage.data["sender_id"]

        image = remoteMessage.data["image"]
        type = remoteMessage.data["type"]
        id = remoteMessage.data["id"]

        teacher_id = remoteMessage.data["teacher_id"]
        titleHeader = remoteMessage.data["title"]
        description = remoteMessage.data["desc"]
        if (type == "chat") {
            if (sender_id != null && !message!!.toLowerCase().contains(".jpg") && !message!!.toLowerCase().contains(".jpeg") && !message!!.toLowerCase().contains(".png") && !message!!.toLowerCase().contains(".mp4") && !message!!.toLowerCase().contains(".3gp") && !message!!.toLowerCase().contains(".mov")) {
                message = decode(message!!)
            } else if (message!!.toLowerCase().contains(".jpg") || message!!.toLowerCase().contains(".jpeg") || message!!.toLowerCase().contains(".png")) {
                image = message
                message = resources.getString(R.string.new_image)
            } else if (message!!.toLowerCase().contains(".mp4") || message!!.toLowerCase().contains(".mov") || message!!.toLowerCase().contains(".3gp")) {
                message = resources.getString(R.string.new_video)
            }
        }

        val am = applicationContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val cn = am.getRunningTasks(1)[0].topActivity
        if (cn.shortClassName != ".activity.ChatScreenActivity")
            sendNotification(message)
    }

    private fun decode(message: String): String? {
        var text: String? = null
        val data = Base64.decode(message, Base64.DEFAULT)
        try {
            text = String(data)
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        return text
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param message FCM message body received.
     */

    private fun sendNotification(message: String?) {
        val intent = Intent(this, SplashActivity::class.java)
        intent.putExtra("content_type", type)
        intent.putExtra("content_id", id)
        intent.putExtra("description", description)
        intent.putExtra("title", title)
        intent.putExtra("sender_id", sender_id)
        intent.putExtra("teacher_id", teacher_id)
        // set intent so it does not start a new activity
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        val resultPendingIntent = PendingIntent.getActivity(applicationContext, Random().nextInt(), intent, PendingIntent.FLAG_UPDATE_CURRENT)
        pi = resultPendingIntent

        if (image != null && image!!.isNotEmpty())
            GetNotificationTask().execute()
        else
            sendBigTextStyleNotification()
    }

    private fun sendBigTextStyleNotification() {

        val notificationManager = ctx!!.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        val mBuilder = NotificationCompat.Builder(ctx)
        val notification = mBuilder.setSmallIcon(R.mipmap.notificationicon_tray).setTicker(title).setWhen(0).setColor(ContextCompat.getColor(ctx!!, R.color.button_color))
                .setAutoCancel(true).setContentTitle(title)
                .setStyle(NotificationCompat.BigTextStyle().bigText(message)).setContentIntent(pi)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLargeIcon(BitmapFactory.decodeResource(ctx!!.resources, R.mipmap.ic_launcher))
                .setContentText(message).build()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val smallIconViewId = resources.getIdentifier("right_icon", "id", android.R::class.java.`package`.name)
            if (smallIconViewId != 0) {
                if (notification.contentIntent != null)
                    notification.contentView?.setViewVisibility(smallIconViewId, View.INVISIBLE)

                if (notification.headsUpContentView != null)
                    notification.headsUpContentView?.setViewVisibility(smallIconViewId, View.INVISIBLE)

                if (notification.bigContentView != null)
                    notification.bigContentView?.setViewVisibility(smallIconViewId, View.INVISIBLE)
            }
        }

        notificationManager.notify(Random().nextInt(), notification)

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("NewApi")
    fun sendBigPictureStyleNotification() {
        val notificationManager = ctx!!
                .getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val mBuilder = NotificationCompat.Builder(ctx)
        val notification = mBuilder.setSmallIcon(R.mipmap.notificationicon_tray).setTicker(title).setWhen(0)
                .setAutoCancel(true).setContentTitle(title).setColor(ContextCompat.getColor(ctx!!, R.color.button_color))
                .setStyle(NotificationCompat.BigPictureStyle(mBuilder).bigPicture(bitmap).setSummaryText(message))
                .setContentIntent(pi).setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentText(message)
                .setLargeIcon(BitmapFactory.decodeResource(ctx!!.resources,
                        R.mipmap.ic_launcher)).build()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val smallIconViewId = resources.getIdentifier("right_icon", "id", android.R::class.java.`package`.name)

            if (smallIconViewId != 0) {
                if (notification.contentView != null)
                    notification.contentView?.setViewVisibility(smallIconViewId, View.INVISIBLE)

                if (notification.headsUpContentView != null)
                    notification.headsUpContentView?.setViewVisibility(smallIconViewId, View.INVISIBLE)

                if (notification.bigContentView != null)
                    notification.bigContentView?.setViewVisibility(smallIconViewId, View.INVISIBLE)
            }
        }
        notificationManager.notify(Random().nextInt(), notification)
    }

    private inner class GetNotificationTask : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void): Void? {
            bitmap = getBitmapFromURL(image)
            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            super.onPostExecute(aVoid)
            sendBigPictureStyleNotification()
        }
    }

    fun getBitmapFromURL(strURL: String?): Bitmap? {
        return try {
            val url = URL(strURL)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
// sendBigPictureStyleNotification();
            BitmapFactory.decodeStream(input)
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }

    }
}