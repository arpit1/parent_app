package com.schoolmanagement.parent.activity

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.widget.LinearLayout
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.util.ParentConstant

/**
 * Created by arpit.jain on 2/15/2018.
 */
class ChatActivity : BaseActivity() {

    private val ctx : ChatActivity = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_chat)

        ParentConstant.ACTIVITIES.add(ctx)
        setDrawerAndToolbar(resources.getString(R.string.chat))

        initialize()
        setListener()
    }

    private fun initialize() {
        val tabChat: LinearLayout = findViewById(R.id.tab_chat)
        tabChat.setBackgroundColor(ContextCompat.getColor(ctx, R.color.tab_selected_color))
    }

    private fun setListener() {

    }
}