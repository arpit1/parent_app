package com.schoolmanagement.parent.pojo

import java.io.Serializable

/**
 * Created by upasna.mishra on 2/21/2018.
 */
class NoticeBoardPojo : BasePojo() , Serializable{

    lateinit var data : ArrayList<NoticeBoardDataPojo>
}