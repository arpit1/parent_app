package com.schoolmanagement.parent.pojo

import java.io.Serializable

/**
 * Created by upasna.mishra on 2/16/2018.
 */
class TeacherPojo : BasePojo(), Serializable{

    lateinit var data : ArrayList<TeacherDataPojo>
}