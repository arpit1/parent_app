package com.schoolmanagement.parent.pojo

import java.io.Serializable

/**
 * Created by upasna.mishra on 2/21/2018.
 */
class NoticeBoardDataPojo : Serializable{

    var id : String = ""
    var school_id : String = ""
    var type : String = ""
    var title : String = ""
    var description : String = ""
    var created : String = ""
    var name : String = ""
    var logo : String? = ""
    lateinit var file : ArrayList<NoticeBoardFileDataPojo>
}