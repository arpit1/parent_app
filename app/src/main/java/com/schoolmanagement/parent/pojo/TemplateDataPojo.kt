package com.teacherapp.pojo

/**
 * Created by upasna.mishra on 3/28/2018.
 */
class TemplateDataPojo{
    var id : String = ""
    var school_id : String = ""
    var name : String = ""
    var program : String = ""
    var start_date : String = ""
    var end_date : String = ""
    var evaluation_criteria1 : String = ""
    var evaluation_criteria2 : String = ""
    var evaluation_criteria3 : String = ""
    var evaluation_criteria4 : String = ""
    var status : String = ""
    var created : String = ""


}