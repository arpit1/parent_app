package com.schoolmanagement.parent.pojo

import java.io.Serializable

/**
 * Created by upasna.mishra on 2/15/2018.
 */
class HomePostDataPojo : Serializable{
    var id : String = ""
    var school_id : String = ""
    var category : String = ""
    var event_name : String = ""
    var album_name : String = ""
    var discussion : String = ""
    var file : ArrayList<HomePostFilePojo>? = null
    var uploaded_by : String = ""
    var upload_type : String = ""
    var dates : String? = ""
    var created_at : String = ""
    var updated_at : String = ""
    var uploaderName : String = ""
    var uploaderEmail : String = ""
    var profile_pic : String? = ""
    var time : String = ""
    var type : String = ""
    var status : String = ""
    var registrationDetails : String = ""
    var details : String = ""
    var audience : String = ""
    var start_date : String = ""
    var end_date : String = ""
    var start_time : String = ""
    var end_time : String = ""
}