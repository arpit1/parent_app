package com.schoolmanagement.parent.pojo

/**
 * Created by arpit.jain on 2/19/2018.
 */
class CountryDataPojo {
    val id :String = ""
    val country :String = ""
    val code :String = ""
    val dial_code :String = ""
    val currency_code :String = ""
}