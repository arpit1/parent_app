package com.schoolmanagement.parent.pojo

/**
 * Created by upasna.mishra on 5/1/2018.
 */
class TeacherTaskDataPojo(){

    var id : String = ""
    var title : String = ""
    var teacher_id : String = ""
    var description : String = ""
    var dates : String = ""
    var created_at : String = ""
    var status : String = ""
}