package com.schoolmanagement.parent.pojo

/**
 * Created by arpit.jain on 2/20/2018.
 */
class AboutSchoolDataPojo {
    var id : String = ""
    var school_id : String = ""
    var phone : String = ""
    var email : String = ""
    var mobile : String = ""
    var web_address : String = ""
    var fb_address : String = ""
    var twitter_address : String = ""
    var address1 : String = ""
    var address2 : String = ""
    var working_hours : String = ""
    var story : String = ""
    var founding_date : String = ""
    var about_founder : String = ""
    var created : String = ""
    var country_code : String = ""
    var area_code : String = ""
    var city : String = ""
    var state : String = ""
    var country : String = ""
    var pin : String = ""

}