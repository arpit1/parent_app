package com.schoolmanagement.parent.pojo

import java.io.Serializable

/**
 * Created by upasna.mishra on 2/21/2018.
 */
class NoticeBoardFileDataPojo : Serializable {

    var file : String? = ""
    var created : String = ""
    var id : String = ""
    var diary_id : String = ""
    var created_at : String = ""
    var title : String = ""
    var description : String = ""
}