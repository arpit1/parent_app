package com.schoolmanagement.parent.pojo

/**
 * Created by hitesh.mathur on 3/27/2018.
 */
class ChatPojo {
    var sender_id: String? = null
    var receiver_id: String? = null
    var message: String? = null
    var type: String? = null
    var message_side: String? = null
    var image_thumb: String? = null
    var image_size_height: String? = null
    var image_size_width: String? = null
    var isSend: String? = null
    var isRead: String? = null
    var time: String? = null
    var chat_id: String? = null
    var image_url: String? = null
    var video_url: String? = null
    var video: String? = null
    var audio: String? = null
    var file: String? = null
    var isPass: String? = null
    var dates: String? = null
    var isPending: String? = null
    var created_at: String? = null
}