package com.schoolmanagement.parent.pojo

/**
 * Created by arpit.jain on 2/26/2018.
 */
class SchoolDiaryDataPojo {
    var created_at : String = ""
    var id : String = ""
    var classes : String = ""
    var time : String = ""
    var title : String = ""
    var description : String = ""
    var fname : String = ""
    var mname : String = ""
    var lname : String = ""
    var profile_pic : String? = ""
    var qualification : String = ""
    var teacher_id : String = ""
}