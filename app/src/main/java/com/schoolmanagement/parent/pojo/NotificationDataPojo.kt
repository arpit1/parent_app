package com.schoolmanagement.parent.pojo

/**
 * Created by upasna.mishra on 3/30/2018.
 */
class NotificationDataPojo {

    var id : String = ""
    var notification_id : String = ""
    var school_id : String = ""
    var teacher_id : String = ""
    var student_id : String = ""
    var type : String = ""
    var title : String = ""
    var description : String = ""
    var created : String = ""
    var logo : String = ""
    var profile_pic : String = ""
    var notify_title : String = ""
}