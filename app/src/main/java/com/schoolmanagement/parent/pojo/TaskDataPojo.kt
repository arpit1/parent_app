package com.schoolmanagement.parent.pojo

/**
 * Created by upasna.mishra on 2/23/2018.
 */
class TaskDataPojo {

    var id : String = ""
    var school_id : String = ""
    var teacher_id : String = ""
    var student_id : String = ""
    var title : String = ""
    var description : String = ""
    var start_date : String = ""
    var end_date : String = ""
    var reminder_time : String = ""
    var assign_name : String = ""
    var assign_email : String = ""
    var complete_status : String = ""
    var createt_at : String = ""
    var qualification : String = ""
    var fname : String = ""
    var mname : String = ""
    var lname : String = ""
}