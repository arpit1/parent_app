package com.schoolmanagement.parent.pojo

import java.io.Serializable

/**
 * Created by upasna.mishra on 2/16/2018.
 */
class TeacherDataPojo : Serializable {

    var id : String = ""
    var school_id : String = ""
    var fname : String = ""
    var mname : String = ""
    var lname : String = ""
    var email : String = ""
    var username : String = ""
    var mobile_number : String = ""
    var emeregency_contact_name : String = ""
    var emergency_contact_no : String = ""
    var password : String = ""
    var profile_pic : String? = ""
    var address1 : String = ""
    var address2 : String? = ""
    var address3 : String? = ""
    var city : String = ""
    var state : String = ""
    var country : String = ""
    var pincode : String = ""
    var qualification : String = ""
    var specialization : String = ""
    var passport_id : String = ""
    var program : String = ""
    var batch : String = ""
    var classes : String = ""
    var document : String = ""
    var status : String = ""
    var reset_password_token : String = ""
    var is_Expiry : String = ""
    var force_change_status : String = ""
    var device_token : String = ""
    var device_type : String = ""
    var profile_update_status : String = ""
    var created : String = ""
    var country_name : String = ""
    var type : String = ""
}