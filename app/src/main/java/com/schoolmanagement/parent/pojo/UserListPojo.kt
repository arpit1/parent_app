package com.schoolmanagement.parent.pojo

import org.json.JSONObject
import java.io.Serializable

/**
 * Created by hitesh.mathur on 3/27/2018.
 */
class UserListPojo : Serializable {
    var id: String? = null
    internal var fname: String? = null
    internal var mname: String? = null
    internal var lname: String? = ""
    internal var profile_pic: String? = null
    internal var isConnected: String? = null
    var login_id: String? = null
    var type: String? = null
    var message_counter : Int = 0
    var chat_size: Int = 0
    var last_msg: JSONObject? = null
}