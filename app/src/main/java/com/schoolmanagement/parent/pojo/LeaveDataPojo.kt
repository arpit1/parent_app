package com.schoolmanagement.parent.pojo

/**
 * Created by arpit.jain on 2/21/2018.
 */
class LeaveDataPojo {
    var id : String  = ""
    var school_id : String  = ""
    var parent_id : String  = ""
    var student_id : String  = ""
    var from_date : String  = ""
    var to_date : String  = ""
    var reason : String  = ""
    var created_at : String  = ""
    var profile_pic : String?  = ""
    var classes : String  = ""
    var student_fname : String  = ""
    var student_mname : String  = ""
    var student_lname : String  = ""
}