package com.schoolmanagement.parent.pojo

import java.io.Serializable

/**
 * Created by upasna.mishra on 2/15/2018.
 */
class HomePostFilePojo : Serializable{

    var id : String = ""
    var file : String = ""
    var thumbnail : String = ""
}