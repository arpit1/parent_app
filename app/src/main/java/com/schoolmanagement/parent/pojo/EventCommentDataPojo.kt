package com.schoolmanagement.parent.pojo

/**
 * Created by upasna.mishra on 2/24/2018.
 */
class EventCommentDataPojo {

    var id : String = ""
    var event_id : String = ""
    var user_id : String = ""
    var description : String = ""
    var created : String = ""
    var father_name : String = ""
    var profile_pic : String? = ""
    var type : String? = ""
}