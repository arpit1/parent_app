package com.schoolmanagement.parent.pojo

/**
 * Created by arpit.jain on 2/20/2018.
 */
class HolidayDataPojo {
    val id : String = ""
    val school_id : String = ""
    val start_date : String = ""
    val to_date : String = ""
    val festival : String = ""
    val created : String = ""
}