package com.schoolmanagement.parent.pojo

/**
 * Created by arpit.jain on 2/28/2018.
 */
class FilesPojo : BasePojo() {
    lateinit var data : ArrayList<NoticeBoardFileDataPojo>
    lateinit var comment : ArrayList<CommentDataPojo>
}