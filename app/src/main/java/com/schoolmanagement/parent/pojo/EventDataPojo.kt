package com.schoolmanagement.parent.pojo

/**
 * Created by upasna.mishra on 2/23/2018.
 */
class EventDataPojo {

    var id : String = ""
    var school_id : String = ""
    var event_name : String = ""
    var start_date : String = ""
    var end_date : String = ""
    var times : String = ""
    var details : String = ""
    var registrationDetails : String = ""
    var image : String? = ""
    var venu : String = ""
    var created_at : String = ""
    var updated_at : String = ""
    var logo : String? = ""
}