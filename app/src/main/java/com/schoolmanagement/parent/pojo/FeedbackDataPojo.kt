package com.schoolmanagement.parent.pojo

import java.io.Serializable

/**
 * Created by upasna.mishra on 2/16/2018.
 */
class FeedbackDataPojo : Serializable{

    var id : String = ""
    var school_id : String = ""
    var parent_id : String = ""
    var teacher_id : String = ""
    var topic : String = ""
    var description : String = ""
    var created : String = ""
    var fname : String = ""
    var mname : String = ""
    var lname : String = ""
    var profile_pic : String? = ""
}