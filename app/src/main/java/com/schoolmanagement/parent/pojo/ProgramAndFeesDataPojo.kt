package com.schoolmanagement.parent.pojo

/**
 * Created by arpit.jain on 2/22/2018.
 */
class ProgramAndFeesDataPojo {
    var currency_code = ""
    var class_name = ""
    var fees = ""
    var programs = ""
    var batch = ""
    var fee_paid = ""
    var mode_of_payment = ""
    var date_of_payment = ""
    var due_date = ""
    var description = ""
}