package com.schoolmanagement.parent.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.schoolmanagement.parent.util.ChatApplication

class ExitService : Service() {
    var id : String = ""
    override fun onBind(intent: Intent?): IBinder {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onTaskRemoved(rootIntent: Intent) {
        super.onTaskRemoved(rootIntent)
        (application as ChatApplication).socket!!.disconnect()
        this.stopSelf()
    }
}