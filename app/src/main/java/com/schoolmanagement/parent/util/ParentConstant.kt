package com.schoolmanagement.parent.util

import android.app.Activity

object ParentConstant {

    const val SERVER_URL: String = "http://192.168.1.234:3007/"
//   const val SERVER_URL: String = "http://in2.endivesoftware.com:3008/"
    @JvmStatic
    val BASE_URL: String = SERVER_URL + "api/parent/"
    @JvmStatic
    val IMAGE_URl: String = SERVER_URL + "portal/uploads/media/"
    @JvmStatic
    val THUMB_IMAGE_URL: String = SERVER_URL + "portal/uploads/media/thumbnail/"
    @JvmStatic
    val SCHOOL_PROFILE_PIC: String = SERVER_URL + "portal/uploads/logo/"
    @JvmStatic
    val IMAGE_PARENT_BASE_URL: String = SERVER_URL + "portal/uploads/parentProfile/"
    @JvmStatic
    val IMAGE_TEACHER_PROFILE: String = SERVER_URL + "portal/uploads/teacherProfile/"
    @JvmStatic
    val ATTACH_IMAGE_URL: String = SERVER_URL + "portal/uploads/notices/"
    @JvmStatic
    val ATTACH_DIARY_IMAGE_URL: String = SERVER_URL + "portal/uploads/diary_files/"
    @JvmStatic
    val EVENT_IMAGE_URL: String = SERVER_URL + "portal/uploads/event/"
    @JvmStatic
    val IMAGE_STUDENT_PROFILE: String = SERVER_URL + "portal/uploads/studentProfile/"
    @JvmStatic
    val POSTIMAGE_BASE_URL: String = SERVER_URL + "portal/uploads/teacherpost/"
    @JvmStatic
    val STUDENT_PROFILE_BASE_URL: String = SERVER_URL + "portal/uploads/student_pic/"
    @JvmStatic
    val PREF_NAME = "com.teacher.prefs"
    @JvmStatic
    val DEVICE_ID = "device_id"
    @JvmStatic
    val IMAGE = "image"
    @JvmStatic
    val EMAIL = "email"
    val USER_ID = "user_id"
    val NAME = "name"
    val FORCE_CHANGE = "force_change"
    val ENHANCED_PROFILE = "enhanced_profile"
    val DEFAULT_VALUE = ""
    val SCHOOL_ID = "school_id"
    val STUDENT_ID = "student_id"
    var LEAVES_REFRESH: Boolean = false
    var PROFILE_REFRESH: Boolean = false
    val PRIMARY_EMAIL = "primary_email"
    var GALLERY_REFRESH: Boolean = false
    var FEEDBACK_REFRESH: Boolean = false
    var TASK_REFRESH: Boolean = false

    @JvmStatic
    var ACTIVITIES = ArrayList<Activity?>()

}
