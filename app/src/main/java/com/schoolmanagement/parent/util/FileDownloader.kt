package com.schoolmanagement.parent.util

import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

/**
 * Created by arpit.jain on 2/23/2018.
 */
class FileDownloader {
    private val MEGABYTE = 1024 * 1024

    fun downloadFile(fileUrl: String, directory: File) {
        try {

            val url = URL(fileUrl)
            val urlConnection = url.openConnection() as HttpURLConnection
            //urlConnection.setRequestMethod("GET");
            //urlConnection.setDoOutput(true);
            urlConnection.connect()

            val inputStream = urlConnection.inputStream
            val fileOutputStream = FileOutputStream(directory)

            val buffer = ByteArray(MEGABYTE)
            var bufferLength = 0
            while ({ bufferLength = inputStream.read(buffer); bufferLength }() >0) {
                fileOutputStream.write(buffer, 0, bufferLength)
            }
            fileOutputStream.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}