package com.teacherapp.util

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R

/**
 * Created by arpit.jain on 1/29/2018.
 */
class ParentDialog(context: Activity) {
    var ctx: Activity = context

    companion object {
        fun showLoading(activity: Activity): ProgressDialog {
            val mProgressDialog = ProgressDialog(activity)
            mProgressDialog.isIndeterminate = true
            mProgressDialog.setMessage(activity.resources.getString(R.string.loading))
            if (!activity.isFinishing && !mProgressDialog.isShowing)
                mProgressDialog.show()
            return mProgressDialog
        }
    }

    fun displayCommonDialog(msg: String) {
        val btn_yes_exit_LL: LinearLayout
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar)
        dialog.setContentView(R.layout.custom_dialog_withheader)

        val msg_textView = dialog.findViewById(R.id.text_exit) as TextView

        msg_textView.text = msg
        btn_yes_exit_LL = dialog.findViewById(R.id.btn_yes_exit_LL) as LinearLayout

        val dialog_header_cross = dialog.findViewById(R.id.dialog_header_cross) as ImageView
        dialog_header_cross.setOnClickListener { dialog.dismiss() }

        btn_yes_exit_LL.setOnClickListener { dialog.dismiss() }

        if (!ctx.isFinishing)
            dialog.show()
    }
}