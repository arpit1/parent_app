package com.schoolmanagement.parent.util

import android.app.Application
import io.socket.client.IO
import io.socket.client.Socket
import java.net.URISyntaxException

/**
 * Created by hitesh.mathur on 3/27/2018.
 */
class ChatApplication : Application() {

    var socket: Socket? = null
        private set

    init {
        try {
            socket = IO.socket(ParentConstant.SERVER_URL)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }

    }
}