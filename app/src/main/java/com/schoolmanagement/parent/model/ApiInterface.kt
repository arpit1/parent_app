package com.teacherapp.util

import com.schoolmanagement.parent.pojo.*
import com.teacherapp.pojo.QuestionPojo
import com.teacherapp.pojo.TemplatePojo
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


/**
 * Created by upasna.mishra on 11/28/2017.
 */
interface ApiInterface {

    @FormUrlEncoded
    @POST("login")
    fun login(@Field("username") username: String, @Field("device_token") device_token: String, @Field("password") password: String,
              @Field("device_type") device_type: String): Call<LoginPojo>

    @GET("getcountries")
    fun country_list(): Call<CountryPojo>

    @FormUrlEncoded
    @POST("forgetPassword")
    fun forgotPassword(@Field("username") username: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("force_changePassword")
    fun forceChangePassword(@Field("user_id") user_id: String, @Field("password") password: String): Call<LoginPojo>

    @FormUrlEncoded
    @POST("feedbackList")
    fun feedbackList(@Field("user_id") user_id: String): Call<FeedbackPojo>

    @FormUrlEncoded
    @POST("teacherList")
    fun teacherList(@Field("user_id") user_id: String, @Field("school_id") school_id: String, @Field("type") type: String): Call<TeacherPojo>

    @FormUrlEncoded
    @POST("addFeedback")
    fun addFeedback(@Field("school_id") school_id: String, @Field("user_id") user_id: String,
                    @Field("teacher_id") teacher_id: String, @Field("topic") topic: String,
                    @Field("description") description: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("getTemplates")
    fun getTamplate(@Field("student_id") student_id: String): Call<TemplatePojo>

    @FormUrlEncoded
    @POST("getPost")
    fun homePost(@Field("school_id") school_id: String, @Field("user_id") user_id: String): Call<HomePostPojo>

    @FormUrlEncoded
    @POST("holidayList")
    fun getHolidays(@Field("school_id") school_id: String): Call<HolidayPojo>

    @FormUrlEncoded
    @POST("get_student_leave")
    fun getLeavesList(@Field("school_id") school_id: String, @Field("user_id") user_id: String, @Field("student_id") student_id: String): Call<LeavePojo>

    @FormUrlEncoded
    @POST("getaboutus")
    fun aboutUs(@Field("school_id") school_id: String): Call<AboutSchoolPojo>

    @FormUrlEncoded
    @POST("deleteFeedbackComment")
    fun deleteFeedbackComment(@Field("id") id: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("deleteComment")
    fun deleteDiaryComment(@Field("comment_id") comment_id: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("addLike")
    fun addLike(@Field("event_id") event_id: String, @Field("user_id") user_id: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("addComment")
    fun addEventComment(@Field("event_id") event_id: String, @Field("user_id") user_id: String,
                        @Field("description") description: String): Call<AddEventCommentPojo>

    @FormUrlEncoded
    @POST("feedbackDetails")
    fun feedbackDetail(@Field("feedback_id") feedback_id: String, @Field("school_id") school_id: String): Call<CommentPojo>

    @FormUrlEncoded
    @POST("addFeedbackComment")
    fun addFeedbackComment(@Field("feedback_id") feedback_id: String,
                           @Field("user_id") user_id: String,
                           @Field("comment") comment: String): Call<AddFeedbackCommentPojo>

    @FormUrlEncoded
    @POST("addDiaryComment")
    fun addDairyComment(@Field("diary_id") diary_id: String,
                           @Field("school_id") school_id: String,
                           @Field("user_id") user_id: String,
                           @Field("comment") comment: String,
                           @Field("comment_id") comment_id: String,
                           @Field("teacher_id") teacher_id: String): Call<AddFeedbackCommentPojo>


    @FormUrlEncoded
    @POST("studentsList")
    fun studentList(@Field("user_id") user_id: String): Call<StudentPojo>

    @FormUrlEncoded
    @POST("parenttaskList")
    fun parenttaskList(@Field("user_id") user_id: String): Call<TeacherTaskPojo>

    @FormUrlEncoded
    @POST("getquestionsanswer")
    fun getQuestionAnswer(@Field("student_id") student_id: String, @Field("template_id") template_id: String): Call<QuestionPojo>

    @FormUrlEncoded
    @POST("editFeedbackComment")
    fun editFeedbackComment(@Field("id") id: String,
                            @Field("comment") comment: String): Call<AddFeedbackCommentPojo>

    @FormUrlEncoded
    @POST("add_student_leave")
    fun addStudentLeave(@Field("school_id") school_id: String, @Field("user_id") user_id: String,
                        @Field("student_id") student_id: String, @Field("from_date") from_date: String,
                        @Field("to_date") to_date: String, @Field("reason") reason: String,
                        @Field("student_name") student_name: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("getNoticeBoard")
    fun getNoticeBoard(@Field("school_id") school_id: String, @Field("program_id") program_id: String, @Field("class_id") class_id: String): Call<NoticeBoardPojo>

    @Streaming
    @GET
    fun downloadFileWithDynamicUrlSync(@Url fileUrl: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("getEvent")
    fun getEvent(@Field("school_id") school_id: String): Call<EventPojo>

    @FormUrlEncoded
    @POST("editDiary")
    fun getSchoolDiaryDetail(@Field("diary_id") diary_id: String,
                             @Field("teacher_id") teacher_id: String, @Field("parent_id") parent_id: String): Call<FilesPojo>

    @FormUrlEncoded
    @POST("taskList")
    fun getTaskList(@Field("user_id") user_id: String): Call<TaskPojo>

    @FormUrlEncoded
    @POST("event_gallery")
    fun getEventGallery(@Field("user_id") user_id: String, @Field("album_name") album_name: String,
                        @Field("category") category: String,@Field("start_date") start_date: String,
                        @Field("end_date") end_date: String): Call<HomePostPojo>

    @FormUrlEncoded
    @POST("updateTask")
    fun updateTask(@Field("user_id") user_id: String, @Field("task_id") task_id: String,
                   @Field("complete_status") complete_status: String,
                   @Field("student_id") student_id: String, @Field("teacher_id") teacher_id: String,
                   @Field("student_name") student_name: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("updateTaskStatus")
    fun teacherTaskUpdate(@Field("id") id: String, @Field("status") status: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("getEventDetails")
    fun getEventDetail(@Field("id") id: String, @Field("user_id") user_id: String): Call<EventDetailPojo>

    @FormUrlEncoded
    @POST("editProfile")
    fun editProfile(@Field("user_id") user_id: String, @Field("father_name") father_name: String,
                    @Field("father_email") father_email: String, @Field("father_phone") father_phone: String,
                    @Field("father_occupation") father_occupation: String, @Field("father_place_of_work") father_place_of_work: String,
                    @Field("mother_name") mother_name: String, @Field("mother_email") mother_email: String,
                    @Field("mother_phone") mother_phone: String, @Field("mother_occupation") mother_occupation: String,
                    @Field("mother_place_of_work") mother_place_of_work: String, @Field("emergency_contact_name") emergency_contact_name: String,
                    @Field("emergency_contact_number") emergency_contact_number: String,
                    @Field("passport_number") passport_number: String, @Field("address_line1") address_line1: String,
                    @Field("address_line2") address_line2: String, @Field("address_line3") address_line3: String,
                    @Field("city") city: String, @Field("state") state: String,
                    @Field("pincode") pincode: String, @Field("country_id") country_id: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("getprofile")
    fun getProfile(@Field("user_id") user_id: String): Call<LoginPojo>

    @FormUrlEncoded
    @POST("logout")
    fun logout(@Field("user_id") user_id: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("addTask")
    fun addTask(@Field("user_id") user_id: String, @Field("title") title: String,
                @Field("description") description: String, @Field("dates") dates: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("getAllNotifications")
    fun getAllNotification(@Field("student_id") student_id: String): Call<NotificationPojo>

    @FormUrlEncoded
    @POST("changePassword")
    fun changePassword(@Field("user_id") user_id: String, @Field("new_password") new_password: String, @Field("old_password") old_password: String): Call<BasePojo>

    @FormUrlEncoded
    @POST("getProgramAndFeesDetail")
    fun getProgramAndFee(@Field("user_id") user_id: String, @Field("school_id") school_id: String): Call<ProgramAndFeePojo>

    @FormUrlEncoded
    @POST("getSchoolDiary")
    fun getSchoolDiary(@Field("user_id") user_id: String, @Field("school_id") school_id: String, @Field("dates") dates: String, @Field("month") month: String, @Field("year") year: String): Call<SchoolDiaryPojo>
}