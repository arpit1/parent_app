package com.schoolmanagement.parent.interfaces

import com.schoolmanagement.parent.pojo.ChatPojo
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface FileUploadSer {

    interface FileUploadService {
        @Multipart
        @POST("api/chat_api/post_image")
        fun sendImage(@Part("sender_id") sender_id: RequestBody, @Part("receiver_id") receiver_id: RequestBody, @Part("type") type: RequestBody, @Part("image_size_width") image_size_width: RequestBody, @Part("image_size_height") image_size_height: RequestBody, @Part("image_thumb") image_thumb: RequestBody, @Part uploadedFile: MultipartBody.Part): Call<ChatPojo>

        @Multipart
        @POST("api/chat_api/post_video")
        fun sendVideo(@Part("sender_id") sender_id: RequestBody, @Part("receiver_id") receiver_id: RequestBody, @Part("type") type: RequestBody, @Part("image_size_width") image_size_width: RequestBody, @Part("image_size_height") image_size_height: RequestBody, @Part("image_thumb") image_thumb: RequestBody,@Part uploadedFile: MultipartBody.Part): Call<ChatPojo>
    }
}
