package com.teacherapp.interfaces

import android.app.Activity
import android.net.Uri
import android.provider.MediaStore

/**
 * Created by hitesh.mathur on 3/26/2018.
 */
object GlobalDataAccess {
    @JvmStatic fun getPath(uri: Uri, mActivity: Activity): String {
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = mActivity.contentResolver.query(uri, null, null, null, null)
        cursor!!.moveToFirst()
        val columnIndex = cursor.getColumnIndex(filePathColumn[0])
        val picturePath = cursor.getString(columnIndex)
        cursor.close()
        return picturePath
    }
}