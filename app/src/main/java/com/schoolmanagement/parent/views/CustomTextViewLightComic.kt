package com.schoolmanagement.parent.views

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.TextView

/**
 * Created by arpit.jain on 2/14/2018.
CustomTextViewLightComic*/
class CustomTextViewLightComic(context: Context, attrs: AttributeSet) : TextView(context, attrs) {

    init {
        val face = Typeface.createFromAsset(context.assets, "font/ROBOTO-REGULAR.TTF")
        this.typeface = face
    }
}
