package com.schoolmanagement.parent.views

import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.schoolmanagement.parent.R

/**
 * Created by arpit.jain on 2/15/2018.
 */
class CircularImageView(context: Context, attrs: AttributeSet?, defStyle: Int) : AppCompatImageView(context, attrs, defStyle) {

    // Border & Selector configuration variables
    private var hasBorder: Boolean = false
    private var hasSelector: Boolean = false
    private var borderWidth: Int = 0
    private val canvasSize: Int = 0
    private var selectorStrokeWidth: Int = 0

    // Objects used for the actual drawing
    private var shader: BitmapShader? = null
    private var image: Bitmap? = null
    private var paint: Paint? = null
    private var paintBorder: Paint? = null
    private var paintSelectorBorder: Paint? = null
    private var selectorFilter: ColorFilter? = null

    fun CircularImageView(context: Context): CircularImageView {
        return CircularImageView(context)
    }

    fun CircularImageView(context: Context, attrs: AttributeSet?): CircularImageView {
        return CircularImageView(context, attrs, R.attr.circularImageViewStyle)
    }

    fun CircularImageView(context: Context, attrs: AttributeSet?, defStyle: Int): CircularImageView {
        init(context, attrs, defStyle)
        return CircularImageView(context, attrs, defStyle)
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyle: Int) {
        // Initialize paint objects
        paint = Paint()
        paint!!.isAntiAlias = true
        paintBorder = Paint()
        paintBorder!!.isAntiAlias = true
        paintSelectorBorder = Paint()
        paintSelectorBorder!!.isAntiAlias = true

        // load the styled attributes and set their properties
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.CircularImageView, defStyle, 0)

        // Check if border and/or border is enabled
        hasBorder = attributes.getBoolean(R.styleable.CircularImageView_border, false)
        hasSelector = attributes.getBoolean(R.styleable.CircularImageView_selector, false)

        // Set border properties if enabled
        if (hasBorder) {
            val defaultBorderSize = (2 * context.resources.displayMetrics.density + 0.5f).toInt()
            setBorderWidth(attributes.getDimensionPixelOffset(R.styleable.CircularImageView_border_width, defaultBorderSize))
            setBorderColor(attributes.getColor(R.styleable.CircularImageView_border_color, Color.WHITE))
        }

        // Set selector properties if enabled
        if (hasSelector) {
            val defaultSelectorSize = (2 * context.resources.displayMetrics.density + 0.5f).toInt()
            setSelectorColor(attributes.getColor(R.styleable.CircularImageView_selector_color, Color.TRANSPARENT))
            setSelectorStrokeWidth(attributes.getDimensionPixelOffset(R.styleable.CircularImageView_selector_stroke_width, defaultSelectorSize))
            setSelectorStrokeColor(attributes.getColor(R.styleable.CircularImageView_selector_stroke_color, Color.BLUE))
        }

        // Add shadow if enabled
        if (attributes.getBoolean(R.styleable.CircularImageView_shadow, false))
            addShadow()

        // We no longer need our attributes TypedArray, give it back to cache
        attributes.recycle()
    }

    /**
     * Sets the CircularImageView's border width in pixels.
     *
     * @param borderWidth
     */
    fun setBorderWidth(borderWidth: Int) {
        this.borderWidth = borderWidth
        this.requestLayout()
        this.invalidate()
    }

    /**
     * Sets the CircularImageView's basic border color.
     *
     * @param borderColor
     */
    fun setBorderColor(borderColor: Int) {
        if (paintBorder != null)
            paintBorder!!.setColor(borderColor)
        this.invalidate()
    }

    /**
     * Sets the color of the selector to be draw over the
     * CircularImageView. Be sure to provide some opacity.
     *
     * @param selectorColor
     */
    fun setSelectorColor(selectorColor: Int) {
        this.selectorFilter = PorterDuffColorFilter(selectorColor, PorterDuff.Mode.SRC_ATOP)
        this.invalidate()
    }

    /**
     * Sets the stroke width to be drawn around the CircularImageView
     * during click events when the selector is enabled.
     *
     * @param selectorStrokeWidth
     */
    fun setSelectorStrokeWidth(selectorStrokeWidth: Int) {
        this.selectorStrokeWidth = selectorStrokeWidth
        this.requestLayout()
        this.invalidate()
    }

    /**
     * Sets the stroke color to be drawn around the CircularImageView
     * during click events when the selector is enabled.
     */
    fun setSelectorStrokeColor(selectorStrokeColor: Int) {
        if (paintSelectorBorder != null)
            paintSelectorBorder!!.setColor(selectorStrokeColor)
        this.invalidate()
    }

    /**
     * Adds a dark shadow to this CircularImageView.
     */
    fun addShadow() {
        setLayerType(View.LAYER_TYPE_SOFTWARE, paintBorder)
        paintBorder!!.setShadowLayer(4.0f, 0.0f, 2.0f, Color.BLACK)
    }


    public override fun onDraw(canvas: Canvas) {
        val drawable = drawable ?: return

        if (width == 0 || height == 0) {
            return
        }
        val b = (drawable as BitmapDrawable).bitmap
        val bitmap = b.copy(Bitmap.Config.ARGB_8888, true)

        val w = width
        val h = height
        val radius = if (w < h) w else h

        val roundBitmap = getCroppedBitmap(bitmap, radius, w, h)
        // roundBitmap= ImageUtils.setCircularInnerGlow(roundBitmap, 0xFFBAB399,
        // 4, 1);
        canvas.drawBitmap(roundBitmap, 0f, 0f, null)
        //		// Don't draw anything without an image
        //		if(image == null)
        //			return;
        //
        //		// Nothing to draw (Empty bounds)
        //		if(image.getHeight() == 0 || image.getWidth() == 0)
        //			return;
        //
        //		// Compare canvas sizes
        //		int oldCanvasSize = canvasSize;
        //
        //		canvasSize = canvas.getWidth();
        //		if(canvas.getHeight() < canvasSize)
        //			canvasSize = canvas.getHeight();
        //
        //		// Reinitialize shader, if necessary
        //		if(oldCanvasSize != canvasSize)
        //			refreshBitmapShader();
        //
        //		// Apply shader to paint
        //		paint.setShader(shader);
        //
        //		// Keep track of selectorStroke/border width
        //		int outerWidth = 0;
        //
        //		// Get the exact X/Y axis of the view
        //		int center = canvasSize / 2;
        //
        //
        //		if(hasSelector && isSelected) { // Draw the selector stroke & apply the selector filter, if applicable
        //			outerWidth = selectorStrokeWidth;
        //			center = (canvasSize - (outerWidth * 2)) / 2;
        //
        //			paint.setColorFilter(selectorFilter);
        //			canvas.drawCircle(center + outerWidth, center + outerWidth, ((canvasSize - (outerWidth * 2)) / 2) + outerWidth - 4.0f, paintSelectorBorder);
        //		}
        //		else if(hasBorder) { // If no selector was drawn, draw a border and clear the filter instead... if enabled
        //			outerWidth = borderWidth;
        //			center = (canvasSize - (outerWidth * 2)) / 2;
        //
        //			paint.setColorFilter(null);
        //			canvas.drawCircle(center + outerWidth, center + outerWidth, ((canvasSize - (outerWidth * 2)) / 2) + outerWidth - 4.0f, paintBorder);
        //		}
        //		else // Clear the color filter if no selector nor border were drawn
        //			paint.setColorFilter(null);
        //
        //		// Draw the circular image itself
        //		canvas.drawCircle(center + outerWidth, center + outerWidth, ((canvasSize - (outerWidth * 2)) / 2) - 4.0f, paint);
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        // Check for clickable state and do nothing if disabled
        if (!this.isClickable) {
            this.isSelected = false
            return super.onTouchEvent(event)
        }

        // Set selected state based on Motion Event
        when (event.action) {
            MotionEvent.ACTION_DOWN -> this.isSelected = true
            MotionEvent.ACTION_UP, MotionEvent.ACTION_SCROLL, MotionEvent.ACTION_OUTSIDE, MotionEvent.ACTION_CANCEL -> this.isSelected = false
        }

        // Redraw image and return super type
        this.invalidate()
        return super.dispatchTouchEvent(event)
    }

    override fun invalidate(dirty: Rect) {
        super.invalidate(dirty)
        image = drawableToBitmap(drawable)
        if (shader != null || canvasSize > 0)
            refreshBitmapShader()
    }

    override fun invalidate(l: Int, t: Int, r: Int, b: Int) {
        super.invalidate(l, t, r, b)
        image = drawableToBitmap(drawable)
        if (shader != null || canvasSize > 0)
            refreshBitmapShader()
    }

    override fun invalidate() {
        super.invalidate()
        image = drawableToBitmap(drawable)
        if (shader != null || canvasSize > 0)
            refreshBitmapShader()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = measureWidth(widthMeasureSpec)
        val height = measureHeight(heightMeasureSpec)
        setMeasuredDimension(width, height)
    }

    private fun measureWidth(measureSpec: Int): Int {
        val result : Int
        val specMode = View.MeasureSpec.getMode(measureSpec)
        val specSize = View.MeasureSpec.getSize(measureSpec)

        result = when (specMode) {
            View.MeasureSpec.EXACTLY -> // The parent has determined an exact size for the child.
                specSize
            View.MeasureSpec.AT_MOST -> // The child can be as large as it wants up to the specified size.
                specSize
            else -> // The parent has not imposed any constraint on the child.
                canvasSize
        }

        return result
    }

    private fun measureHeight(measureSpecHeight: Int): Int {
        val result : Int
        val specMode = View.MeasureSpec.getMode(measureSpecHeight)
        val specSize = View.MeasureSpec.getSize(measureSpecHeight)

        result = when (specMode) {
            View.MeasureSpec.EXACTLY -> // We were told how big to be
                specSize
            View.MeasureSpec.AT_MOST -> // The child can be as large as it wants up to the specified size.
                specSize
            else -> // Measure the text (beware: ascent is a negative number)
                canvasSize
        }

        return result + 2
    }

    /**
     * Convert a drawable object into a Bitmap
     *
     * @param drawable
     * @return
     */
    fun drawableToBitmap(drawable: Drawable?): Bitmap? {
        if (drawable == null) { // Don't do anything without a proper drawable
            return null
        } else if (drawable is BitmapDrawable) { // Use the getBitmap() method instead if BitmapDrawable
            return drawable.bitmap
        }

        // Create Bitmap object out of the drawable
        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }

    /**
     * Reinitializes the shader texture used to fill in
     * the Circle upon drawing.
     */
    fun refreshBitmapShader() {
        shader = BitmapShader(Bitmap.createScaledBitmap(image, canvasSize, canvasSize, false), Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
    }

    /**
     * Returns whether or not this view is currently
     * in its selected state.
     */
    override fun isSelected(): Boolean {
        return this.isSelected
    }

    fun getCroppedBitmap(bmp: Bitmap, radius: Int, w: Int, h: Int): Bitmap {
        val sbmp: Bitmap
        if (bmp.width != radius || bmp.height != radius) {
            val _w_rate = 1.0f * radius / bmp.width
            val _h_rate = 1.0f * radius / bmp.height
            val _rate = if (_w_rate < _h_rate) _h_rate else _w_rate
            sbmp = Bitmap.createScaledBitmap(bmp, (bmp.width * _rate).toInt(), (bmp.height * _rate).toInt(), false)
        } else
            sbmp = bmp

        val output = Bitmap.createBitmap(sbmp.width, sbmp.height,
                Bitmap.Config.ARGB_8888)
        val canvas = Canvas(output)

        val paint = Paint()
        val rect = Rect(0, 0, sbmp.width, sbmp.height)

        paint.isAntiAlias = true
        paint.isFilterBitmap = true
        paint.isDither = true
        canvas.drawARGB(0, 0, 0, 0)
        paint.color = Color.parseColor("#BAB399")
        canvas.drawCircle((w / 2).toFloat(), (h / 2).toFloat(), ((if (w < h) w else h) / 2).toFloat(), paint)
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(sbmp, rect, rect, paint)

        return output
    }
}