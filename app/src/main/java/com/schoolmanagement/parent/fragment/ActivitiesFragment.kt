package com.schoolmanagement.parent.fragment

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.adapter.GalleryAdapter
import com.schoolmanagement.parent.pojo.FilterPojo
import com.schoolmanagement.parent.pojo.HomePostDataPojo
import com.schoolmanagement.parent.pojo.HomePostPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivitiesFragment : Fragment() {

    private lateinit var activitiesView: View
    private lateinit var gridActivities: GridView
    private lateinit var noDataAvailable: TextView
    private lateinit var clear_filter: TextView
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private var galleryList: ArrayList<HomePostDataPojo>? = null
    private var searchView: android.support.v7.widget.SearchView? = null
    private lateinit var ctx: HomeActivity
    private var dialog: ParentDialog? = null
    private lateinit var adapter: GalleryAdapter
    private var categoryName: String = "Activities"
    private var galleryDataFilter: ArrayList<HomePostDataPojo>? = null
    private lateinit var filterPojo: FilterPojo

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activitiesView = inflater!!.inflate(R.layout.fragment_activities, container, false)
        ctx = activity as HomeActivity
        initialize()
        setListener()
        return activitiesView
    }

    private fun initialize() {
        galleryList = ArrayList()
        dialog = ParentDialog(ctx)
        galleryDataFilter = ArrayList()
        gridActivities = activitiesView.findViewById(R.id.gridActivities)
        noDataAvailable = activitiesView.findViewById(R.id.noDataAvailable)
        searchView = activitiesView.findViewById(R.id.search_box)
        clear_filter = activitiesView.findViewById(R.id.clear_filter)
        if (!fragmentResume && fragmentVisible && galleryList?.size == 0) {   //only when first time fragment is created
            getEvent("",  categoryName, "", "", "")
        }
    }

    private fun setListener() {
        searchView!!.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                //Log.e("onQueryTextChange", "called");
                val gallery: ArrayList<HomePostDataPojo> = galleryList!!
                galleryDataFilter!!.clear()
                if (newText.isNotEmpty()) {
                    (0 until gallery.size)
                            .filter {
                                gallery[it].event_name.toLowerCase().contains(newText.toLowerCase()) ||
                                        gallery[it].discussion.toLowerCase().contains(newText.toLowerCase())
                            }
                            .forEach { galleryDataFilter!!.add(gallery[it]) }
                } else {
                    galleryDataFilter!!.addAll(gallery)
                }
                setAdapter(galleryDataFilter!!)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // Do your task here
                ctx.hideSoftKeyboard()
                return false
            }
        })

        clear_filter.setOnClickListener {
            clear_filter.visibility = View.GONE
            filterPojo = FilterPojo()
            ctx.saveFilterData(filterPojo)
            getEvent("", categoryName, "clear", "", "")
        }
    }

    override fun onResume() {
        super.onResume()
        val filterpojo = ctx.getFilterData()
        if (ParentConstant.GALLERY_REFRESH) {
            if (filterpojo!!.category == "Activities") {
                clear_filter.visibility = View.VISIBLE
                getEvent(filterpojo.album_name, categoryName,"", filterpojo.start_date, filterpojo.end_date)
            } else {
                clear_filter.visibility = View.GONE
                getEvent("",  categoryName, "", "", "")
            }
            searchView?.setQuery("", true)
        }
        if (searchView != null) {
            searchView!!.isIconified = false
            searchView!!.clearFocus()
        }
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && galleryList != null && galleryList?.size == 0) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            val handler = Handler()
            handler.postDelayed({
                getEvent("",  categoryName, "", "", "")
            }, 150)
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
            if (searchView != null)
                searchView!!.clearFocus()
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
            if (searchView != null)
                searchView!!.clearFocus()
        }
    }

    private fun getEvent(album_name: String, categoryName: String, from : String, startDate : String, endDate : String) {
        println("from value is  "+from)
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<HomePostPojo> = apiService.getEventGallery(ctx.getFromPrefs(ParentConstant.USER_ID).toString(),
                    album_name, categoryName, startDate, endDate)
            call.enqueue(object : Callback<HomePostPojo> {
                override fun onResponse(call: Call<HomePostPojo>, response: Response<HomePostPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            if(from == "clear")
                                clear_filter.visibility = View.GONE
                            ParentConstant.GALLERY_REFRESH = false
                            if (response.body()!!.data.size > 0) {
                                galleryList = ArrayList()
                                gridActivities.visibility = View.VISIBLE
                                noDataAvailable.visibility = View.GONE
                                galleryList?.addAll(response.body()!!.data)
                                setAdapter(galleryList!!)
                            } else {
                                gridActivities.visibility = View.GONE
                                noDataAvailable.visibility = View.VISIBLE
                            }
                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<HomePostPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setAdapter(eventList: ArrayList<HomePostDataPojo>) {
        adapter = GalleryAdapter(ctx, eventList)
        gridActivities.adapter = adapter
    }
}