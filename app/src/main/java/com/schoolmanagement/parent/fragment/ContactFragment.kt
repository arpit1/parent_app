package com.schoolmanagement.parent.fragment

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.adapter.ContactAdapter
import com.schoolmanagement.parent.pojo.TeacherDataPojo
import com.schoolmanagement.parent.pojo.TeacherPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactFragment : Fragment() {

    private lateinit var viewContactFragment: View
    private lateinit var ctx: HomeActivity
    private lateinit var rvContactList: RecyclerView
    private lateinit var noDataAvailable: TextView
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private var contactList: ArrayList<TeacherDataPojo>? = null
    private lateinit var mLayoutManager: LinearLayoutManager
    private var dialog: ParentDialog? = null
    private var searchView: android.support.v7.widget.SearchView? = null
    private var contactDataFilter: ArrayList<TeacherDataPojo>? = null
    private lateinit var adapter: ContactAdapter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewContactFragment = inflater!!.inflate(R.layout.fragment_contact, container, false)
        ctx = activity as HomeActivity
        initialize()
        setListener()
        return viewContactFragment
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        mLayoutManager = LinearLayoutManager(ctx)
        contactList = ArrayList()
        contactDataFilter = ArrayList()
        searchView = viewContactFragment.findViewById(R.id.search_box)
        rvContactList = viewContactFragment.findViewById(R.id.rvContactList)
        noDataAvailable = viewContactFragment.findViewById(R.id.noDataAvailable)
        rvContactList.layoutManager = mLayoutManager

        if (!fragmentResume && fragmentVisible && contactList != null && contactList?.size == 0) {   //only when first time fragment is created
            getTeacherList()
        }
    }

    private fun setListener() {
        searchView!!.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                //Log.e("onQueryTextChange", "called");
                val task: ArrayList<TeacherDataPojo> = contactList!!
                contactDataFilter!!.clear()
                if (newText.isNotEmpty()) {
                    (0 until task.size)
                            .filter { getTeacherName(task[it]).toLowerCase().contains(newText.toLowerCase()) }
                            .forEach { contactDataFilter!!.add(task[it]) }
                } else {
                    contactDataFilter!!.addAll(task)
                }
                setAdapter(contactDataFilter!!)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // Do your task here
                ctx.hideSoftKeyboard()
                return false
            }
        })
    }

    private fun getTeacherName(teacherData: TeacherDataPojo): String {
        return if (teacherData.mname != "")
            teacherData.fname + " " + teacherData.mname + " " + teacherData.lname
        else
            teacherData.fname + " " + teacherData.lname
    }

    override fun onResume() {
        super.onResume()
        if (searchView != null) {
            searchView!!.isIconified = false
            searchView!!.clearFocus()
        }
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && contactList != null && contactList?.size == 0) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            val handler = Handler()
            handler.postDelayed({
                getTeacherList()
            }, 150)
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
            if (searchView != null)
                searchView!!.clearFocus()
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
            if (searchView != null)
                searchView!!.clearFocus()
        }
    }

    /*Type = 1 to get the Admin
            * Type = 0 to get normal list without Admin
            * */
    private fun getTeacherList() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<TeacherPojo> = apiService.teacherList(ctx.getFromPrefs(ParentConstant.USER_ID).toString(),
                    ctx.getFromPrefs(ParentConstant.SCHOOL_ID).toString(), "1")
            call.enqueue(object : Callback<TeacherPojo> {
                override fun onResponse(call: Call<TeacherPojo>, response: Response<TeacherPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            contactList = ArrayList()
                            ParentConstant.LEAVES_REFRESH = false
                            if (response.body()!!.data.size > 0) {
                                rvContactList.visibility = View.VISIBLE
                                noDataAvailable.visibility = View.GONE
                                contactList!!.addAll(response.body()!!.data)
                                setAdapter(contactList!!)
                            } else {
                                rvContactList.visibility = View.GONE
                                noDataAvailable.visibility = View.VISIBLE
                            }
                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<TeacherPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setAdapter(teacherList: ArrayList<TeacherDataPojo>) {
        adapter = ContactAdapter(ctx, teacherList)
        rvContactList.adapter = adapter
    }
}