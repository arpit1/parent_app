package com.schoolmanagement.parent.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.adapter.HomePostAdapter
import com.schoolmanagement.parent.pojo.HomePostDataPojo
import com.schoolmanagement.parent.pojo.HomePostPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by upasna.mishra on 2/16/2018.
 */
class HomeFragment : Fragment(){

    private lateinit var viewHomeFragment: View
    private var dialog: ParentDialog? = null
    private lateinit var noDataAvailable: TextView
    private lateinit var ctx: HomeActivity
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private lateinit var homePost: RecyclerView
    private lateinit var adapter: HomePostAdapter
    private var home_post_list: ArrayList<HomePostDataPojo>? = null
    private lateinit var mLayoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewHomeFragment = inflater!!.inflate(R.layout.fragment_home, container, false)
        ctx = activity as HomeActivity
        initialize()
        setListener()
        return viewHomeFragment
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        mLayoutManager = LinearLayoutManager(ctx)
        home_post_list = ArrayList()

        homePost = viewHomeFragment.findViewById(R.id.homePost)
        noDataAvailable = viewHomeFragment.findViewById(R.id.noDataAvailable)
        homePost.layoutManager = mLayoutManager

        if (!fragmentResume && fragmentVisible) {   //only when first time fragment is created
            homePost()
        }
    }

    private fun setListener() {

    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && home_post_list != null && home_post_list?.size == 0) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            homePost()
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
        }
    }

    private fun homePost() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<HomePostPojo> = apiService.homePost(ctx.getFromPrefs(ParentConstant.SCHOOL_ID).toString(),
                    ctx.getFromPrefs(ParentConstant.USER_ID).toString())
            call.enqueue(object : Callback<HomePostPojo> {
                override fun onResponse(call: Call<HomePostPojo>, response: Response<HomePostPojo>?) {
                    if (response != null) {
                        if (response.body()!!.data.size > 0) {
                            homePost.visibility = View.VISIBLE
                            noDataAvailable.visibility = View.GONE
                            if (response.body()!!.status == "1") {
                                home_post_list!!.addAll(response.body()!!.data)
                                adapter = HomePostAdapter(ctx, home_post_list)
                                homePost.adapter = adapter
                            } else {
                                ctx.displayToast(response.body()!!.message)
                            }
                        } else {
                            homePost.visibility = View.GONE
                            noDataAvailable.visibility = View.VISIBLE
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<HomePostPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

}