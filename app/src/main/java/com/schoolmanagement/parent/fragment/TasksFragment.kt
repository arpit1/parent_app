package com.schoolmanagement.parent.fragment

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.adapter.TaskAdapter
import com.schoolmanagement.parent.pojo.BasePojo
import com.schoolmanagement.parent.pojo.StudentDetailsPojo
import com.schoolmanagement.parent.pojo.TaskDataPojo
import com.schoolmanagement.parent.pojo.TaskPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by arpit.jain on 2/14/2018.
 */
class TasksFragment : Fragment() {
    private lateinit var viewTasksFragment: View
    private var dialog: ParentDialog? = null
    private lateinit var noDataAvailable: TextView
    private lateinit var ctx: HomeActivity
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private var taskList: ArrayList<TaskDataPojo>? = null
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var rvTaskList: RecyclerView
    private lateinit var adapter: TaskAdapter
    private var searchView: android.support.v7.widget.SearchView? = null
    private var taskDataFilter: ArrayList<TaskDataPojo>? = null
    private lateinit var studentDetailPojo: StudentDetailsPojo

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewTasksFragment = inflater!!.inflate(R.layout.fragment_tasks, container, false)
        ctx = activity as HomeActivity
        initialize()
        setListener()
        return viewTasksFragment
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        studentDetailPojo = ctx.getStudentData()!!
        mLayoutManager = LinearLayoutManager(ctx)
        taskList = ArrayList()
        taskDataFilter = ArrayList()
        searchView = viewTasksFragment.findViewById(R.id.search_box)
        noDataAvailable = viewTasksFragment.findViewById(R.id.noDataAvailable)
        rvTaskList = viewTasksFragment.findViewById(R.id.rvTaskList)
        rvTaskList.layoutManager = mLayoutManager

        if (!fragmentResume && fragmentVisible && taskList != null && taskList?.size == 0) {   //only when first time fragment is created
            getTask()
        }
    }

    private fun setListener() {
        searchView!!.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                //Log.e("onQueryTextChange", "called");
                val task: ArrayList<TaskDataPojo> = taskList!!
                taskDataFilter!!.clear()
                if (newText.isNotEmpty()) {
                    (0 until task.size)
                            .filter { task[it].title.toLowerCase().contains(newText.toLowerCase()) || task[it].description.toLowerCase().contains(newText.toLowerCase()) }
                            .forEach { taskDataFilter!!.add(task[it]) }
                } else {
                    taskDataFilter!!.addAll(task)
                }
                setAdapter(taskDataFilter!!)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // Do your task here
                ctx.hideSoftKeyboard()
                return false
            }
        })
    }

    override fun onResume() {
        super.onResume()
        if (searchView != null) {
            searchView!!.isIconified = false
            searchView!!.clearFocus()
        }
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && taskList != null && taskList?.size == 0) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            val handler = Handler()
            handler.postDelayed({
                getTask()
            }, 150)
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
            if (searchView != null)
                searchView!!.clearFocus()
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
            searchView!!.clearFocus()
        }
    }

    private fun getTask() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<TaskPojo> = apiService.getTaskList(ctx.getFromPrefs(ParentConstant.USER_ID).toString())
            call.enqueue(object : Callback<TaskPojo> {
                override fun onResponse(call: Call<TaskPojo>, response: Response<TaskPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            ParentConstant.LEAVES_REFRESH = false
                            if (response.body()!!.data.size > 0) {
                                rvTaskList.visibility = View.VISIBLE
                                noDataAvailable.visibility = View.GONE
                                taskList!!.addAll(response.body()!!.data)
                                setAdapter(taskList!!)
                            } else {
                                rvTaskList.visibility = View.GONE
                                noDataAvailable.visibility = View.VISIBLE
                            }
                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<TaskPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setAdapter(taskList: ArrayList<TaskDataPojo>) {
        adapter = TaskAdapter(ctx, taskList, this)
        rvTaskList.adapter = adapter
    }

    fun updateTask(task_ctx: HomeActivity, taskId: String, completeStatus: String, teacher_id: String) {
        if (ConnectionDetector.isConnected(task_ctx)) {
            val d = ParentDialog.showLoading(task_ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<BasePojo> = apiService.updateTask(task_ctx.getFromPrefs(ParentConstant.USER_ID).toString(),
                    taskId, completeStatus, studentDetailPojo.id, teacher_id, ctx.getStudentName())
            call.enqueue(object : Callback<BasePojo> {
                override fun onResponse(call: Call<BasePojo>, response: Response<BasePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            ctx.displayToast(response.body()!!.message)
                        }
                        for (i in 0 until taskList!!.size) {
                            if (taskId == taskList?.get(i)!!.id) {
                                taskList!![i].complete_status = completeStatus
                                break
                            }
                        }
                        if (searchView?.query != "") {
                            for (i in 0 until taskDataFilter!!.size) {
                                if (taskId == taskDataFilter?.get(i)!!.id) {
                                    taskDataFilter!![i].complete_status = completeStatus
                                    break
                                }
                            }
                        }
                        adapter.notifyDataSetChanged()
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<BasePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog?.displayCommonDialog(task_ctx.resources.getString(R.string.no_internet_connection))
        }
    }
}