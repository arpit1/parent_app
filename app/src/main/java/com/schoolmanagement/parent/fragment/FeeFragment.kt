package com.schoolmanagement.parent.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.ProfileActivity
import com.schoolmanagement.parent.pojo.ProgramAndFeePojo
import com.schoolmanagement.parent.pojo.ProgramAndFeesDataPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


/**
 * Created by arpit.jain on 2/14/2018.
 */
class FeeFragment : Fragment() {

    private lateinit var viewFeeFragment: View
    private var dialog: ParentDialog? = null
    private lateinit var ctx: ProfileActivity
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private lateinit var programName: TextView
    private lateinit var period: TextView
    private lateinit var batchName: TextView
    private lateinit var programFee: TextView
    private lateinit var feePaid: TextView
    private lateinit var paymentMode: TextView
    private lateinit var paymentDate: TextView
    private lateinit var feeDue: TextView
    private lateinit var dueDate: TextView
    private lateinit var descriptionFee: TextView
    private var programFeeData: ProgramAndFeesDataPojo? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewFeeFragment = inflater!!.inflate(R.layout.fragment_fee, container, false)
        initialize()
        return viewFeeFragment
    }

    private fun initialize() {
        ctx = activity as ProfileActivity
        dialog = ParentDialog(ctx)
        programName = viewFeeFragment.findViewById(R.id.programName)
        period = viewFeeFragment.findViewById(R.id.period)
        batchName = viewFeeFragment.findViewById(R.id.batchName)
        programFee = viewFeeFragment.findViewById(R.id.programFee)
        feePaid = viewFeeFragment.findViewById(R.id.feePaid)
        paymentMode = viewFeeFragment.findViewById(R.id.paymentMode)
        paymentDate = viewFeeFragment.findViewById(R.id.paymentDate)
        feeDue = viewFeeFragment.findViewById(R.id.feeDue)
        dueDate = viewFeeFragment.findViewById(R.id.dueDate)
        descriptionFee = viewFeeFragment.findViewById(R.id.descriptionFee)
        if (!fragmentResume && fragmentVisible) {   //only when first time fragment is created
            getProgramAndFee()
        }
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && programFeeData == null) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            getProgramAndFee()
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
        }
    }

    private fun getProgramAndFee() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<ProgramAndFeePojo> = apiService.getProgramAndFee(ctx.getFromPrefs(ParentConstant.USER_ID).toString(), ctx.getFromPrefs(ParentConstant.SCHOOL_ID).toString())
            call.enqueue(object : Callback<ProgramAndFeePojo> {
                override fun onResponse(call: Call<ProgramAndFeePojo>, response: Response<ProgramAndFeePojo>?) {
                    if (response != null) {
                        if (response.body()?.status == "1" && response.body()?.data != null) {
                            programFeeData = response.body()!!.data
                            val currency = Utils.getCurrencySymbol(programFeeData?.currency_code!!)
//                            val currency = Utils.getCurrencySymbol("USD")
                            programName.text = programFeeData?.programs
                            batchName.text = programFeeData?.batch
                            period.text = programFeeData?.class_name
                            programFee.text = currency + " " + programFeeData?.fees
                            feePaid.text = currency + " " + programFeeData?.fee_paid
                            paymentMode.text = programFeeData?.mode_of_payment
                            paymentDate.text = programFeeData?.date_of_payment
                            feeDue.text = currency + " " + (programFeeData?.fees!!.toFloat() - programFeeData?.fee_paid!!.toFloat()).toString()
                            dueDate.text = programFeeData?.due_date
                            descriptionFee.text = programFeeData?.description
                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<ProgramAndFeePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    internal object Utils {
        private var currencyLocaleMap: SortedMap<Currency, Locale>

        init {
            currencyLocaleMap = TreeMap<Currency, Locale>(Comparator<Currency> { c1, c2 -> c1.currencyCode.compareTo(c2.currencyCode) })
            for (locale in Locale.getAvailableLocales()) {
                try {
                    val currency = Currency.getInstance(locale)
                    currencyLocaleMap[currency] = locale
                } catch (e: Exception) {
                }

            }
        }

        fun getCurrencySymbol(currencyCode: String): String {
            val currency = Currency.getInstance(currencyCode)
            return currency.getSymbol(currencyLocaleMap[currency])
        }
    }
}