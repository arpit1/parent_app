package com.schoolmanagement.parent.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.AddLeaveActivity
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.adapter.CalendarPagerAdapter
import com.teacherapp.util.ParentDialog

/**
 * Created by arpit.jain on 2/16/2018.
 */
class CalendarFragment : Fragment(), View.OnClickListener {
    private lateinit var viewCalendarFragment: View
    private var dialog: ParentDialog? = null
    private lateinit var noDataAvailable: TextView
    private lateinit var ctx: HomeActivity
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private var calendarViewPager: ViewPager? = null
    private var mAdapter: CalendarPagerAdapter? = null
    private lateinit var tasksText: TextView
    private lateinit var eventsText: TextView
    private lateinit var holidaysText: TextView
    private lateinit var leavesText: TextView

    private lateinit var tasksLayoutSelector: LinearLayout
    private lateinit var eventsLayoutSelector: LinearLayout
    private lateinit var holidaysLayoutSelector: LinearLayout
    private lateinit var leavesLayoutSelector: LinearLayout

    lateinit var tasksLayout: LinearLayout
    lateinit var eventsLayout: LinearLayout
    private lateinit var holidaysLayout: LinearLayout
    lateinit var leavesLayout: LinearLayout

    private lateinit var iv_add_icon: ImageView

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewCalendarFragment = inflater!!.inflate(R.layout.fragment_calendar, container, false)
        ctx = activity as HomeActivity
        initialize()
        setListener()
        return viewCalendarFragment
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        calendarViewPager = viewCalendarFragment.findViewById(R.id.calendar_view_pager)
        mAdapter = CalendarPagerAdapter(childFragmentManager)

        tasksText = viewCalendarFragment.findViewById(R.id.tasks_text)
        eventsText = viewCalendarFragment.findViewById(R.id.events_text)
        holidaysText = viewCalendarFragment.findViewById(R.id.holidays_text)
        leavesText = viewCalendarFragment.findViewById(R.id.leaves_text)

        tasksLayoutSelector = viewCalendarFragment.findViewById(R.id.tasks_selector)
        eventsLayoutSelector = viewCalendarFragment.findViewById(R.id.events_selector)
        holidaysLayoutSelector = viewCalendarFragment.findViewById(R.id.holidays_selector)
        leavesLayoutSelector = viewCalendarFragment.findViewById(R.id.leaves_selector)

        tasksLayout = viewCalendarFragment.findViewById(R.id.tasks_layout)
        eventsLayout = viewCalendarFragment.findViewById(R.id.events_layout)
        holidaysLayout = viewCalendarFragment.findViewById(R.id.holidays_layout)
        leavesLayout = viewCalendarFragment.findViewById(R.id.leaves_layout)

        iv_add_icon = ctx.findViewById(R.id.iv_add_icon)

        tasksLayout.setOnClickListener(this)
        eventsLayout.setOnClickListener(this)
        holidaysLayout.setOnClickListener(this)
        leavesLayout.setOnClickListener(this)
    }

    private fun setListener() {
        calendarViewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                setStyle(position)
            }

            override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

            override fun onPageScrollStateChanged(arg0: Int) {}
        })

        iv_add_icon.setOnClickListener {
            startActivity(Intent(ctx, AddLeaveActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
//        if (mAdapter == null) {
//            mAdapter = CalendarPagerAdapter(childFragmentManager)
//        }
//        if (calendarViewPager?.adapter == null) {
//            calendarViewPager?.adapter = mAdapter
//        }
//
//        val handler = Handler()
//        handler.postDelayed({
//            calendarViewPager?.offscreenPageLimit = 3
//        }, 250)
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && calendarViewPager?.adapter == null) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            if (calendarViewPager?.adapter == null) {
                calendarViewPager?.adapter = mAdapter
                calendarViewPager?.offscreenPageLimit = 3
            }
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
            if (calendarViewPager != null && calendarViewPager!!.currentItem == 3)
                iv_add_icon.visibility = View.VISIBLE
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
            iv_add_icon.visibility = View.GONE
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tasks_layout -> if (calendarViewPager?.currentItem != 0) {
                calendarViewPager?.currentItem = 0
                setStyle(0)
            }

            R.id.events_layout -> if (calendarViewPager?.currentItem != 1) {
                calendarViewPager?.currentItem = 1
                setStyle(1)
            }

            R.id.holidays_layout -> if (calendarViewPager?.currentItem != 2) {
                calendarViewPager?.currentItem = 2
                setStyle(2)
            }

            R.id.leaves_layout -> if (calendarViewPager?.currentItem != 3) {
                calendarViewPager?.currentItem = 3
                setStyle(3)
            }
            else -> {
            }
        }
    }

    fun setStyle(position: Int) {
        when (position) {
            0 -> {
                iv_add_icon.visibility = View.GONE
                tasksText.setTextColor(ContextCompat.getColor(ctx, R.color.header_color))
                eventsText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
                holidaysText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
                leavesText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))

                tasksLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.header_color))
                eventsLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
                holidaysLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
                leavesLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
            }
            1 -> {
                iv_add_icon.visibility = View.GONE
                tasksText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
                eventsText.setTextColor(ContextCompat.getColor(ctx, R.color.header_color))
                holidaysText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
                leavesText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))

                tasksLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
                eventsLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.header_color))
                holidaysLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
                leavesLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
            }
            2 -> {
                iv_add_icon.visibility = View.GONE
                tasksText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
                eventsText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
                holidaysText.setTextColor(ContextCompat.getColor(ctx, R.color.header_color))
                leavesText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))

                tasksLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
                eventsLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
                holidaysLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.header_color))
                leavesLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
            }
            3 -> {
                iv_add_icon.visibility = View.VISIBLE
                tasksText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
                eventsText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
                holidaysText.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
                leavesText.setTextColor(ContextCompat.getColor(ctx, R.color.header_color))

                tasksLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
                eventsLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
                holidaysLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
                leavesLayoutSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.header_color))
            }
        }
    }
}