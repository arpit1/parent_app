package com.schoolmanagement.parent.fragment

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.prolificinteractive.materialcalendarview.*
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.adapter.SchoolDiaryAdapter
import com.schoolmanagement.parent.pojo.SchoolDiaryDataPojo
import com.schoolmanagement.parent.pojo.SchoolDiaryPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arpit.jain on 2/16/2018.
 */
class SchoolDiaryFragment : Fragment(), OnDateSelectedListener, OnMonthChangedListener {

    private lateinit var viewSchoolDiaryFragment: View
    private var dialog: ParentDialog? = null
    private lateinit var noDataAvailable: TextView
    private lateinit var ctx: HomeActivity
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private lateinit var calendar: MaterialCalendarView
    private var diary_list: ArrayList<SchoolDiaryDataPojo>? = null
    private lateinit var adapter: SchoolDiaryAdapter
    private lateinit var tv_no_data: TextView
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var schoolDiary: RecyclerView
    private var month: String = ""
    private var year: String = ""
    lateinit var datesAssigned: HashSet<CalendarDay>

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewSchoolDiaryFragment = inflater!!.inflate(R.layout.fragment_school_diary, container, false)
        ctx = activity as HomeActivity
        initialize()
        setListener()
        return viewSchoolDiaryFragment
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        diary_list = ArrayList()
        calendar = viewSchoolDiaryFragment.findViewById(R.id.calendar_view)
        tv_no_data = viewSchoolDiaryFragment.findViewById(R.id.tv_no_data)
        calendar.selectedDate = CalendarDay.today()
        schoolDiary = viewSchoolDiaryFragment.findViewById(R.id.schoolDiary)
        mLayoutManager = LinearLayoutManager(ctx)
        schoolDiary.layoutManager = mLayoutManager
    }

    private fun setListener() {
        calendar.setOnDateChangedListener(this)
        calendar.setOnMonthChangedListener(this)
    }

    override fun onDateSelected(widget: MaterialCalendarView, date: CalendarDay, selected: Boolean) {
        calendar.selectedDate = date
//        calendar.goToNext()
//        val handler = Handler()
//        handler.postDelayed({
//            calendar.goToPrevious()
//        }, 0)
        getSchoolDiaryData(date)
    }

    override fun onMonthChanged(widget: MaterialCalendarView?, date: CalendarDay?) {
        month = (date!!.month + 1).toString()
        year = date.year.toString()
        calendar.selectedDate = CalendarDay.today()
        val handler = Handler()
        handler.postDelayed({
            getSchoolDiary("")
        }, 150)

    }

    private fun getSchoolDiaryData(date: CalendarDay) {
        val df = SimpleDateFormat(ctx.getString(R.string.dateformat), Locale.US)
        val dateVal = df.format(date.date)
        getSchoolDiary(dateVal)
    }

    private fun getSchoolDiary(dateVal: String) {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<SchoolDiaryPojo> = apiService.getSchoolDiary(ctx.getFromPrefs(ParentConstant.USER_ID).toString(), ctx.getFromPrefs(ParentConstant.SCHOOL_ID).toString(), dateVal, month, year)
            call.enqueue(object : Callback<SchoolDiaryPojo> {
                override fun onResponse(call: Call<SchoolDiaryPojo>, response: Response<SchoolDiaryPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            if (response.body()!!.data.size > 0) {
                                tv_no_data.visibility = View.GONE
                                diary_list?.clear()
                                datesAssigned = HashSet()
                                datesAssigned.clear()
                                diary_list?.addAll(response.body()!!.data)
                                val listDiary = diary_list
                                for (i in 0 until listDiary!!.size) {
                                    val date = listDiary[i].time.split("-")
                                    val day = date[0].toInt()
                                    val month = date[1].toInt() - 1
                                    val year = date[2].toInt()
                                    val dateToHighLight = CalendarDay.from(year, month, day)
                                    datesAssigned.add(dateToHighLight)
                                }

                                calendar.addDecorators(CircleDecoratorLow(ctx, datesAssigned))

                                adapter = SchoolDiaryAdapter(ctx, diary_list!!)
                                schoolDiary.adapter = adapter
                                schoolDiary.itemAnimator = DefaultItemAnimator()
                            } else {
                                tv_no_data.visibility = View.VISIBLE
                                diary_list = ArrayList()
                                adapter = SchoolDiaryAdapter(ctx, diary_list!!)
                                schoolDiary.adapter = adapter
                                adapter.notifyDataSetChanged()
                            }
                        } else {
                            dialog?.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<SchoolDiaryPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog?.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && diary_list != null && diary_list?.size == 0) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true

            calendar.selectedDate
            if (month == "") {
                month = (CalendarDay.today().month + 1).toString()
                year = CalendarDay.today().year.toString()
            }

            val handler = Handler()
            handler.postDelayed({
                getSchoolDiary("")
            }, 150)
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
        }
    }

    inner class CircleDecoratorLow(context: Context, dates: Collection<CalendarDay>) : DayViewDecorator {
        private val drawable: Drawable = ContextCompat.getDrawable(context, R.drawable.circle)
        private var datesLow: HashSet<CalendarDay> = java.util.HashSet(dates)

        override fun shouldDecorate(day: CalendarDay): Boolean {
            return datesLow.contains(day)
        }

        override fun decorate(view: DayViewFacade) {
            view.addSpan(ForegroundColorSpan(Color.BLACK))
            view.setSelectionDrawable(drawable)
        }
    }
}