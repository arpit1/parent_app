package com.schoolmanagement.parent.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.adapter.LeaveAdapter
import com.schoolmanagement.parent.pojo.LeaveDataPojo
import com.schoolmanagement.parent.pojo.LeavePojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by arpit.jain on 2/14/2018.
 */
class LeavesFragment : Fragment() {
    private lateinit var viewLeavesFragment: View
    private var dialog: ParentDialog? = null
    private lateinit var noDataAvailable: TextView
    private lateinit var ctx: HomeActivity
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private lateinit var leaveList: RecyclerView
    private lateinit var adapter: LeaveAdapter
    private var leaveData: ArrayList<LeaveDataPojo>? = null
    private var leaveDataFilter: ArrayList<LeaveDataPojo>? = null
    private lateinit var mLayoutManager: LinearLayoutManager
    private var searchView: android.support.v7.widget.SearchView? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewLeavesFragment = inflater!!.inflate(R.layout.fragment_leaves, container, false)
        ctx = activity as HomeActivity
        initialize()
        setListener()
        return viewLeavesFragment
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        mLayoutManager = LinearLayoutManager(ctx)
        leaveData = ArrayList()
        leaveDataFilter = ArrayList()

        searchView = viewLeavesFragment.findViewById(R.id.search_box)
        leaveList = viewLeavesFragment.findViewById(R.id.leaveList)
        noDataAvailable = viewLeavesFragment.findViewById(R.id.noDataAvailable)
        leaveList.layoutManager = mLayoutManager

        if (!fragmentResume && fragmentVisible) {   //only when first time fragment is created
            getLeaves()
        }
    }

    private fun setListener() {
        searchView!!.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                //Log.e("onQueryTextChange", "called");
                val leaves: ArrayList<LeaveDataPojo> = leaveData!!
                leaveDataFilter!!.clear()
                if (newText.isNotEmpty()) {
                    (0 until leaves.size)
                            .filter { leaves[it].reason.toLowerCase().contains(newText.toLowerCase()) }
                            .forEach { leaveDataFilter!!.add(leaves[it]) }
                } else {
                    leaveDataFilter!!.addAll(leaves)
                }
                setAdapter(leaveDataFilter!!)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // Do your task here
                ctx.hideSoftKeyboard()
                return false
            }

        })
    }

    override fun onResume() {
        super.onResume()
        leaveDataFilter = ArrayList()
        leaveDataFilter?.clear()
        if (ParentConstant.LEAVES_REFRESH) {
            leaveData?.clear()
            getLeaves()
            searchView?.setQuery("", false)
        }
        if (searchView != null) {
            searchView!!.isIconified = false
            searchView!!.clearFocus()
        }
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && leaveData != null && leaveData?.size == 0) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            if (searchView != null)
                searchView!!.clearFocus()
            getLeaves()
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
            if (searchView != null)
                searchView!!.clearFocus()
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
            if (searchView != null)
                searchView!!.clearFocus()
        }
    }

    private fun getLeaves() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<LeavePojo> = apiService.getLeavesList(ctx.getFromPrefs(ParentConstant.SCHOOL_ID).toString(), ctx.getFromPrefs(ParentConstant.USER_ID).toString(), ctx.getStudentData()!!.id)
            call.enqueue(object : Callback<LeavePojo> {
                override fun onResponse(call: Call<LeavePojo>, response: Response<LeavePojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            ParentConstant.LEAVES_REFRESH = false
                            if (response.body()!!.data.size > 0) {
                                leaveList.visibility = View.VISIBLE
                                noDataAvailable.visibility = View.GONE
                                leaveData!!.addAll(response.body()!!.data)
                                setAdapter(leaveData!!)
                            } else {
                                leaveList.visibility = View.GONE
                                noDataAvailable.visibility = View.VISIBLE
                            }
                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<LeavePojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setAdapter(leaves: ArrayList<LeaveDataPojo>) {
        adapter = LeaveAdapter(ctx, leaves)
        leaveList.adapter = adapter
    }
}