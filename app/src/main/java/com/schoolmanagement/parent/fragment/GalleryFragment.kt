package com.schoolmanagement.parent.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.adapter.GalleryPagerAdapter
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ParentDialog

/**
 * Created by arpit.jain on 2/16/2018.
 */
class GalleryFragment : Fragment(), View.OnClickListener {

    private lateinit var viewGalleryFragment: View
    private var dialog: ParentDialog? = null
    private lateinit var ctx: HomeActivity
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private lateinit var assignmentTxt: TextView
    private lateinit var activitiesTxt: TextView
    private lateinit var gallery_view_pager: ViewPager
    private var mAdapter: GalleryPagerAdapter? = null
    private lateinit var assignmentLayout: LinearLayout
    private lateinit var assignmentSelector: LinearLayout
    private lateinit var activitiesLayout: LinearLayout
    private lateinit var activitiesSelector: LinearLayout

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewGalleryFragment = inflater!!.inflate(R.layout.fragment_gallery, container, false)
        ctx = activity as HomeActivity
        initialize()
        setListener()
        return viewGalleryFragment
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        mAdapter = GalleryPagerAdapter(ctx.supportFragmentManager)
        assignmentTxt = viewGalleryFragment.findViewById(R.id.assignmentTxt)
        activitiesTxt = viewGalleryFragment.findViewById(R.id.activitiesTxt)
        gallery_view_pager = viewGalleryFragment.findViewById(R.id.gallery_view_pager)
        assignmentLayout = viewGalleryFragment.findViewById(R.id.assignmentLayout)
        assignmentSelector = viewGalleryFragment.findViewById(R.id.assignmentSelector)
        activitiesLayout = viewGalleryFragment.findViewById(R.id.activitiesLayout)
        activitiesSelector = viewGalleryFragment.findViewById(R.id.activitiesSelector)

        assignmentLayout.setOnClickListener(this)
        activitiesLayout.setOnClickListener(this)
    }

    private fun setListener() {
        gallery_view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                setStyle(position)
            }

            override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

            override fun onPageScrollStateChanged(arg0: Int) {}
        })
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.assignmentLayout -> if (gallery_view_pager.currentItem != 0) {
                gallery_view_pager.currentItem = 0
                setStyle(0)
            }

            R.id.activitiesLayout -> if (gallery_view_pager.currentItem != 1) {
                gallery_view_pager.currentItem = 1
                setStyle(1)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val filterpojo = ctx.getFilterData()
        if (ParentConstant.GALLERY_REFRESH) {
            if (filterpojo!!.category == "Activities") {
                setStyle(1)
                activitiesLayout.performClick()
            } else {
                setStyle(0)
                assignmentLayout.performClick()
            }
        }
    }

    private fun setStyle(position: Int) {
        when (position) {
            0 -> {
                assignmentTxt.setTextColor(ContextCompat.getColor(ctx, R.color.header_color))
                activitiesTxt.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))

                assignmentSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.header_color))
                activitiesSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
            }
            1 -> {
                assignmentTxt.setTextColor(ContextCompat.getColor(ctx, R.color.dark_grey))
                activitiesTxt.setTextColor(ContextCompat.getColor(ctx, R.color.header_color))

                assignmentSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.profile_bg))
                activitiesSelector.setBackgroundColor(ContextCompat.getColor(ctx, R.color.header_color))
            }
        }
    }


    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && mAdapter != null) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            if (gallery_view_pager.adapter == null) {
                gallery_view_pager.adapter = mAdapter
                gallery_view_pager.offscreenPageLimit = 2
            }
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
            ctx.hideSoftKeyboard()
        }
    }
}