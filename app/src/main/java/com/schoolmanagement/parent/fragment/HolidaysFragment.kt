package com.schoolmanagement.parent.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.adapter.HolidayAdapter
import com.schoolmanagement.parent.pojo.HolidayDataPojo
import com.schoolmanagement.parent.pojo.HolidayPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by arpit.jain on 2/14/2018.
 */
class HolidaysFragment : Fragment() {
    private lateinit var viewHolidaysFragment: View
    private var dialog: ParentDialog? = null
    private lateinit var noDataAvailable: TextView
    private lateinit var ctx: HomeActivity
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private lateinit var holidayList: RecyclerView
    private lateinit var adapter: HolidayAdapter
    private var holidayData: ArrayList<HolidayDataPojo>? = null
    private lateinit var mLayoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewHolidaysFragment = inflater!!.inflate(R.layout.fragment_holidays, container, false)
        ctx = activity as HomeActivity
        initialize()
        setListener()
        return viewHolidaysFragment
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        mLayoutManager = LinearLayoutManager(ctx)
        holidayData = ArrayList()

        holidayList = viewHolidaysFragment.findViewById(R.id.holidayList)
        noDataAvailable = viewHolidaysFragment.findViewById(R.id.noDataAvailable)
        holidayList.layoutManager = mLayoutManager

        if (!fragmentResume && fragmentVisible) {   //only when first time fragment is created
            holidayList()
        }
    }

    private fun setListener() {

    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && holidayData != null && holidayData?.size == 0) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            holidayList()
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
        }
    }

    private fun holidayList() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<HolidayPojo> = apiService.getHolidays(ctx.getFromPrefs(ParentConstant.SCHOOL_ID).toString())
            call.enqueue(object : Callback<HolidayPojo> {
                override fun onResponse(call: Call<HolidayPojo>, response: Response<HolidayPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            if (response.body()!!.data.size > 0) {
                                holidayList.visibility = View.VISIBLE
                                noDataAvailable.visibility = View.GONE
                                holidayData!!.addAll(response.body()!!.data)
                                adapter = HolidayAdapter(ctx, holidayData)
                                holidayList.adapter = adapter
                            } else {
                                holidayList.visibility = View.GONE
                                noDataAvailable.visibility = View.VISIBLE
                            }
                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<HolidayPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }
}