package com.schoolmanagement.parent.fragment

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.*
import com.schoolmanagement.parent.util.ParentConstant
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.footer.*

class DrawerFragment : Fragment() {
    private lateinit var viewFragment: View
    private lateinit var profileImage: CircleImageView
    private lateinit var userName: TextView
    private lateinit var email: TextView

    private lateinit var mDrawerToggle: ActionBarDrawerToggle
    private var selectedPos = -1
    private var drawerlayout: DrawerLayout? = null

    private lateinit var my_profile: LinearLayout
    private lateinit var notice_board_menu_option: LinearLayout
    private lateinit var task_menu_option: LinearLayout
    private lateinit var teacher_task_menu_option: LinearLayout
    private lateinit var events_menu_option: LinearLayout
    private lateinit var leaves_menu_option: LinearLayout
    private lateinit var student_progress_menu_option: LinearLayout
    private lateinit var feedback_menu_option: LinearLayout
    private lateinit var about_menu_option: LinearLayout
    private lateinit var logout_menu_option: LinearLayout
    private lateinit var home_menu_option: LinearLayout

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (inflater != null) {
            viewFragment = inflater.inflate(R.layout.left_side_list_layout, container, false)
        }
        initialize()
        return viewFragment
    }

    private fun initialize() {
        profileImage = viewFragment.findViewById(R.id.profilePic)
        userName = viewFragment.findViewById(R.id.user_name)
        email = viewFragment.findViewById(R.id.email)
        my_profile = viewFragment.findViewById(R.id.my_profile)
        notice_board_menu_option = viewFragment.findViewById(R.id.notice_board_menu_option)
        task_menu_option = viewFragment.findViewById(R.id.task_menu_option)
        teacher_task_menu_option = viewFragment.findViewById(R.id.teacher_task_menu_option)
        events_menu_option = viewFragment.findViewById(R.id.events_menu_option)
        leaves_menu_option = viewFragment.findViewById(R.id.leaves_menu_option)
        student_progress_menu_option = viewFragment.findViewById(R.id.student_progress_menu_option)
        feedback_menu_option = viewFragment.findViewById(R.id.feedback_menu_option)
        about_menu_option = viewFragment.findViewById(R.id.about_menu_option)
        logout_menu_option = viewFragment.findViewById(R.id.logout_menu_option)
        home_menu_option = viewFragment.findViewById(R.id.home_menu_option)

        if ((activity as BaseActivity).getStudentData() != null && !(activity as BaseActivity).getStudentData()?.id!!.isEmpty()) {
            userName.text = (activity as BaseActivity).getStudentName()
            email.text = (activity as BaseActivity).getFromPrefs(ParentConstant.PRIMARY_EMAIL)
            (activity as BaseActivity).setProfileImageInLayoutProfile(context, ParentConstant.IMAGE_STUDENT_PROFILE + (activity as BaseActivity).getStudentData()!!.profile_pic, profileImage)
        }

        setListener()
    }

    private fun setListener() {
        notice_board_menu_option.setOnClickListener {
            selectedPos = 0
            drawerlayout!!.closeDrawers()
        }
        task_menu_option.setOnClickListener {
            selectedPos = 1
            drawerlayout!!.closeDrawers()
        }
        events_menu_option.setOnClickListener {
            selectedPos = 2
            drawerlayout!!.closeDrawers()
        }
        leaves_menu_option.setOnClickListener {
            selectedPos = 3
            drawerlayout!!.closeDrawers()
        }
        student_progress_menu_option.setOnClickListener {
            selectedPos = 4
            drawerlayout!!.closeDrawers()

        }
        feedback_menu_option.setOnClickListener {
            selectedPos = 5
            drawerlayout!!.closeDrawers()
        }
        about_menu_option.setOnClickListener {
            selectedPos = 6
            drawerlayout!!.closeDrawers()
        }
        my_profile.setOnClickListener {
            selectedPos = 7
            drawerlayout!!.closeDrawers()
        }
        home_menu_option.setOnClickListener {
            selectedPos = 8
            drawerlayout!!.closeDrawers()
        }
        logout_menu_option.setOnClickListener {
            selectedPos = 9
            drawerlayout!!.closeDrawers()
        }
        teacher_task_menu_option.setOnClickListener {
            selectedPos = 10
            drawerlayout!!.closeDrawers()
        }
    }

    @SuppressLint("NewApi")
    fun setUp(drawerlayout: DrawerLayout) {

        this.drawerlayout = drawerlayout
        mDrawerToggle = object : ActionBarDrawerToggle(activity, drawerlayout, R.string.open_drawer, R.string.close_drawer) {
            override fun onDrawerOpened(drawerView: View?) {
                super.onDrawerOpened(drawerView)
                (activity as BaseActivity).hideSoftKeyboard()
                if ((activity as BaseActivity).getStudentData() != null && !(activity as BaseActivity).getStudentData()?.id!!.isEmpty()) {
                    userName.text = (activity as BaseActivity).getStudentName()
                    email.text = (activity as BaseActivity).getFromPrefs(ParentConstant.PRIMARY_EMAIL)
                    (activity as BaseActivity).setProfileImageInLayoutProfile(context, ParentConstant.IMAGE_STUDENT_PROFILE + (activity as BaseActivity).getStudentData()!!.profile_pic, profileImage)
                }
                activity.invalidateOptionsMenu()
            }

            @SuppressLint("NewApi")
            override fun onDrawerClosed(drawerView: View?) {
                super.onDrawerClosed(drawerView)
                if (selectedPos != -1) {
                    when (selectedPos) {
                        0 -> if (getClassShortName() != ".activity.NoticeBoardActivity") {
                            (activity as BaseActivity).removeActivity("NoticeBoardActivity")
                            startActivity(Intent(activity, NoticeBoardActivity::class.java))
                        } else {
                            drawerlayout.closeDrawers()
                        }

                        1 -> navigateToTasks()

                        2 -> navigateToEvents()

                        3 -> navigateToLeaves()

                        4 -> if (getClassShortName() != ".activity.StudentProgressActivity") {
                            (activity as BaseActivity).removeActivity("StudentProgressActivity")
                            startActivity(Intent(activity, StudentProgressActivity::class.java))
                        } else {
                            drawerlayout.closeDrawers()
                        }

                        5 -> if (getClassShortName() != ".activity.FeedbackActivity") {
                            (activity as BaseActivity).removeActivity("FeedbackActivity")
                            startActivity(Intent(activity, FeedbackActivity::class.java))
                        } else {
                            drawerlayout.closeDrawers()
                        }

                        6 -> if (getClassShortName() != ".activity.AboutSchoolActivity") {
                            (activity as BaseActivity).removeActivity("AboutSchoolActivity")
                            startActivity(Intent(activity, AboutSchoolActivity::class.java))
                        } else {
                            drawerlayout.closeDrawers()
                        }

                        7 -> if (getClassShortName() != ".activity.ProfileActivity") {
                            (activity as BaseActivity).removeActivity("ProfileActivity")
                            startActivity(Intent(activity, ProfileActivity::class.java))
                        } else {
                            drawerlayout.closeDrawers()
                        }

                        8 -> if (getClassShortName() != ".activity.HomeActivity") {
                            (activity as BaseActivity).removeAllActivities()
                            activity.finish()
                            (activity as BaseActivity).getHomeActivity()?.tab_home?.performClick()
//                            startActivity(Intent(activity, HomeActivity::class.java))
                        } else {
                            (activity as BaseActivity).getHomeActivity()?.tab_home?.performClick()
                            drawerlayout.closeDrawers()
                        }

                        9 -> (activity as BaseActivity).appLogout()

                        10 -> if (getClassShortName() != ".activity.MyTaskListActivity") {
                            (activity as BaseActivity).removeActivity("MyTaskListActivity")
                            startActivity(Intent(activity, MyTaskListActivity::class.java))
                        } else {
                            drawerlayout.closeDrawers()
                        }

                        else -> {
                        }
                    }
                    selectedPos = -1
                }
            }
        }
        drawerlayout.addDrawerListener(mDrawerToggle)
    }

    private fun navigateToTasks() {
        if (getClassShortName() != ".activity.HomeActivity") {
            (activity as BaseActivity).removeAllActivities()
            activity.finish()
            (activity as BaseActivity).getHomeActivity()?.tab_calendar?.performClick()
            ((activity as BaseActivity).getHomeActivity()?.supportFragmentManager!!.fragments[4] as CalendarFragment).tasksLayout.performClick()
        } else {
            (activity as HomeActivity).tab_calendar.performClick()
            (activity.supportFragmentManager.fragments[4] as CalendarFragment).tasksLayout.performClick()
        }
    }

    private fun navigateToEvents() {
        if (getClassShortName() != ".activity.HomeActivity") {
            (activity as BaseActivity).removeAllActivities()
            activity.finish()
            (activity as BaseActivity).getHomeActivity()?.tab_calendar?.performClick()
            ((activity as BaseActivity).getHomeActivity()?.supportFragmentManager!!.fragments[4] as CalendarFragment).eventsLayout.performClick()
        } else {
            (activity as HomeActivity).tab_calendar.performClick()
            (activity.supportFragmentManager.fragments[4] as CalendarFragment).eventsLayout.performClick()
        }
    }

    private fun navigateToLeaves() {
        if (getClassShortName() != ".activity.HomeActivity") {
            (activity as BaseActivity).removeAllActivities()
            activity.finish()
            ParentConstant.LEAVES_REFRESH = true
            (activity as BaseActivity).getHomeActivity()?.tab_calendar?.performClick()
            ((activity as BaseActivity).getHomeActivity()?.supportFragmentManager!!.fragments[4] as CalendarFragment).leavesLayout.performClick()
        } else {
            (activity as HomeActivity).tab_calendar.performClick()
            (activity.supportFragmentManager.fragments[4] as CalendarFragment).leavesLayout.performClick()
        }
    }

    private fun getClassShortName(): String {
        val am = activity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val cn = am.getRunningTasks(1)[0].topActivity
        return cn.shortClassName
    }
}