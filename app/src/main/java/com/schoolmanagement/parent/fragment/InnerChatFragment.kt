package com.schoolmanagement.parent.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.BaseActivity
import com.schoolmanagement.parent.activity.ChatScreenActivity
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.adapter.ChatContactAdapter
import com.schoolmanagement.parent.pojo.UserListPojo
import com.schoolmanagement.parent.util.ParentConstant
import io.socket.emitter.Emitter
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class InnerChatFragment : Fragment() {

    private lateinit var viewChatfragment: View
    private lateinit var ctx: HomeActivity
    private lateinit var rvChatList: RecyclerView
    private var user_list_data: ArrayList<UserListPojo>? = null
    private var contactDataFilter: ArrayList<UserListPojo>? = null
    private lateinit var adapter: ChatContactAdapter
    private lateinit var mLayoutManager: LinearLayoutManager
    private var searchView: android.support.v7.widget.SearchView? = null
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private var LOGIN_ID: String? = null
    private var SCHOOL_ID: String? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewChatfragment = inflater!!.inflate(R.layout.fragment_innerchat_layout, container, false)
        ctx = activity as HomeActivity
        initialize()
        setListener()
        return viewChatfragment
    }

    private fun initialize() {
        rvChatList = viewChatfragment.findViewById(R.id.rvChatList)
        searchView = viewChatfragment.findViewById(R.id.search_box)
        contactDataFilter = ArrayList()
        mLayoutManager = LinearLayoutManager(ctx)
        rvChatList.layoutManager = mLayoutManager
        LOGIN_ID = ctx.getFromPrefs(ParentConstant.USER_ID).toString()
        SCHOOL_ID = ctx.getFromPrefs(ParentConstant.SCHOOL_ID).toString()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("user_id", "P_$LOGIN_ID")
            jsonObject.put("school_id", SCHOOL_ID)
            ctx.mSocket.emit("connectUser", jsonObject)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        ctx.mSocket.on("usersList", getUserList)
        ctx.mSocket.on("get_private_chat", onNewMessage)
    }

    override fun onResume() {
        super.onResume()
        if (searchView != null) {
            searchView!!.isIconified = false
            searchView!!.clearFocus()
        }
        val handler = Handler()
        handler.postDelayed({
            ctx.runOnUiThread {
                if (ctx.getClassShortName() == ".activity.HomeActivity") {
                    val jsonObject = JSONObject()
                    try {
                        jsonObject.put("user_id", "P_$LOGIN_ID")
                        jsonObject.put("school_id", SCHOOL_ID)
                        ctx.mSocket.emit("connectUser", jsonObject)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }
        }, 380)

    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && user_list_data != null) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            val jsonObject = JSONObject()
            try {
                jsonObject.put("user_id", "P_$LOGIN_ID")
                jsonObject.put("school_id", SCHOOL_ID)
                ctx.mSocket.emit("connectUser", jsonObject)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
            if (searchView != null)
                searchView!!.clearFocus()
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
            if (searchView != null)
                searchView!!.clearFocus()
        }
    }

    private fun setListener() {
        searchView!!.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                val task: ArrayList<UserListPojo> = user_list_data!!
                contactDataFilter!!.clear()
                if (newText.isNotEmpty()) {
                    (0 until task.size)
                            .filter { getTeacherName(task[it]).toLowerCase().contains(newText.toLowerCase()) }
                            .forEach { contactDataFilter!!.add(task[it]) }
                } else {
                    contactDataFilter!!.addAll(task)
                }
                setAdapter(contactDataFilter!!)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // Do your task here
                ctx.hideSoftKeyboard()
                return false
            }
        })

        rvChatList.addOnItemTouchListener(BaseActivity.RecyclerTouchListener(ctx, rvChatList, object : BaseActivity.ClickListener {
            override fun onClick(view: View, position: Int) {

                val task: ArrayList<UserListPojo> = contactDataFilter!!
                val username: String?
                val receiverId: String?
                if (!searchView!!.query.toString().isEmpty()) {
                    task[position].message_counter = 0
                    task[position].chat_size = 0
                    task[position].last_msg!!.put("isRead", "1")
                    username = getTeacherName(task[position])
                    receiverId = task[position].id
                } else {
                    user_list_data!![position].message_counter = 0
                    user_list_data!![position].chat_size = 0
                    user_list_data!![position].last_msg!!.put("isRead", "1")
                    username = getTeacherName(user_list_data!![position])
                    receiverId = user_list_data!![position].id
                }
                adapter.notifyDataSetChanged()

                ctx.startActivity(Intent(ctx, ChatScreenActivity::class.java).putExtra("reciver_id", receiverId).putExtra("username", username))
            }

            override fun onLongClick(view: View?, position: Int) {
            }
        }))
    }

    // call when new message received
    private val onNewMessage = Emitter.Listener { _ ->
        ctx.runOnUiThread {
            if (ctx.getClassShortName() == ".activity.HomeActivity") {
                val jsonObject = JSONObject()
                try {
                    jsonObject.put("user_id", "P_$LOGIN_ID")
                    jsonObject.put("school_id", SCHOOL_ID)
                    ctx.mSocket.emit("connectUser", jsonObject)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun getTeacherName(teacherData: UserListPojo): String {
        return if (teacherData.type == "0") {
            if (teacherData.mname != "")
                teacherData.fname + " " + teacherData.mname + " " + teacherData.lname
            else
                teacherData.fname + " " + teacherData.lname
        } else {
            teacherData.fname!!
        }
    }

    private val getUserList = Emitter.Listener { args ->
        ctx.runOnUiThread {
            val data = args[0] as JSONObject
            println("USERLIST>>>>" + args[0])
            try {
                val jarray = data.optJSONArray("data")
                user_list_data = ArrayList()
                if (jarray.length() > 0) {
                    for (i in 0 until jarray.length()) {
                        val json = jarray.optJSONObject(i)
                        val user_data = UserListPojo()
                        user_data.id = json.optString("id")
                        user_data.login_id = "P_" + ctx.getFromPrefs(ParentConstant.USER_ID)
                        user_data.fname = json.optString("fname")
                        if (json.has("mname")) {
                            user_data.mname = json.optString("mname")
                        }
                        user_data.lname = json.optString("lname")

                        user_data.isConnected = json.optString("isConnected")
                        user_data.profile_pic = json.optString("profile_pic")
                        user_data.last_msg = json.optJSONObject("last_msg")
                        user_data.type = json.optString("type")
                        user_data.message_counter = Integer.parseInt(json.optString("chat_size"))
                        user_data.chat_size = Integer.parseInt(json.optString("chat_size"))

                        user_list_data!!.add(user_data)
                    }
                    setAdapter(user_list_data)
                    searchView?.setQuery("", false)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    private fun setAdapter(user_list_data: ArrayList<UserListPojo>?) {
        adapter = ChatContactAdapter(ctx, user_list_data!!)
        rvChatList.adapter = adapter
    }
}