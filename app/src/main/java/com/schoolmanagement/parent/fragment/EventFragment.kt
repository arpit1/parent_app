package com.schoolmanagement.parent.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.HomeActivity
import com.schoolmanagement.parent.adapter.EventAdapter
import com.schoolmanagement.parent.pojo.EventDataPojo
import com.schoolmanagement.parent.pojo.EventPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by arpit.jain on 2/14/2018.
 */
class EventFragment : Fragment() {
    private lateinit var viewEventFragment: View
    private var dialog: ParentDialog? = null
    private lateinit var noDataAvailable: TextView
    private lateinit var ctx: HomeActivity
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private lateinit var rvEventList: RecyclerView
    private var eventList: ArrayList<EventDataPojo>? = null
    private lateinit var adapter: EventAdapter
    private lateinit var mLayoutManager: LinearLayoutManager
    private var searchView: android.support.v7.widget.SearchView? = null
    private var eventDataFilter: ArrayList<EventDataPojo>? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewEventFragment = inflater!!.inflate(R.layout.fragment_event, container, false)
        ctx = activity as HomeActivity
        initialize()
        setListener()
        return viewEventFragment
    }

    private fun initialize() {
        dialog = ParentDialog(ctx)
        mLayoutManager = LinearLayoutManager(ctx)
        eventList = ArrayList()
        eventDataFilter = ArrayList()

        rvEventList = viewEventFragment.findViewById(R.id.rvEventList)
        noDataAvailable = viewEventFragment.findViewById(R.id.noDataAvailable)
        searchView = viewEventFragment.findViewById(R.id.search_box)
        rvEventList.layoutManager = mLayoutManager

        if (!fragmentResume && fragmentVisible) {   //only when first time fragment is created
            getEvent()
        }
    }

    private fun setListener() {
        searchView!!.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                //Log.e("onQueryTextChange", "called");
                val event: ArrayList<EventDataPojo> = eventList!!
                eventDataFilter!!.clear()
                if (newText.isNotEmpty()) {
                    (0 until event.size)
                            .filter { event[it].event_name.toLowerCase().contains(newText.toLowerCase()) }
                            .forEach { eventDataFilter!!.add(event[it]) }
                } else {
                    eventDataFilter!!.addAll(event)
                }
                setAdapter(eventDataFilter!!)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // Do your task here
                ctx.hideSoftKeyboard()
                return false
            }

        })
    }

    override fun onResume() {
        super.onResume()
        if (searchView != null) {
            searchView!!.isIconified = false
            searchView!!.clearFocus()
        }
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && eventList != null && eventList?.size == 0) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            getEvent()
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
            searchView!!.clearFocus()
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
            searchView!!.clearFocus()
        }
    }

    private fun getEvent() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<EventPojo> = apiService.getEvent(ctx.getFromPrefs(ParentConstant.SCHOOL_ID).toString())
            call.enqueue(object : Callback<EventPojo> {
                override fun onResponse(call: Call<EventPojo>, response: Response<EventPojo>?) {
                    if (response != null) {
                        if (response.body()!!.status == "1") {
                            ParentConstant.LEAVES_REFRESH = false
                            if (response.body()!!.data.size > 0) {
                                rvEventList.visibility = View.VISIBLE
                                noDataAvailable.visibility = View.GONE
                                eventList!!.addAll(response.body()!!.data)
                                setAdapter(eventList!!)
                            } else {
                                rvEventList.visibility = View.GONE
                                noDataAvailable.visibility = View.VISIBLE
                            }
                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<EventPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }

    private fun setAdapter(eventList: ArrayList<EventDataPojo>) {
        adapter = EventAdapter(ctx, eventList)
        rvEventList.adapter = adapter
    }
}