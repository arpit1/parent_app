package com.schoolmanagement.parent.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.activity.ProfileActivity
import com.schoolmanagement.parent.adapter.StudentPagerAdapter
import com.schoolmanagement.parent.pojo.LoginDataPojo
import com.schoolmanagement.parent.pojo.LoginPojo
import com.schoolmanagement.parent.util.ParentConstant
import com.teacherapp.util.ApiClient
import com.teacherapp.util.ApiInterface
import com.teacherapp.util.ConnectionDetector
import com.teacherapp.util.ParentDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by arpit.jain on 2/14/2018.
 */
class ProfileFragment : Fragment() {

    private lateinit var viewProfileFragment: View
    private var dialog: ParentDialog? = null
    private lateinit var ctx: ProfileActivity
    private var fragmentResume = false
    private var fragmentVisible = false
    private var fragmentOnCreated = false
    private lateinit var userName: TextView
    private lateinit var email: TextView
    private lateinit var etEmergencyName: TextView
    private lateinit var etEmergencyContact: TextView
    private lateinit var etId: TextView
    private lateinit var etPassportNumber: TextView
    private lateinit var etAddress: TextView
    private lateinit var etCountry: TextView
    private lateinit var profilePic: ImageView
    private var loginDataPojo: LoginDataPojo? = null
    private lateinit var studentViewPager: ViewPager
    private lateinit var mAdapter: StudentPagerAdapter
    private lateinit var primary_phone : TextView
    private lateinit var Father_name : TextView
    private lateinit var father_email : TextView
    private lateinit var father_phone : TextView
    private lateinit var father_occup : TextView
    private lateinit var father_work_place : TextView
    private lateinit var mother_name : TextView
    private lateinit var mother_email : TextView
    private lateinit var mother_phone : TextView
    private lateinit var mother_occup : TextView
    private lateinit var mother_work_place : TextView

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewProfileFragment = inflater!!.inflate(R.layout.fragment_profile, container, false)
        initialize()
        setListener()
        return viewProfileFragment
    }

    private fun initialize() {
        ctx = activity as ProfileActivity
        dialog = ParentDialog(ctx)
        studentViewPager = viewProfileFragment.findViewById(R.id.student_view_pager)
        mAdapter = StudentPagerAdapter(ctx)
        studentViewPager.adapter = mAdapter
        userName = viewProfileFragment.findViewById(R.id.userName)
        email = viewProfileFragment.findViewById(R.id.email)
        etEmergencyName = viewProfileFragment.findViewById(R.id.etEmergencyName)
        etEmergencyContact = viewProfileFragment.findViewById(R.id.etEmergencyContact)
        etId = viewProfileFragment.findViewById(R.id.etId)
        etPassportNumber = viewProfileFragment.findViewById(R.id.etPassportNumber)
        etAddress = viewProfileFragment.findViewById(R.id.etAddress)
        etCountry = viewProfileFragment.findViewById(R.id.etCountry)
        profilePic = viewProfileFragment.findViewById(R.id.profilePic)
        primary_phone = viewProfileFragment.findViewById(R.id.primary_phone)
        Father_name = viewProfileFragment.findViewById(R.id.father_name)
        father_phone = viewProfileFragment.findViewById(R.id.father_phone)
        father_email = viewProfileFragment.findViewById(R.id.father_email)
        father_occup = viewProfileFragment.findViewById(R.id.father_occup)
        father_work_place = viewProfileFragment.findViewById(R.id.father_work_place)
        mother_name = viewProfileFragment.findViewById(R.id.mother_name)
        mother_email = viewProfileFragment.findViewById(R.id.mother_email)
        mother_phone = viewProfileFragment.findViewById(R.id.mother_phone)
        mother_occup = viewProfileFragment.findViewById(R.id.mother_occup)
        mother_work_place = viewProfileFragment.findViewById(R.id.mother_work_place)
        if (!fragmentResume && fragmentVisible) {   //only when first time fragment is created
            getProfile()
        }
    }

    override fun onResume() {
        super.onResume()
        if (ParentConstant.PROFILE_REFRESH) {
            getProfile()
        }
    }

    private fun setListener() {

    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && loginDataPojo == null) {   // only at fragment screen is resumed
            fragmentResume = true
            fragmentVisible = false
            fragmentOnCreated = true
            getProfile()
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false
            fragmentVisible = true
            fragmentOnCreated = true
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false
            fragmentResume = false
        }
    }

    private fun getProfile() {
        if (ConnectionDetector.isConnected(ctx)) {
            val d = ParentDialog.showLoading(ctx)
            d.setCanceledOnTouchOutside(false)
            val apiService: ApiInterface = ApiClient.getClient()!!.create(ApiInterface::class.java)
            val call: Call<LoginPojo> = apiService.getProfile(ctx.getFromPrefs(ParentConstant.USER_ID).toString())
            call.enqueue(object : Callback<LoginPojo> {
                override fun onResponse(call: Call<LoginPojo>, response: Response<LoginPojo>?) {
                    if (response != null) {
                        if (response.body()?.status == "1" && response.body()?.data != null) {
                            loginDataPojo = response.body()?.data
                            userName.text = loginDataPojo?.name
                            email.text = loginDataPojo?.email
                            etEmergencyName.text = loginDataPojo?.students?.emergency_contact_name
                            etEmergencyContact.text = loginDataPojo?.students?.emergency_contact_number
                            etId.text = loginDataPojo?.id
                            etPassportNumber.text = loginDataPojo?.passport_number
                            if(loginDataPojo?.students?.address_line2 == "" && loginDataPojo?.students?.address_line3 == ""){
                                etAddress.text = loginDataPojo?.students?.address_line1+ ", "+loginDataPojo?.students?.city +", "+loginDataPojo?.students?.state+", "+loginDataPojo?.students?.pincode
                            }else if(loginDataPojo?.students?.address_line2 != ""){
                                etAddress.text = loginDataPojo?.students?.address_line1+ ", " + loginDataPojo?.students?.address_line2 + ", "+
                                        loginDataPojo?.students?.city +", "+loginDataPojo?.students?.state+", "+loginDataPojo?.students?.pincode
                            }else if(loginDataPojo?.students?.address_line3 != ""){
                                etAddress.text = loginDataPojo?.students?.address_line1+ ", "+loginDataPojo?.students?.address_line3+
                                        ", "+loginDataPojo?.students?.city +", "+loginDataPojo?.students?.state+", "+loginDataPojo?.students?.pincode
                            }
                            etCountry.text = loginDataPojo?.country_name
                            primary_phone.text = loginDataPojo?.primary_phone
                            Father_name.text = loginDataPojo?.father_name
                            father_email.text = loginDataPojo?.father_email
                            father_phone.text = loginDataPojo?.father_phone
                            father_occup.text = loginDataPojo?.father_occupation
                            father_work_place.text = loginDataPojo?.father_place_of_work
                            mother_name.text = loginDataPojo?.mother_name
                            mother_email.text = loginDataPojo?.mother_email
                            mother_phone.text = loginDataPojo?.mother_phone
                            mother_occup.text = loginDataPojo?.mother_occupation
                            mother_work_place.text = loginDataPojo?.mother_place_of_work
                            ctx.setProfileImageInLayoutProfile(ctx, ParentConstant.IMAGE_STUDENT_PROFILE + loginDataPojo?.students?.profile_pic, profilePic)
                            ParentConstant.PROFILE_REFRESH = false
                        } else {
                            dialog!!.displayCommonDialog(response.body()!!.message)
                        }
                        d.dismiss()
                    }
                }

                override fun onFailure(call: Call<LoginPojo>?, t: Throwable?) {
                    println(t.toString())
                    d.dismiss()
                }
            })
        } else {
            dialog!!.displayCommonDialog(resources.getString(R.string.no_internet_connection))
        }
    }
}