package com.schoolmanagement.parent.listener

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import com.schoolmanagement.parent.R
import com.schoolmanagement.parent.R.string.app_name
import com.schoolmanagement.parent.activity.HomeActivity



/**
 * Created by matangi.agarwal on 5/2/2018.
 */

class AlarmReceiver:BroadcastReceiver() {
    override fun onReceive(context:Context, intent:Intent) {
        val notificationIntent = Intent(context, HomeActivity::class.java)
        val title = intent.getStringExtra("notification_title")
        val stackBuilder = TaskStackBuilder.create(context)
        stackBuilder.addParentStack(HomeActivity::class.java)
        stackBuilder.addNextIntent(notificationIntent)
        val pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        val builder = NotificationCompat.Builder(context)

        val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        builder.setSound(uri)

        val notification = builder.setContentTitle(context.getString(app_name))
                .setContentText(title)
                .setTicker(context.resources.getString(R.string.remindar_ticker))
                .setSmallIcon(R.mipmap.notificationicon_tray)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher))
                .setContentIntent(pendingIntent).build()
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notification)
    }
}